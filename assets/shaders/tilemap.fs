#version 410
out vec4 FragColor;

uniform sampler2D tile_atlas;

in vec4  v_Color;
in vec2  v_texCoords;
in float v_texID;

uniform float u_tilingFactor;

uniform float u_time;

void main()
{
    float time = clamp(u_time, 0, 1); //sin(u_time) * .5 + .5;
    FragColor = texture(tile_atlas, v_texCoords) * v_Color;
}
