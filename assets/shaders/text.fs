#version 410
out vec4 FragColor;

in vec4  v_Color;
in vec2  v_texCoords;
in float v_texID;

uniform vec4 u_color;
uniform float u_time;

uniform sampler2D tex;

void main()
{
    vec4 c = v_Color * texture(tex, v_texCoords).r;
    // if(texture(tex, v_texCoords).r < .1) discard;

    FragColor = c;
    // FragColor = v_Color;
}
