#version 410
out vec4 FragColor;

in vec4  v_Color;
in vec2  v_texCoords;
in float v_texID;

uniform vec4 u_color;
uniform float u_time;

void main()
{
    FragColor = v_Color;
}
