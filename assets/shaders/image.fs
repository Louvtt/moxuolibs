#version 410
out vec4 FragColor;

in vec4  v_Color;
in vec2  v_texCoords;
in float v_texID;

uniform vec4 u_color;
uniform float u_time;

uniform sampler2D u_tex;

void main()
{
    FragColor = texture(u_tex, v_texCoords);//* u_color * v_Color;
}
