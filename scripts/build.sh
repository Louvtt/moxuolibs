#!/bin/bash

# Main build dir
cd ../
mkdir build_linux

# Example 1
cd build
mkdir particles_mk
cd particles_m
echo Building example [particles] into build/particles_mk/
cmake ../examples/particles/ -G "Unix Makefiles"
make .

# Example 2
cd ..
mkdir raytracing_mk
cd raytracing_mk
echo Building example [raytracing] into build/raytracing_mk/
cmake ../examples/raytracing/ -G "Unix Makefiles"
make .

# Example 3
cd ..
mkdir tilemaps_mk
cd tilemaps_mk
echo Building example [tilemaps] into build/tilemaps_mk/
cmake ../examples/tilemaps/ -G "Unix Makefiles"
make .

# Example 4
cd ..
mkdir gui_mk
cd gui_mk
echo Building example [gui] into build/gui_mk/
cmake ../examples/gui/ -G "Unix Makefiles"
make .
