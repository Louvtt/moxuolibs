@echo off

cd ../
mkdir build
cd build
mkdir particles_mgw
cd particles_mgw
echo Building example [particles] into build/particles_mgw/
cmake ../../examples/particles/ -G "MinGW Makefiles"
mingw32-make .

cd ..
mkdir raytracing_mgw
cd raytracing_mgw
echo Building example [raytracing] into build/raytracing_mgw/
cmake ../../examples/raytracing/ -G "MinGW Makefiles"
mingw32-make .

cd ..
mkdir tilemaps_mgw
cd tilemaps_mgw
echo Building example [tilemaps] into build/tilemaps_mgw/
cmake ../../examples/tilemaps/ -G "MinGW Makefiles"
mingw32-make .

cd ..
mkdir gui_mgw
cd gui_mgw
echo Building example [gui] into build/gui_mgw/
cmake ../../examples/gui/ -G "Unix Makefiles"
mingw32-make .