/*
MXLIBS. mx gui is based of mxrender and is part of MXLIBS.
They are small(header-only or .h+.cpp files) libraries to make some graphical
experiments or small projetcs quickly and easily.
It let's you access anything 
Copyright (C) 2021 Lucyel Zanardo (@Louvtt)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _MX_MXGUI_H_
#define _MX_MXGUI_H_

#define MXGUI_VERSION 1

#include "mxmaths.h"
#include "mxrender.h"

//////////////////////////////////////////////////
// HEADER FILE
#if !defined(MXH_STATIC)
    #define MXGDEF static
#else
    #define MXGDEF extern
#endif

namespace mx {
    typedef struct {
        mx_vec3 pos;
        mx::mesh_t background, bar;
        float min, max, value;
        mx_vec2 size;
        bool alignCenter = false;

        vec4 backgroundColor, barColor;
    } progress_bar_t;
    namespace progress_bar {
        progress_bar_t create( mx_vec3 pos, float min = 0.f, float max = 1.f, mx_vec2 size = { 1.f, 1.f }, bool alignCenter = false);

        void draw(progress_bar_t bar, mx::shader_t shader);
    }
}

#endif //_MX_MXGUI_H_
