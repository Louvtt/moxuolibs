/*
MXLIBS. MX tilemap is an addon of mxrender and is part of MXLIBS.
They are small(header-only or .h+.cpp files) libraries to make some graphical
experiments or small projetcs quickly and easily.
It let's you access anything 
Copyright (C) 2021 Lucyel Zanardo (@Louvtt)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _MX_ADDONS_TILEMAP_H_
#define _MX_ADDONS_TILEMAP_H_

#include "../mxrender.h"

namespace mx {
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    // SOURCE: [https://gamedevelopment.tutsplus.com/tutorials/how-to-use-tile-bitmasking-to-auto-tile-your-level-layouts--cms-25673]
    // tile rule
    enum class tiletype : int
    {
        // 4 bit (15 cases)
        // BRLT (Bottom Right Left Top)

        CENTER      = 0,
        BOTTOM     ,
        RIGHT      ,
        BR_CORNER  ,
        LEFT       ,
        BL_CORNER  ,
        COL_MIDDLE ,
        U_BOTTOM   ,
        TOP        ,
        ROW_MIDDLE ,
        TR_CORNER  ,
        U_RIGHT    ,
        TL_CORNER  ,
        U_LEFT     ,
        U_TOP      ,
        NONE

        // 8 bits values
        // { 2 = 1, 8 = 2, 10 = 3, 11 = 4, 16 = 5, 18 = 6, 22 = 7, 24 = 8, 26 = 9, 27 = 10, 30 = 11, 31 = 12, 64 = 13, 66 = 14, 72 = 15, 74 = 16, 75 = 17, 80 = 18, 82 = 19, 86 = 20, 88 = 21, 90 = 22, 91 = 23, 94 = 24, 95 = 25, 104 = 26, 106 = 27, 107 = 28, 120 = 29, 122 = 30, 123 = 31, 126 = 32, 127 = 33, 208 = 34, 210 = 35, 214 = 36, 216 = 37, 218 = 38, 219 = 39, 222 = 40, 223 = 41, 248 = 42, 250 = 43, 251 = 44, 254 = 45, 255 = 46, 0 = 47 }
    };

    // tile infos
    struct tile_t {
        int id;
        vec2 texpos;
        vec2 texsize;
        bool operator== (const tile_t& o);
    };
    
    // tile atlas infos
    struct tileset_t
    {
        std::map<tiletype, tile_t> tiles { };
        int count;
        int map_value = 1;
        mx::texture_t atlas;
    };
    namespace tileset
    {
        // load tileset
        // MXRDEF tileset_t load_tileset_json(const ::std::string& tileset_path);
        MXRDEF tileset_t load_tileset(const ::std::string& atlas);

        // auto convert a atlas into tiles
        MXRDEF void auto_tiles(tileset_t* tileset, vec2 tile_count);
        // add a tile into the tileset
        MXRDEF void add_tile(tileset_t* tileset, vec2 pixelorigin, vec2 pixelsize, tiletype type);
    }

    // represent the info of a cell in a tilemap
    typedef struct
    {
        int base_value;
        int tile_id;
        int tileset_id;
    } tilemap_cell_t;

    // tilemap
    struct tilemap_t
    {
        int width;
        int height;
        int** data; // tab

        tileset_t tileset;
        vec2 tile_pixelsize;

        int default_value = 0;
        bool has_changed = false;

        mx::geometry_t tilebatch;

        vec3 pos;
    };
    namespace autotiling {
        // test the tile in north / west / east / south and return the sum of the bit values of the corresponding directions
        // into a tiletype which correspond to his position and context
        MXRDEF tiletype test_4(tilemap_t tilemap, vec2 tilepos, int tile_value);
        // MXRDEF int test_8(tilemap_t tilemap, tiletype tiletype, vec2 tilepos, int empty_tile_value);

        // MXRDEF tiletype test_4_multiple_terrains(tilemap_t tilemap, tiletype tiletype, vec2 tilepos, int empty_tile_value);
        // MXRDEF int test_8_multiple_terrains(tilemap_t tilemap, tiletype tiletype, vec2 tilepos, int empty_tile_value);
    }
    namespace tilemap
    {
        // create a tilemap
        MXRDEF tilemap_t create(int width, int height, mx::tileset_t tileset, vec2 tile_pixelsize = { 16.f, 16.f }, int default_value = 0);

        // generate the batch data for the tiles
        MXRDEF void generate_geometry(tilemap_t tilemap);

        // generate the batch data for the tiles with some rules
        MXRDEF void generate_geometry_with_autotiling(tilemap_t tilemap);

        // Return if the coord is in the tilemap bounds
        MXRDEF bool is_valid_cell(tilemap_t tilemap, uint32_t x, uint32_t y);

        // Set the value of a certain tile (if in bounds)
        MXRDEF void set_tile(tilemap_t* tilemap, uint32_t x, uint32_t y, int value);

        // Draw the (whole) tilemap to the screen
        MXRDEF void draw(tilemap_t tilemap, mx::shader_t shader);
    }
}

#endif //_MX_ADDONS_TILEMAP_H_