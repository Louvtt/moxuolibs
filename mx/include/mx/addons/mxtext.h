/*
MXLIBS. MX text is an addon of mxrender and is part of MXLIBS.
They are small(header-only or .h+.cpp files) libraries to make some graphical
experiments or small projetcs quickly and easily.
It let's you access anything 
Copyright (C) 2021 Lucyel Zanardo (@Louvtt)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _MX_ADDONS_TEXT_H_
#define _MX_ADDONS_TEXT_H_

#include "../mxrender.h"

#include <map>
#include <string>

namespace mx {

    struct fontchar_t {
        int sizex, sizey;
        int bearingx, bearingy;
        int advance;

        mx_vec2 uv0;
        mx_vec2 uv1;
    };

    struct font_t {
        std::string name;
        std::map<char, fontchar_t> characters;
        
        mx::texture_t tex;

        float scale_factor = 1.f;
        int baselineH = 0.f;
        int descent, ascent;
        int linegap = 1;
        int kerning = 2;
        int size = 32;
    };

    namespace font {
        MXRDEF font_t load(const std::string& name, int size);
        
        
        MXRDEF mx::geometry_t bake_text(mx::font_t font, const std::string& text);
    }
}

#endif //_MX_ADDONS_TEXT_H_