/*
MXLIBS. mx maths is part of MXLIBS.
They are small(header-only or .h+.cpp files) libraries to make some graphical
experiments or small projetcs quickly and easily.
It let's you access anything 
Copyright (C) 2021 Lucyel Zanardo (@Louvtt)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _MX_MATHS_H_
#define _MX_MATHS_H_

#define MXMATHS_VERSION 1
#define __mxmaths_h__

#ifdef __cplusplus
extern "C" {
#endif

//////////////////////////////////////////////////
// HEADER FILE
#if !defined(MXM_STATIC)
    #define MXMDEF static
#else
    #define MXMDEF extern
#endif

#ifndef MXM_PRECISION_DOUBLE
    #define FLOATING_TYPE float
    #define _F(x) x##f
    #define MXM_INT_TYPE int
#else 
    #define FLOATING_TYPE double
    #define _F(x) x
    #define MXM_INT_TYPE uint32_t
#endif

/////////////////
// VECTORS
// ivec<N>

typedef struct 
{
    int x, y;
} mx_ivec2;

typedef struct 
{
    int x, y, z;
} mx_ivec3;
/////////
// vec<N>

typedef struct
{
    FLOATING_TYPE x, y;
} mx_vec2;

struct mx_vec3
{
    FLOATING_TYPE x, y, z;

    inline mx_vec3 operator-(mx_vec3 p);
    inline mx_vec3 operator+(mx_vec3 p);
    inline mx_vec3 operator*(mx_vec3 p);
    inline mx_vec3 operator/(mx_vec3 p);

};

typedef struct
{
    FLOATING_TYPE x, y, z, w;
} mx_vec4;

////////////////////////
// USEFUL


MXMDEF FLOATING_TYPE   mx_interpolate     (FLOATING_TYPE a, FLOATING_TYPE b, FLOATING_TYPE t);
MXMDEF mx_vec2         mx_interpolate_vec2(mx_vec2 a, mx_vec2 b, FLOATING_TYPE t);
MXMDEF mx_vec3         mx_interpolate_vec3(mx_vec3 a, mx_vec3 b, FLOATING_TYPE t);
MXMDEF mx_vec4         mx_interpolate_vec4(mx_vec4 a, mx_vec4 b, FLOATING_TYPE t);

MXMDEF FLOATING_TYPE mx_smoothstep  (FLOATING_TYPE a, FLOATING_TYPE b, FLOATING_TYPE t);
MXMDEF FLOATING_TYPE mx_smootherstep(FLOATING_TYPE a, FLOATING_TYPE b, FLOATING_TYPE t);

MXMDEF FLOATING_TYPE mx_max(FLOATING_TYPE x, FLOATING_TYPE max);
MXMDEF FLOATING_TYPE mx_min(FLOATING_TYPE x, FLOATING_TYPE min);
MXMDEF FLOATING_TYPE mx_clamp(FLOATING_TYPE x, FLOATING_TYPE min, FLOATING_TYPE max);

MXMDEF FLOATING_TYPE mx_random();
MXMDEF FLOATING_TYPE mx_random_range(FLOATING_TYPE min, FLOATING_TYPE max);

////////////////////////
// VECTOR FUNCTIONS

MXMDEF mx_ivec2 mx_ivec2_create(int x, int y);
#define MX_IVEC2(x, y) mx_ivec2_create(x, y);
MXMDEF mx_ivec3 mx_ivec3_create(int x, int y, int z);
#define MX_IVEC3(x, y, z) mx_ivec3_create(x, y, z);

MXMDEF mx_vec2 mx_vec2_create(FLOATING_TYPE x, FLOATING_TYPE y);
#define MX_VEC2(x, y) mx_vec2_create(x, y);
MXMDEF mx_vec3 mx_vec3_create(FLOATING_TYPE x, FLOATING_TYPE y, FLOATING_TYPE z);
MXMDEF mx_vec3 mx_vec3_random();
MXMDEF mx_vec3 mx_vec3_random_range(FLOATING_TYPE min, FLOATING_TYPE max);
MXMDEF mx_vec3 mx_vec3_random_unit();
MXMDEF mx_vec3 mx_vec3_random_unit_sphere();
MXMDEF mx_vec3 mx_vec3_random_unit_disk();

#define MX_VEC3(x, y, z) mx_vec3_create(x, y, z);
MXMDEF mx_vec4 mx_vec4_create(FLOATING_TYPE x, FLOATING_TYPE y, FLOATING_TYPE z, FLOATING_TYPE w);
#define MX_VEC4(x, y, z, w) mx_vec4_create(x, y, z, w);

MXMDEF bool mx_near_zero(mx_vec3 v);

// UNPACK
#define MX_VEC2_UNPACK(v) v.x, v.y
#define MX_VEC3_UNPACK(v) v.x, v.y, v.z
#define MX_VEC4_UNPACK(v) v.x, v.y, v.z, v.w

// MAGNITUDES
MXMDEF FLOATING_TYPE mx_ivec2_mag(mx_ivec2 v);
MXMDEF FLOATING_TYPE mx_ivec3_mag(mx_ivec3 v);

MXMDEF FLOATING_TYPE mx_vec2_mag(mx_vec2 v);
MXMDEF FLOATING_TYPE mx_vec3_mag(mx_vec3 v);
MXMDEF FLOATING_TYPE mx_vec4_mag(mx_vec4 v);

// ADD
MXMDEF mx_ivec2 mx_ivec2_add(mx_ivec2 a, mx_ivec2 b);
MXMDEF mx_ivec3 mx_ivec3_add(mx_ivec3 a, mx_ivec3 b);

MXMDEF mx_vec2 mx_vec2_add(mx_vec2 a, mx_vec2 b);
MXMDEF mx_vec3 mx_vec3_add(mx_vec3 a, mx_vec3 b);
MXMDEF mx_vec4 mx_vec4_add(mx_vec4 a, mx_vec4 b);

// SUBSTRACT

MXMDEF mx_vec2 mx_vec2_sub(mx_vec2 a, mx_vec2 b);
MXMDEF mx_vec3 mx_vec3_sub(mx_vec3 a, mx_vec3 b);
MXMDEF mx_vec4 mx_vec4_sub(mx_vec4 a, mx_vec4 b);

// MULTIPLY
MXMDEF mx_ivec2 mx_ivec2_mul (mx_ivec2 a, mx_ivec2 b);
MXMDEF mx_ivec2 mx_ivec2_mulf(mx_ivec2 a, int b);
MXMDEF mx_ivec3 mx_ivec3_mul (mx_ivec3 a, mx_ivec3 b);
MXMDEF mx_ivec3 mx_ivec3_mulf(mx_ivec3 a, int b);

MXMDEF mx_vec2 mx_vec2_mul (mx_vec2 a, mx_vec2 b);
MXMDEF mx_vec2 mx_vec2_mulf(mx_vec2 a, FLOATING_TYPE b);
MXMDEF mx_vec3 mx_vec3_mul (mx_vec3 a, mx_vec3 b);
MXMDEF mx_vec3 mx_vec3_mulf(mx_vec3 a, FLOATING_TYPE b);
MXMDEF mx_vec4 mx_vec4_mul (mx_vec4 a, mx_vec4 b);
MXMDEF mx_vec4 mx_vec4_mulf(mx_vec4 a, FLOATING_TYPE b);

// DIVISION
MXMDEF mx_ivec2 mx_ivec2_div (mx_ivec2 a, mx_ivec2 b);
MXMDEF mx_ivec2 mx_ivec2_divf(mx_ivec2 a, int b);
MXMDEF mx_ivec3 mx_ivec3_div (mx_ivec3 a, mx_ivec3 b);
MXMDEF mx_ivec3 mx_ivec3_divf(mx_ivec3 a, int b);

MXMDEF mx_vec2 mx_vec2_div (mx_vec2 a, mx_vec2 b);
MXMDEF mx_vec2 mx_vec2_divf(mx_vec2 a, FLOATING_TYPE b);
MXMDEF mx_vec3 mx_vec3_div (mx_vec3 a, mx_vec3 b);
MXMDEF mx_vec3 mx_vec3_divf(mx_vec3 a, FLOATING_TYPE b);
MXMDEF mx_vec4 mx_vec4_div (mx_vec4 a, mx_vec4 b);
MXMDEF mx_vec4 mx_vec4_divf(mx_vec4 a, FLOATING_TYPE b);


// NORMALIZE
MXMDEF mx_vec2 mx_vec2_normalize(mx_vec2 v);
MXMDEF mx_vec3 mx_vec3_normalize(mx_vec3 v);
MXMDEF mx_vec4 mx_vec4_normalize(mx_vec4 v);

// DOT
MXMDEF FLOATING_TYPE mx_vec2_dot(mx_vec2 a, mx_vec2 b);
MXMDEF FLOATING_TYPE mx_vec3_dot(mx_vec3 a, mx_vec3 b);
MXMDEF FLOATING_TYPE mx_vec4_dot(mx_vec4 a, mx_vec4 b);

// CROSS
MXMDEF mx_vec3 mx_vec3_cross(mx_vec3 a, mx_vec3 b);

// ANGLE
MXMDEF FLOATING_TYPE mx_vec2_angle(mx_vec2 a, mx_vec2 b);
MXMDEF FLOATING_TYPE mx_vec3_angle(mx_vec3 a, mx_vec3 b);
MXMDEF FLOATING_TYPE mx_vec4_angle(mx_vec4 a, mx_vec4 b);

// 2D Rotation (Z axis rot)
MXMDEF mx_vec2 mx_vec2_rot2d(mx_vec2 a, FLOATING_TYPE angle);
MXMDEF mx_vec3 mx_vec3_rot2d(mx_vec3 a, FLOATING_TYPE angle);

// PROJECTION
MXMDEF mx_vec2 mx_vec2_proj(mx_vec2 a, mx_vec2 b);
MXMDEF mx_vec3 mx_vec3_proj(mx_vec3 a, mx_vec3 b);
MXMDEF mx_vec4 mx_vec4_proj(mx_vec4 a, mx_vec4 b);

// REFLECT
MXMDEF mx_vec2 mx_vec2_refl(mx_vec2 a, mx_vec2 b);
MXMDEF mx_vec3 mx_vec3_refl(mx_vec3 a, mx_vec3 b);
MXMDEF mx_vec4 mx_vec4_refl(mx_vec4 a, mx_vec4 b);



/////////////////
// MATRICES

typedef struct 
{
    FLOATING_TYPE m0, m1;
    FLOATING_TYPE m2, m3;
} mx_mat2;

typedef struct
{
    FLOATING_TYPE value[2][2];
    FLOATING_TYPE* ptr;
} mx_mat2_valueptr;

typedef struct
{
    FLOATING_TYPE m0,  m1,  m2,  m3;
    FLOATING_TYPE m4,  m5,  m6,  m7;
    FLOATING_TYPE m8,  m9,  m10, m11;
    FLOATING_TYPE m12, m13, m14, m15;
} mx_mat4;

typedef struct {
    FLOATING_TYPE value[4][4];
    FLOATING_TYPE* ptr;
} mx_mat4_valueptr;

typedef struct
{
    FLOATING_TYPE m0,  m1,  m2;
    FLOATING_TYPE m3,  m4,  m5;
    FLOATING_TYPE m6,  m7,  m8;
} mx_mat3;

typedef struct {
    FLOATING_TYPE value[3][3];
    FLOATING_TYPE* ptr;
} mx_mat3_valueptr;


/////////////
// MATRICES FUNC

MXMDEF mx_mat2 mx_mat2_create(FLOATING_TYPE v = _F(.0));
MXMDEF mx_mat3 mx_mat3_create(FLOATING_TYPE v = _F(.0));
MXMDEF mx_mat4 mx_mat4_create(FLOATING_TYPE v = _F(.0));

MXMDEF mx_mat2 mx_mat2_identity(FLOATING_TYPE v = _F(1.));
MXMDEF mx_mat3 mx_mat3_identity(FLOATING_TYPE v = _F(1.));
MXMDEF mx_mat4 mx_mat4_identity(FLOATING_TYPE v = _F(1.));


MXMDEF mx_mat2 mx_mat2_mul(mx_mat2 a, mx_mat2 b);
MXMDEF mx_mat2 mx_mat2_mulf(mx_mat2 m, FLOATING_TYPE f);
MXMDEF mx_mat3 mx_mat3_mul(mx_mat3 a, mx_mat3 b);
MXMDEF mx_mat3 mx_mat3_mulf(mx_mat3 m, FLOATING_TYPE f);
MXMDEF mx_mat4 mx_mat4_mul(mx_mat4 a, mx_mat4 b);
MXMDEF mx_mat4 mx_mat4_mulf(mx_mat4 m, FLOATING_TYPE f);

MXMDEF FLOATING_TYPE mx_mat2_det(mx_mat2 m);
MXMDEF FLOATING_TYPE mx_mat3_det(mx_mat3 m);
MXMDEF FLOATING_TYPE mx_mat4_det(mx_mat4 m);

/////////////////

MXMDEF mx_mat3 mx_mat3_translate(mx_mat3 m, mx_vec2 translation);
MXMDEF mx_mat4 mx_mat4_translate(mx_mat4 m, mx_vec3 translation);

MXMDEF mx_mat4 mx_mat4_get_rotX(FLOATING_TYPE angle);
MXMDEF mx_mat4 mx_mat4_get_rotY(FLOATING_TYPE angle);
MXMDEF mx_mat4 mx_mat4_get_rotZ(FLOATING_TYPE angle);
MXMDEF mx_mat3 mx_mat3_get_rotZ(FLOATING_TYPE angle);

MXMDEF mx_mat3 mx_mat3_rotate(mx_mat3 m, mx_vec3 axis, FLOATING_TYPE angle);
MXMDEF mx_mat4 mx_mat4_rotate(mx_mat4 m, mx_vec3 axis, FLOATING_TYPE angle);

MXMDEF mx_mat3 mx_mat3_scale(mx_mat3 m, mx_vec2 stretch);
MXMDEF mx_mat4 mx_mat4_scale(mx_mat4 m, mx_vec3 stretch);

MXMDEF mx_mat2 mx_mat2_transpose(mx_mat2 m);
MXMDEF mx_mat3 mx_mat3_transpose(mx_mat3 m);
MXMDEF mx_mat4 mx_mat4_transpose(mx_mat4 m);

MXMDEF mx_mat3 mx_mat4_minor(mx_mat4 m, int row, int col);
MXMDEF mx_mat4 mx_mat4_minormat(mx_mat4 m);
MXMDEF mx_mat4 mx_mat4_cofactor(mx_mat4 m);
MXMDEF mx_mat4 mx_mat4_inverse(mx_mat4 m);

/////////////////
// MAT FOR CAMERA

MXMDEF mx_mat4 mx_ortho_proj_(FLOATING_TYPE aspect_ratio, int size);
MXMDEF mx_mat4 mx_persp_proj(FLOATING_TYPE fov, FLOATING_TYPE near, FLOATING_TYPE far, FLOATING_TYPE aspect);

MXMDEF mx_mat4 mx_ortho_proj(FLOATING_TYPE right, FLOATING_TYPE left, FLOATING_TYPE top, FLOATING_TYPE bottom, FLOATING_TYPE near, FLOATING_TYPE far);

MXMDEF mx_mat2_valueptr mx_mat2_raw(mx_mat2 m);
MXMDEF mx_mat3_valueptr mx_mat3_raw(mx_mat3 m);
MXMDEF mx_mat4_valueptr mx_mat4_raw(mx_mat4 m);

///////////////////
// VEC AND MAT

MXMDEF mx_vec2 mx_m2v2_mul(mx_mat2 m, mx_vec2 v);
MXMDEF mx_vec3 mx_m3v3_mul(mx_mat3 m, mx_vec3 v);
MXMDEF mx_vec4 mx_m4v4_mul(mx_mat4 m, mx_vec4 v);

///////////////////
// COLORS

MXMDEF mx_vec3 mx_rgb2hsl(mx_vec3 rgb);
MXMDEF mx_vec3 mx_hsl2rgb(mx_vec3 hsl);

/////////////////
// ROTORS

// typedef struct
// {
//     FLOATING_TYPE x, y, z, w;
// } mx_rotor;

//////////////////////////
// DEBUG
MXMDEF void mx_vec3_string  (mx_vec3 v,  char** str);
MXMDEF void mx_mat4_string  (mx_mat4 m,  char** str);
// MXMDEF void mx_rotor_string (mx_rotor r, char** str);

MXMDEF void mx_log_mat4(mx_mat4 m);
MXMDEF void mx_log_vec3(mx_vec3 v);

#ifdef __cplusplus
}
#endif
// END HEADER FILE
//////////////////////////////////////////////////


#ifdef MXM_INCLUDE_IMPLEMENTATION
// DEBUG ASSERT
#ifndef MXM_ASSERT
    #include <assert.h>
    #define MXM_ASSERT(x) assert(x)
#endif //MXM_ASSERT

// EXTERN TYPE
#ifdef __cplusplus
    #define MXM_EXTERN extern "C"
#else
    #define MXM_EXTERN extern
#endif //__cplusplus

#include <cstdio>

// INLINE TYPE
#ifdef _MSC_VER
    #ifdef __cplusplus
        #define mxm_inline inline
    #else
        #define mxm_inline
    #endif //__cplusplus
#else
    #define mxm_inline __forceinline
#endif //_MSC_VER

////////////////////
// USEFUL FUNC

MXMDEF FLOATING_TYPE   mx_interpolate     (FLOATING_TYPE a,   FLOATING_TYPE b,   FLOATING_TYPE t)
{
    return (_F(1.) - t) * a + t * b;
}
MXMDEF mx_vec2 mx_interpolate_vec2(mx_vec2 a, mx_vec2 b, FLOATING_TYPE t)
{
    return mx_vec2_add(mx_vec2_mulf(a, _F(1.) - t), mx_vec2_mulf(b, t));
}
MXMDEF mx_vec3 mx_interpolate_vec3(mx_vec3 a, mx_vec3 b, FLOATING_TYPE t)
{
    return mx_vec3_add(mx_vec3_mulf(a, _F(1.) - t), mx_vec3_mulf(b, t));
}
MXMDEF mx_vec4 mx_interpolate_vec4(mx_vec4 a, mx_vec4 b, FLOATING_TYPE t)
{
    return mx_vec4_add(mx_vec4_mulf(a, _F(1.) - t), mx_vec4_mulf(b, t));
}

MXMDEF FLOATING_TYPE mx_smoothstep  (FLOATING_TYPE a, FLOATING_TYPE b, FLOATING_TYPE t)
{
    return (b - a) * (_F(3.) - t * _F(2.)) * t * t + a;
}
MXMDEF FLOATING_TYPE mx_smootherstep(FLOATING_TYPE a, FLOATING_TYPE b, FLOATING_TYPE t)
{
    return (b - a) * ((t * (t * _F(6.) - _F(15.)) + _F(10.)) * t * t * t) + a;
}

MXMDEF FLOATING_TYPE mx_max(FLOATING_TYPE x, FLOATING_TYPE max) {
    return (x > max) ? max : x;
}
MXMDEF FLOATING_TYPE mx_min(FLOATING_TYPE x, FLOATING_TYPE min) {
    return (x < min) ? min : x;
}
MXMDEF FLOATING_TYPE mx_clamp(FLOATING_TYPE x, FLOATING_TYPE min, FLOATING_TYPE max) {
    if (x > max) return max;
    if (x < min) return min;
    return x;
}

#include <random>

MXMDEF FLOATING_TYPE mx_random() {
    std::uniform_real_distribution<FLOATING_TYPE> d(_F(.0), _F(1.));
    static std::mt19937 generator;
    return d(generator);
}

MXMDEF FLOATING_TYPE mx_random_range(FLOATING_TYPE min, FLOATING_TYPE max) {
    return min + (max - min) * mx_random();
}


///////////////
// CREATION FUNC

#include <math.h>

MXMDEF mx_ivec2 mx_ivec2_create(int x, int y)
{
    mx_ivec2 res = {x, y};
    return res;
}
MXMDEF mx_ivec3 mx_ivec3_create(int x, int y, int z) 
{
    mx_ivec3 res = {x, y, z};
    return res;
}
MXMDEF mx_vec2 mx_vec2_create(FLOATING_TYPE  x, FLOATING_TYPE y) 
{
    mx_vec2 res = {x, y};
    return res;
}
MXMDEF mx_vec3 mx_vec3_create(FLOATING_TYPE x, FLOATING_TYPE y, FLOATING_TYPE z)
{
    mx_vec3 res = {x,y,z};
    return res;
}
MXMDEF mx_vec4 mx_vec4_create(FLOATING_TYPE x, FLOATING_TYPE y, FLOATING_TYPE z, FLOATING_TYPE w) 
{
    mx_vec4 res = {x, y, z, w};
    return res;
}

MXMDEF bool mx_near_zero(mx_vec3 v) {
    const FLOATING_TYPE s = 1e-8;
    return (fabs(v.x) < s) && (fabs(v.y) < s) && (fabs(v.z) < s);
}

///////////////
// RND FUNCS

MXMDEF mx_vec3 mx_vec3_random() {
    const FLOATING_TYPE x = mx_random_range(_F(-1.), _F(1.));
    const FLOATING_TYPE y = mx_random_range(_F(-1.), _F(1.));
    const FLOATING_TYPE z = mx_random_range(_F(-1.), _F(1.));
    return {
        (fabs(x) < _F(.01) ? _F(.1) : x),
        (fabs(y) < _F(.01) ? _F(.1) : y),
        (fabs(z) < _F(.01) ? _F(.1) : z)
    };
}
MXMDEF mx_vec3 mx_vec3_random_range(FLOATING_TYPE min, FLOATING_TYPE max) {
    return { mx_random_range(min, max), mx_random_range(min, max), mx_random_range(min, max) };
}
MXMDEF mx_vec3 mx_vec3_random_unit() {
    return mx_vec3_normalize(mx_vec3_random());
}
MXMDEF mx_vec3 mx_vec3_random_unit_sphere() {
    mx_vec3 r;
    while(true) {
        r = mx_vec3_random_range(_F(-1.), _F(1.));
        if (mx_vec3_dot(r, r) >= _F(1.)) continue;
        return r;
    }
}
MXMDEF mx_vec3 mx_vec3_random_unit_disk() {
    mx_vec3 p;
    while (true) {
        p = { mx_random_range(_F(-1.),_F(1.)), mx_random_range(_F(-1.),_F(1.)), _F(0.)};
        if (mx_vec3_dot(p, p) >= _F(1.)) continue;
        return p;
    }
}


///////////////
// MAG FUNCS

MXMDEF FLOATING_TYPE mx_ivec2_mag(mx_ivec2 v)
{
    return sqrt(FLOATING_TYPE(v.x * v.x + v.y * v.y));
}
MXMDEF FLOATING_TYPE mx_ivec3_mag(mx_ivec3 v)
{
    return sqrt(FLOATING_TYPE(v.x * v.x + v.y * v.y + v.z * v.z));
}

MXMDEF FLOATING_TYPE mx_vec2_mag(mx_vec2 v) 
{
    return sqrt(v.x * v.x + v.y * v.y);

}
MXMDEF FLOATING_TYPE mx_vec3_mag(mx_vec3 v)
{
    return sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}
MXMDEF FLOATING_TYPE mx_vec4_mag(mx_vec4 v)
{
    return sqrt(v.x * v.x + v.y * v.y + v.z * v.z + v.w * v.w);
}

// OPERATOR
mx_vec3 mx_vec3::operator-(mx_vec3 p) {
    return { x - p.x, y - p.y, z - p.z};
}
mx_vec3 mx_vec3::operator+(mx_vec3 p) {
    return { x + p.x, y + p.y, z + p.z };
}
mx_vec3 mx_vec3::operator*(mx_vec3 p) {
    return { x * p.x, y * p.y, z * p.z };
}
mx_vec3 mx_vec3::operator/(mx_vec3 p) {
    return { x / p.x, y / p.y, z / p.z };
}

// ADD
MXMDEF mx_ivec2 mx_ivec2_add(mx_ivec2 a, mx_ivec2 b)
{
    mx_ivec2 res = {a.x + b.x, a.y + b.y};
    return res;
}
MXMDEF mx_ivec3 mx_ivec3_add(mx_ivec3 a, mx_ivec3 b)
{
    mx_ivec3 res = { a.x + b.x, a.y + b.y, a.z + b.z };
    return res;
}

MXMDEF mx_vec2 mx_vec2_add(mx_vec2 a, mx_vec2 b)
{
    mx_vec2 res = { a.x + b.x, a.y + b.y };
    return res;
}
MXMDEF mx_vec3 mx_vec3_add(mx_vec3 a, mx_vec3 b)
{
    mx_vec3 res = { a.x + b.x, a.y + b.y, a.z + b.z };
    return res;
}
MXMDEF mx_vec4 mx_vec4_add(mx_vec4 a, mx_vec4 b)
{
    mx_vec4 res = { a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w };
    return res;
}

// SUBSTRACT

MXMDEF mx_vec2 mx_vec2_sub(mx_vec2 a, mx_vec2 b)
{
    mx_vec2 res = { a.x - b.x, a.y - b.y };
    return res;
}
MXMDEF mx_vec3 mx_vec3_sub(mx_vec3 a, mx_vec3 b)
{
    mx_vec3 res = { a.x - b.x, a.y - b.y, a.z - b.z };
    return res;
}
MXMDEF mx_vec4 mx_vec4_sub(mx_vec4 a, mx_vec4 b)
{
    mx_vec4 res = { a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w };
    return res;
}

// MULTIPLY
MXMDEF mx_ivec2 mx_ivec2_mul (mx_ivec2 a, mx_ivec2 b)
{
    mx_ivec2 res = {a.x * b.x, a.y * b.y};
    return res;
}
MXMDEF mx_ivec2 mx_ivec2_mulf(mx_ivec2 a, int b)
{
    mx_ivec2 res = {a.x * b, a.y * b};
    return res;
}
MXMDEF mx_ivec3 mx_ivec3_mul (mx_ivec3 a, mx_ivec3 b)
{
    mx_ivec3 res = {a.x * b.x, a.y * b.y, a.z * b.z };
    return res;
}
MXMDEF mx_ivec3 mx_ivec3_mulf(mx_ivec3 a, int b)
{
    mx_ivec3 res = {a.x * b, a.y * b, a.z * b };
    return res;
}

MXMDEF mx_vec2 mx_vec2_mul (mx_vec2 a, mx_vec2 b)
{
    mx_vec2 res = {a.x * b.x, a.y * b.y};
    return res;
}
MXMDEF mx_vec2 mx_vec2_mulf(mx_vec2 a, FLOATING_TYPE b)
{
    mx_vec2 res = {a.x * b, a.y * b};
    return res;
}
MXMDEF mx_vec3 mx_vec3_mul (mx_vec3 a, mx_vec3 b)
{
    mx_vec3 res = {a.x * b.x, a.y * b.y, a.z * b.z };
    return res;
}
MXMDEF mx_vec3 mx_vec3_mulf(mx_vec3 a, FLOATING_TYPE b)
{
    mx_vec3 res = {a.x * b, a.y * b, a.z * b };
    return res;
}
MXMDEF mx_vec4 mx_vec4_mul (mx_vec4 a, mx_vec4 b)
{
    mx_vec4 res = {a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w };
    return res;
}
MXMDEF mx_vec4 mx_vec4_mulf(mx_vec4 a, FLOATING_TYPE b)
{
    mx_vec4 res = {a.x * b, a.y * b, a.z * b, a.w * b};
    return res;
}

//////////////
// DIVISION
MXMDEF mx_ivec2 mx_ivec2_div (mx_ivec2 a, mx_ivec2 b) {
    mx_ivec2 res = {a.x / b.x, a.y / b.y};
    return res;
}
MXMDEF mx_ivec2 mx_ivec2_divf(mx_ivec2 a, int bb) {
    int b = 1 / bb;
    mx_ivec2 res = {a.x * b, a.y * b};
    return res;
}
MXMDEF mx_ivec3 mx_ivec3_div (mx_ivec3 a, mx_ivec3 b) {
    mx_ivec3 res = {a.x / b.x, a.y / b.y, a.z / b.z};
    return res;
}
MXMDEF mx_ivec3 mx_ivec3_divf(mx_ivec3 a, int bb) {
    int b = 1 / bb;
    mx_ivec3 res = {a.x * b, a.y * b, a.z * b};
    return res;
}

MXMDEF mx_vec2 mx_vec2_div (mx_vec2 a, mx_vec2 b) {
    mx_vec2 res = {a.x / b.x, a.y / b.y};
    return res;
}
MXMDEF mx_vec2 mx_vec2_divf(mx_vec2 a, FLOATING_TYPE bb)   {
    FLOATING_TYPE b = _F(1.) / bb;
    mx_vec2 res = {a.x * b, a.y * b};
    return res;
}
MXMDEF mx_vec3 mx_vec3_div (mx_vec3 a, mx_vec3 b) {
    mx_vec3 res = {a.x / b.x, a.y / b.y, a.z / b.z};
    return res;
}
MXMDEF mx_vec3 mx_vec3_divf(mx_vec3 a, FLOATING_TYPE bb)   {
    FLOATING_TYPE b = _F(1.) / bb;
    mx_vec3 res = {a.x * b, a.y * b, a.z * b};
    return res;
}
MXMDEF mx_vec4 mx_vec4_div (mx_vec4 a, mx_vec4 b) {
    mx_vec4 res = {a.x / b.x, a.y / b.y, a.z / b.z, a.w / b.w};
    return res;
}
MXMDEF mx_vec4 mx_vec4_divf(mx_vec4 a, FLOATING_TYPE bb)   {
    FLOATING_TYPE b = _F(1.) / bb;
    mx_vec4 res = {a.x * b, a.y * b, a.z * b, a.w * b};
    return res;
}

/////////////
// NORMALIZE
MXMDEF mx_vec2 mx_vec2_normalize(mx_vec2 v)
{
    return mx_vec2_mulf(v, _F(1.)/mx_vec2_mag(v));
}
MXMDEF mx_vec3 mx_vec3_normalize(mx_vec3 v)
{
    return mx_vec3_mulf(v, _F(1.)/mx_vec3_mag(v));
}
MXMDEF mx_vec4 mx_vec4_normalize(mx_vec4 v)
{
    return mx_vec4_mulf(v, _F(1.)/mx_vec4_mag(v));
}

/////////
// DOT
MXMDEF FLOATING_TYPE mx_vec2_dot(mx_vec2 a, mx_vec2 b)
{
    return a.x * b.x + a.y * b.y;
}
MXMDEF FLOATING_TYPE mx_vec3_dot(mx_vec3 a, mx_vec3 b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z;
}
MXMDEF FLOATING_TYPE mx_vec4_dot(mx_vec4 a, mx_vec4 b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
}
/////////
// CROSS
MXMDEF mx_vec3 mx_vec3_cross(mx_vec3 a, mx_vec3 b)
{
    mx_vec3 res = {
        a.y * b.z - a.z * b.y,
        a.z * b.x - a.x * b.z,
        a.x * b.y - a.y * b.x
    };
    return res;
}

////////
// ANGLE
#ifndef M_PI
    #define M_PI _F(3.14159265359)
#endif
#ifndef MX_DEG2RAD
    #define MX_DEG2RAD(a) ((a) * M_PI / _F(180.))
#endif
#ifndef MX_RAD2DEG
    #define MX_RAD2DEG(a) ((a) * _F(180.) / M_PI)
#endif

MXMDEF FLOATING_TYPE mx_vec2_angle(mx_vec2 a, mx_vec2 b)
{
    return acosf(mx_vec2_dot(a, b)/(mx_vec2_mag(a)*mx_vec2_mag(b)));
}
MXMDEF FLOATING_TYPE mx_vec3_angle(mx_vec3 a, mx_vec3 b)
{
    return acosf(mx_vec3_dot(a, b)/(mx_vec3_mag(a)*mx_vec3_mag(b)));
}
MXMDEF FLOATING_TYPE mx_vec4_angle(mx_vec4 a, mx_vec4 b)
{
    return acosf(mx_vec4_dot(a, b)/(mx_vec4_mag(a)*mx_vec4_mag(b)));
}

// ROTATION 2D (Z axis rot)
MXMDEF mx_vec2 mx_vec2_rot2d(mx_vec2 a, FLOATING_TYPE angle) {
    const float c = cos(angle);
    const float s = sin(angle);

    return {
        a.x * c - a.y * s,
        a.x * s + a.y * c
    };
}
MXMDEF mx_vec3 mx_vec3_rot2d(mx_vec3 a, FLOATING_TYPE angle) {
    const float c = cos(angle);
    const float s = sin(angle);

    return {
        a.x * c - a.y * s,
        a.x * s + a.y * c,
        a.z
    };
}

// PROJECTION
MXMDEF mx_vec2 mx_vec2_proj(mx_vec2 a, mx_vec2 b)
{
    mx_vec2 res;
    FLOATING_TYPE bm = mx_vec2_mag(b);
    res = mx_vec2_mulf(b, mx_vec2_dot(a, b) / (bm*bm));
    return res; 
}
MXMDEF mx_vec3 mx_vec3_proj(mx_vec3 a, mx_vec3 b)
{
    mx_vec3 res;
    FLOATING_TYPE bm = mx_vec3_mag(b);
    res = mx_vec3_mulf(b, mx_vec3_dot(a, b) / (bm*bm));
    return res;
}
MXMDEF mx_vec4 mx_vec4_proj(mx_vec4 a, mx_vec4 b)
{
    mx_vec4 res;
    FLOATING_TYPE bm = mx_vec4_mag(b);
    res = mx_vec4_mulf(b, mx_vec4_dot(a, b) / (bm*bm));
    return res; 
}

/////////////////////////////////////////////
// REFLECT

MXMDEF mx_vec2 mx_vec2_refl(mx_vec2 a, mx_vec2 b) {
    FLOATING_TYPE d = mx_vec2_dot(a, b);
    return mx_vec2_sub(a, mx_vec2_mulf(b, _F(2.)*d));
}
MXMDEF mx_vec3 mx_vec3_refl(mx_vec3 a, mx_vec3 b) {
    FLOATING_TYPE d = mx_vec3_dot(a, b);
    return mx_vec3_sub(a, mx_vec3_mulf(b, _F(2.)*d));
}
MXMDEF mx_vec4 mx_vec4_refl(mx_vec4 a, mx_vec4 b) {
    FLOATING_TYPE d = mx_vec4_dot(a, b);
    return mx_vec4_sub(a, mx_vec4_mulf(b, _F(2.)*d));
}


/////////////////////////////////////////////
// MATRICES

MXMDEF mx_mat2 mx_mat2_create(FLOATING_TYPE v)
{
    return {
        v, v,
        v, v
    };
}

MXMDEF mx_mat3 mx_mat3_create(FLOATING_TYPE v)
{
    mx_mat3 res;
    res.m0  = v; res.m1  = v; res.m2  = v;
    res.m3  = v; res.m4  = v; res.m5  = v;
    res.m6  = v; res.m7  = v; res.m8  = v;
    return res;
}

MXMDEF mx_mat4 mx_mat4_create(FLOATING_TYPE v)
{
    mx_mat4 res;
    res.m0  = v;  res.m1  = v;  res.m2  = v;  res.m3  = v;
    res.m4  = v;  res.m5  = v;  res.m6  = v;  res.m7  = v;
    res.m8  = v;  res.m9  = v;  res.m10 = v;  res.m11 = v;
    res.m12 = v;  res.m13 = v;  res.m14 = v;  res.m15 = v;
    return res;
}

MXMDEF mx_mat2 mx_mat2_identity(FLOATING_TYPE v)
{
    return {
             v, _F(.0),
        _F(.0),      v
    };
}

MXMDEF mx_mat4 mx_mat4_identity(FLOATING_TYPE v)
{
    mx_mat4 res;
    res.m0  =      v;  res.m1  = _F(.0);  res.m2  = _F(.0);  res.m3  = _F(.0);
    res.m4  = _F(.0);  res.m5  =      v;  res.m6  = _F(.0);  res.m7  = _F(.0);
    res.m8  = _F(.0);  res.m9  = _F(.0);  res.m10 =      v;  res.m11 = _F(.0);
    res.m12 = _F(.0);  res.m13 = _F(.0);  res.m14 = _F(.0);  res.m15 =      v;
    return res;
}
MXMDEF mx_mat3 mx_mat3_identity(FLOATING_TYPE v)
{
    mx_mat3 res;
    res.m0  =      v; res.m1  = _F(.0); res.m2  = _F(.0);
    res.m3  = _F(.0); res.m4  =      v; res.m5  = _F(.0);
    res.m6  = _F(.0); res.m7  = _F(.0); res.m8  =      v;
    return res;
}

MXMDEF mx_mat4 mx_mat4_mul(mx_mat4 a, mx_mat4 b)
{
    mx_mat4 res;
    // A LINE * B COLUMN
    // LINE 0
    res.m0  = a.m0 * b.m0 + a.m1 * b.m4 + a.m2 * b.m8  + a.m3 * b.m12;
    res.m1  = a.m0 * b.m1 + a.m1 * b.m5 + a.m2 * b.m9  + a.m3 * b.m13;
    res.m2  = a.m0 * b.m2 + a.m1 * b.m6 + a.m2 * b.m10 + a.m3 * b.m14;
    res.m3  = a.m0 * b.m3 + a.m1 * b.m7 + a.m2 * b.m11 + a.m3 * b.m15;
    // LINE 1
    res.m4  = a.m4 * b.m0 + a.m5 * b.m4 + a.m6 * b.m8  + a.m7 * b.m12;
    res.m5  = a.m4 * b.m1 + a.m5 * b.m5 + a.m6 * b.m9  + a.m7 * b.m13;
    res.m6  = a.m4 * b.m2 + a.m5 * b.m6 + a.m6 * b.m10 + a.m7 * b.m14;
    res.m7  = a.m4 * b.m3 + a.m5 * b.m7 + a.m6 * b.m11 + a.m7 * b.m15;
    // LINE 2
    res.m8  = a.m8 * b.m0 + a.m9 * b.m4 + a.m10 * b.m8  + a.m11 * b.m12;
    res.m9  = a.m8 * b.m1 + a.m9 * b.m5 + a.m10 * b.m9  + a.m11 * b.m13;
    res.m10 = a.m8 * b.m2 + a.m9 * b.m6 + a.m10 * b.m10 + a.m11 * b.m14;
    res.m11 = a.m8 * b.m3 + a.m9 * b.m7 + a.m10 * b.m11 + a.m11 * b.m15;
    // LINE 3
    res.m12 = a.m12 * b.m0 + a.m13 * b.m4 + a.m14 * b.m8  + a.m15 * b.m12;
    res.m13 = a.m12 * b.m1 + a.m13 * b.m5 + a.m14 * b.m9  + a.m15 * b.m13;
    res.m14 = a.m12 * b.m2 + a.m13 * b.m6 + a.m14 * b.m10 + a.m15 * b.m14;
    res.m15 = a.m12 * b.m3 + a.m13 * b.m7 + a.m14 * b.m11 + a.m15 * b.m15;
    return res;
}
MXMDEF mx_mat4 mx_mat4_mulf(mx_mat4 m, FLOATING_TYPE f)
{
    mx_mat4 res;
    res.m0  = m.m0  * f;  res.m1  = m.m1  * f;  res.m2  = m.m2  * f;  res.m3  = m.m3  * f;
    res.m4  = m.m4  * f;  res.m5  = m.m5  * f;  res.m6  = m.m6  * f;  res.m7  = m.m7  * f;
    res.m8  = m.m8  * f;  res.m9  = m.m9  * f;  res.m10 = m.m10 * f;  res.m11 = m.m11 * f;
    res.m12 = m.m12 * f;  res.m13 = m.m13 * f;  res.m14 = m.m14 * f;  res.m15 = m.m15 * f;
    return res;
}

MXMDEF mx_mat2 mx_mat2_mul(mx_mat2 a, mx_mat2 b)
{
    mx_mat2 res;
    // LINE 0
    res.m0 = a.m0 * b.m0 + a.m1 * b.m2;
    res.m1 = a.m0 * b.m1 + a.m1 * b.m3;
    // LINE 1
    res.m2 = a.m2 * b.m0 + a.m3 * b.m2;
    res.m3 = a.m2 * b.m1 + a.m3 * b.m3;
    return res;
}
MXMDEF mx_mat2 mx_mat2_mulf(mx_mat2 m, FLOATING_TYPE f)
{
    return {
        m.m0 * f, m.m1 * f,
        m.m2 * f, m.m3 * f
    };
}

MXMDEF mx_mat3 mx_mat3_mul(mx_mat3 a, mx_mat3 b)
{
    mx_mat3 res;
    // A LINE * B COLUMN
    // LINE 0
    res.m0 = a.m0 * b.m0 + a.m1 * b.m3 + a.m2 * b.m6;
    res.m1 = a.m0 * b.m1 + a.m1 * b.m4 + a.m2 * b.m7;
    res.m2 = a.m0 * b.m2 + a.m1 * b.m5 + a.m2 * b.m8;
    // LINE 1
    res.m3 = a.m3 * b.m0 + a.m4 * b.m3 + a.m5 * b.m6;
    res.m4 = a.m3 * b.m1 + a.m4 * b.m4 + a.m5 * b.m7;
    res.m5 = a.m3 * b.m2 + a.m4 * b.m5 + a.m5 * b.m8;
    // LINE 2
    res.m6 = a.m6 * b.m0 + a.m7 * b.m3 + a.m8 * b.m6;
    res.m7 = a.m6 * b.m1 + a.m7 * b.m4 + a.m8 * b.m7;
    res.m8 = a.m6 * b.m2 + a.m7 * b.m5 + a.m8 * b.m8;
    return res;
}
MXMDEF mx_mat3 mx_mat3_mulf(mx_mat3 m, FLOATING_TYPE f)
{
    mx_mat3 res;
    res.m0  = m.m0  * f;  res.m1  = m.m1  * f;  res.m2  = m.m2  * f;
    res.m3  = m.m3  * f;  res.m4  = m.m4  * f;  res.m5  = m.m5  * f;
    res.m6  = m.m6  * f;  res.m7  = m.m7  * f;  res.m8  = m.m8  * f;
    return res;
}

MXMDEF FLOATING_TYPE mx_mat2_det(mx_mat2 m)
{
    // | a b |
    // | c d | => ad-cb
    FLOATING_TYPE r = m.m1 * m.m3 - m.m2 * m.m1;
    return r;
}

MXMDEF FLOATING_TYPE mx_mat3_det(mx_mat3 m)
{
    FLOATING_TYPE r = _F(.0);
    // | a b c |
    // | d e f | = a[ei-hf]-b[di-gf]+c[dh-ge]
    // | g h i |
    r  = m.m0 * (m.m4 * m.m8 - m.m7 * m.m5)
       - m.m1 * (m.m3 * m.m8 - m.m6 * m.m5)
       + m.m2 * (m.m3 * m.m7 - m.m6 * m.m4);
    return r;

}
MXMDEF FLOATING_TYPE mx_mat4_det(mx_mat4 m)
{
    // | a 'b' c d |     | m0  m1  m2  m3 |
    // | e 'f' g h |     | m4  m5  m6  m7 |
    // | i 'j' k l | <=> | m8  m9  m10 m11|
    // | l 'm' n o |     | m12 m13 m14 m15|
    // Separate into 4 sub 3x3 matrices
    mx_mat3 mb = {
        m.m4,  m.m6,  m.m7,
        m.m8,  m.m10, m.m11,
        m.m12, m.m14, m.m15
    };
    mx_mat3 mf = {
        m.m0,  m.m2,  m.m3,
        m.m8,  m.m10, m.m11,
        m.m12, m.m14, m.m15
    };
    mx_mat3 mj = {
        m.m0,  m.m2, m.m3,
        m.m4,  m.m6,  m.m7,
        m.m12, m.m14, m.m15
    };
    mx_mat3 mm = {
        m.m0,  m.m2,  m.m3,
        m.m4,  m.m6,  m.m7,
        m.m8,  m.m10, m.m11,
    };

    FLOATING_TYPE r =
      - m.m1  * mx_mat3_det(mb) 
      + m.m5  * mx_mat3_det(mf)
      - m.m9  * mx_mat3_det(mj)
      + m.m13 * mx_mat3_det(mm);
    return r;
}

///////////////////////////

MXMDEF mx_mat3 mx_mat3_translate(mx_mat3 m, mx_vec2 translation)
{
    mx_mat3 n = { m };
    n.m6 += translation.x;
    n.m7 += translation.y;
    return n;
}
MXMDEF mx_mat4 mx_mat4_translate(mx_mat4 m, mx_vec3 translation)
{
    mx_mat4 n = { m };
    n.m3  += translation.x;
    n.m7  += translation.y;
    n.m11 += translation.z;

    return n;
}
//////////////////////////
// ROTATIONS MATRIX

// X axis
MXMDEF mx_mat4 mx_mat4_get_rotX(FLOATING_TYPE angle)
{
    return {
         _F(1.),         _F(.0),         _F(0.), _F(0.),
         _F(0.), cos(angle),-sin(angle), _F(0.),
         _F(0.), sin(angle), cos(angle), _F(0.),
         _F(0.),         _F(.0),         _F(0.), _F(1.)
    };
}
// Y axis
MXMDEF mx_mat4 mx_mat4_get_rotY(FLOATING_TYPE angle)
{
    return {
         cos(angle), _F(0.), sin(angle), _F(0.),
         _F(0.),         _F(1.),         _F(0.), _F(0.),
        -sin(angle), _F(0.), cos(angle), _F(0.),
         _F(0.),         _F(0.),         _F(0.), _F(1.)
    };
}

// Z axis
MXMDEF mx_mat4 mx_mat4_get_rotZ(FLOATING_TYPE angle)
{
    return {
         cos(angle),-sin(angle), _F(0.), _F(0.),
         sin(angle), cos(angle), _F(0.), _F(0.),
                 _F(0.),         _F(.0), _F(1.), _F(0.),
                 _F(0.),         _F(.0), _F(0.), _F(1.)
    };
}
MXMDEF mx_mat3 mx_mat3_get_rotZ(FLOATING_TYPE angle)
{
    return {
         cos(angle), sin(angle), _F(0.),
        -sin(angle), cos(angle), _F(0.),
                 _F(.0),         _F(.0), _F(1.),
    };
}

MXMDEF mx_mat3 mx_mat3_rotate(mx_mat3 m, mx_vec3 axis, FLOATING_TYPE angle)
{
    mx_mat3 n = { m };
    n = mx_mat3_mul(n, mx_mat3_get_rotZ(angle));
    return n;
}
MXMDEF mx_mat4 mx_mat4_rotate(mx_mat4 m, mx_vec3 axis, FLOATING_TYPE angle)
{
    mx_mat4 n = { m };
    // Translate to origin
    mx_vec3 otranslation = { n.m12, n.m13, n.m14 };
    n.m12 = _F(.0);
    n.m13 = _F(.0);
    n.m14 = _F(.0);

    // Rotate y
    FLOATING_TYPE inv_z = _F(1.) / axis.z;
    mx_mat4 rotY = mx_mat4_get_rotY(atan(axis.x * inv_z));
    mx_mat4 rotX = mx_mat4_get_rotX(atan(axis.y * inv_z));
    // project to z plane
    n = mx_mat4_mul(n, rotY);
    n = mx_mat4_mul(n, rotX);
    n = mx_mat4_mul(n, mx_mat4_get_rotZ(angle));
    // reflect to original plane
    n = mx_mat4_mul(n, mx_mat4_mulf(rotX, -_F(1.)));
    n = mx_mat4_mul(n, mx_mat4_mulf(rotY, -_F(1.)));

    n.m12 = otranslation.x;
    n.m13 = otranslation.y;
    n.m14 = otranslation.z;
    return n;
}

MXMDEF mx_mat3 mx_mat3_scale(mx_mat3 m, mx_vec2 scale)
{
    mx_mat3 n = { m };
    n.m0 *= scale.x;
    n.m4 *= scale.y;
    return n;
}
MXMDEF mx_mat4 mx_mat4_scale(mx_mat4 m, mx_vec3 scale)
{
    mx_mat4 n = { m };
    n.m0  *= scale.x;
    n.m5  *= scale.y;
    n.m10 *= scale.z;
    return n;
}

MXMDEF mx_mat2 mx_mat2_transpose(mx_mat2 m)
{
    return {
        m.m0, m.m2,
        m.m1, m.m3
    };
}
MXMDEF mx_mat3 mx_mat3_transpose(mx_mat3 m)
{
    return {
        m.m0, m.m3, m.m6,
        m.m1, m.m4, m.m7,
        m.m2, m.m5, m.m8
    };
}

MXMDEF mx_mat4 mx_mat4_transpose(mx_mat4 m)
{ 
    // | m0  m1  m2  m3  |     | m0  m4  m8  m12 |
    // | m4  m5  m6  m7  |     | m1  m5  m9  m13 |
    // | m8  m9  m10 m11 | <=> | m2  m6  m10 m14 |
    // | m12 m13 m14 m15 |     | m3  m7  m11 m15 |
    return {
        m.m0, m.m4, m.m8,  m.m12,
        m.m1, m.m5, m.m9,  m.m13,
        m.m2, m.m6, m.m10, m.m14,
        m.m3, m.m7, m.m11, m.m15
    };
}

MXMDEF mx_mat3 mx_mat4_minor(mx_mat4 m, int row, int col)
{
    mx_mat4_valueptr vm = mx_mat4_raw(m);
    FLOATING_TYPE values[3][3];
    for(int i = 0; i < 4; ++i)
    {
        for(int j = 0; j < 4; ++j)
        {
            if(i != row && j != col)
            {
                int x = i - (int)(i > row);
                int y = j - (int)(j > col);
                values[x][y] = vm.value[i][j];
            }
        }
    }
    return {
        values[0][0], values[0][1], values[0][2],
        values[1][0], values[1][1], values[1][2],
        values[2][0], values[2][1], values[2][2]
    };
}
MXMDEF mx_mat4 mx_mat4_minormat(mx_mat4 m)
{
    FLOATING_TYPE values[4][4];
    for(int i = 0; i < 4; ++i)
        for(int j = 0; j < 4; ++j)
            values[i][j] = mx_mat3_det(mx_mat4_minor(m, i, j));
    return {
        values[0][0], values[0][1], values[0][2], values[0][3],
        values[1][0], values[1][1], values[1][2], values[1][3],
        values[2][0], values[2][1], values[2][2], values[2][3],
        values[3][0], values[3][1], values[3][2], values[3][3]
    };
}

MXMDEF mx_mat4 mx_mat4_cofactor(mx_mat4 m)
{
    mx_mat4_valueptr mm = mx_mat4_raw(m);
    for(int i = 0; i < 4; ++i)
        for(int j = 0; j < 4; ++j)
            mm.value[i][j] *= (i + j & 1)? -1 : 1; // -1 pow i + j
    return {
        mm.value[0][0], mm.value[0][1], mm.value[0][2], mm.value[0][3],
        mm.value[1][0], mm.value[1][1], mm.value[1][2], mm.value[1][3],
        mm.value[2][0], mm.value[2][1], mm.value[2][2], mm.value[2][3],
        mm.value[3][0], mm.value[3][1], mm.value[3][2], mm.value[3][3]
    };
}

MXMDEF mx_mat4 mx_mat4_inverse(mx_mat4 m)
{
    //  _1__ * Adj(A)
    // | A | 
    mx_mat4 minor    = mx_mat4_minormat(m);
    mx_mat4 cofactor = mx_mat4_cofactor(minor);
    mx_mat4 adj      = mx_mat4_transpose(cofactor);
    return mx_mat4_mulf(adj, _F(1.) / mx_mat4_det(m));
}


// CAMERA MATRICES

MXMDEF mx_mat4 mx_ortho_proj_(FLOATING_TYPE aspect_ratio, int size)
{
    size = mx_max(_F(.1), size);
    mx_mat4 cam = mx_mat4_identity();
    cam.m0  = _F(1.) / (size * aspect_ratio);
    cam.m5  = _F(1.) / size;
    cam.m10 = _F(0.);
    return cam;
}

MXMDEF mx_mat4 mx_ortho_proj(FLOATING_TYPE l, FLOATING_TYPE r, FLOATING_TYPE b, FLOATING_TYPE t, FLOATING_TYPE n = .0f, FLOATING_TYPE f = 2.f)
{
    return {
        _F(2.) / (r - l),           _F(0.),            _F(0.), -(r+l) / (r-l),
                  _F(0.), _F(2.) / (t - b),            _F(0.), -(t+b) / (t-b),
                  _F(0.),           _F(0.), -_F(2.) / (f - n), -(f+n) / (f-n),
                  _F(0.),           _F(0.),            _F(0.),         _F(1.)
    };
}


MXMDEF mx_mat4 mx_persp_proj(FLOATING_TYPE near, FLOATING_TYPE far, FLOATING_TYPE fov)
{
    mx_mat4 cam = mx_mat4_identity();
    FLOATING_TYPE scale = _F(1.) / tan(MX_DEG2RAD(fov*_F(.5)));
    FLOATING_TYPE z_mapping = -far / (far - near);
    cam.m0  = scale;           // scale x
    cam.m5  = scale;           // scale y
    cam.m10 = z_mapping;       // z [0; 1   ]
    cam.m14 = near*z_mapping;  // z [0; 1]
    cam.m11 = -_F(1.);            // w = -z
    return cam;
}

MXMDEF mx_mat2_valueptr mx_mat2_raw(mx_mat2 m)
{
    mx_mat2_valueptr valueptr = {
    {
        {m.m0, m.m1},
        {m.m2, m.m3}
    },
        NULL
    };

    valueptr.ptr = &valueptr.value[0][0];
    return valueptr;
}

MXMDEF mx_mat3_valueptr mx_mat3_raw(mx_mat3 m)
{
    mx_mat3_valueptr valueptr = {
    {
        {m.m0, m.m3, m.m6},
        {m.m1, m.m4, m.m7},
        {m.m2, m.m5, m.m8}
    },
        NULL
    };

    valueptr.ptr = &valueptr.value[0][0];
    return valueptr;
}
MXMDEF mx_mat4_valueptr mx_mat4_raw(mx_mat4 m)
{
    
    mx_mat4_valueptr valueptr = {
    {
        {m.m0, m.m4, m.m8,  m.m12},
        {m.m1, m.m5, m.m9,  m.m13},
        {m.m2, m.m6, m.m10, m.m14},
        {m.m3, m.m7, m.m11, m.m15} 
    },
        NULL
    };

    valueptr.ptr = &valueptr.value[0][0];
    return valueptr;
}


MXMDEF mx_vec2 mx_m2v2_mul(mx_mat2 m, mx_vec2 v)
{
    return {
        m.m0 * v.x + m.m1 * v.y,
        m.m2 * v.x + m.m3 * v.y
    };
}
MXMDEF mx_vec3 mx_m3v3_mul(mx_mat3 m, mx_vec3 v)
{
    return {
        m.m0 * v.x + m.m1 * v.y + m.m2 * v.z,
        m.m3 * v.x + m.m4 * v.y + m.m5 * v.z,
        m.m6 * v.x + m.m7 * v.y + m.m8 * v.z
    };
}
MXMDEF mx_vec4 mx_m4v4_mul(mx_mat4 m, mx_vec4 v)
{
    return {
        m.m0  * v.x + m.m1  * v.y + m.m2  * v.z + m.m3  * v.w,
        m.m4  * v.x + m.m5  * v.y + m.m6  * v.z + m.m7  * v.w,
        m.m8  * v.x + m.m9  * v.y + m.m10 * v.z + m.m11 * v.w,
        m.m12 * v.x + m.m13 * v.y + m.m14 * v.z + m.m15 * v.w
    };
}

///////////////////////
// COLOR


MXMDEF mx_vec3 mx_rgb2hsl(mx_vec3 rgb) {
    FLOATING_TYPE M = mx_max(rgb.x, mx_max(rgb.y, rgb.z));
    FLOATING_TYPE m = mx_min(rgb.x, mx_min(rgb.y, rgb.z));
    FLOATING_TYPE c = M - m; // chroma

    FLOATING_TYPE Hp = _F(0.);
    if(M == rgb.x) Hp = (rgb.y - rgb.z) / c + 0;
    if(M == rgb.y) Hp = (rgb.z - rgb.x) / c + 2;
    if(M == rgb.z) Hp = (rgb.x - rgb.y) / c + 4;;
    Hp = (MXM_INT_TYPE)Hp % 6;
    // hue
    FLOATING_TYPE h = _F(60.) * Hp;

    // lightness
    FLOATING_TYPE l = M;
    // Saturation
    FLOATING_TYPE s = (M == _F(0.))? _F(0.) : c / M;

    return mx_vec3{h, s, l};
}
MXMDEF mx_vec3 mx_hsl2rgb(mx_vec3 hsl) {
    FLOATING_TYPE c  = hsl.z * hsl.y;
    FLOATING_TYPE Hp = hsl.x / _F(60.);
    FLOATING_TYPE x = c * (1 - abs((MXM_INT_TYPE)Hp % 2 - 1));

    FLOATING_TYPE m = hsl.z - c;

    if(0 <= Hp && Hp < 1) return {c, x, _F(0.)};
    if(Hp < 2) return {x, c, _F(0.)};
    if(Hp < 3) return {_F(0.), c, x};
    if(Hp < 4) return {_F(0.), x, c};
    if(Hp < 5) return {x, _F(0.), c};
    if(Hp < 6) return {c, _F(0.), x};

    return mx_vec3{};
}

///////////
// DEBUG

#include <string.h>
#if defined(__WIN32) || defined(__WIN64)
    #define __mxm_prnbuf(s, mlen, f, ...) _snprintf_s(s, mlen, f, __VA_ARGS__)
    #define __mxm_log(...) printf_s(__VA_ARGS__)
#else
    #define __mxm_prnbuf(s, mlen, f, ...) snprintf(s, mlen, f, __VA_ARGS__)
    #define __mxm_log(...) printf(__VA_ARGS__)
#endif // win

MXMDEF void mx_vec3_string  (mx_vec3 v,  char** str)
{
    char buffer[512];
    int ret = __mxm_prnbuf(buffer, sizeof(buffer), "{x: %f, y: %f, z: %f}", v.x, v.y, v.z);
    MXM_ASSERT(ret != EXIT_FAILURE);
    *str = buffer;
}

MXMDEF void mx_mat4_string  (mx_mat4 m,  char** str)
{
    char buffer[2048];
    int ret = __mxm_prnbuf(buffer, sizeof(buffer),
    "| %f %f %f %f |\n| %f %f %f %f |\n| %f %f %f %f |\n| %f %f %f %f |",
    m.m0, m.m1, m.m2, m.m3,
    m.m4, m.m5, m.m6, m.m7,
    m.m8, m.m9, m.m10,m.m11,
    m.m12,m.m13,m.m14,m.m15);
    *str = buffer;
}

MXMDEF void mx_log_mat4(mx_mat4 m)
{
    mx_mat4_valueptr matData = mx_mat4_raw(m);
    __mxm_log(
    "MAT4x4:\n| %f %f %f %f |\n| %f %f %f %f |\n| %f %f %f %f |\n| %f %f %f %f |\n\n",
    matData.value[0][0], matData.value[0][1], matData.value[0][2], matData.value[0][3],
    matData.value[1][0], matData.value[1][1], matData.value[1][2], matData.value[1][3],
    matData.value[2][0], matData.value[2][1], matData.value[2][2], matData.value[2][3],
    matData.value[3][0], matData.value[3][1], matData.value[3][2], matData.value[3][3]
    );
}

MXMDEF void mx_log_vec3(mx_vec3 v) {
    __mxm_log("VEC3: | %f %f %f |\n", v.x, v.y, v.z);
}

#endif //MXM_INCLUDE_IMPLEMENTATION
#endif //_MX_MATHS_H_