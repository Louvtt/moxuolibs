/*
MXLIBS. mx rsx is part of MXLIBS.
They are small(header-only or .h+.cpp files) libraries to make some graphical
experiments or small projetcs quickly and easily.
It let's you access anything 
Copyright (C) 2021 Lucyel Zanardo (@Louvtt)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _MX_RSX_H_
#define _MX_RSX_H_

// #define MX_RSX_VERSION 1

#if !defined(MX_RSX_STATIC)
    #define MXRSXDEF static
#else
    #define MXRSXDEF extern
#endif

#include <stdio.h>
#include <string>
#include <vector>
#include <map>

namespace mx
{

    ////////////////////////////////////////

    enum class ressource_type : int {
        // System types

        // if the format isn't recognised
        UNKNOW = 0,
        FOLDER, // to store file with folder info to dump the memory to for the next time
        COMPRESSED_FOLDER,
        APPLICATION,

        // Image Types

        IMAGE_PNG,
        IMAGE_JPEG,
        IMAGE_BMP,
        IMAGE_PPM,

        // SHADER TYPES

        SHADER_VERTEX,
        SHADER_FRAGMENT,
        SHADER_COMPUTE,
        SHADER_PLAIN,   // could be cool

        // TEXT TYPE

        TEXT_PLAIN,
        TEXT_YAML,
        TEXT_MARKDOWN, // not supported (it's for example)
        TEXT_JSON,

        // FONT FILES

        FONT_TTF,

        // LEAVE IT AT THE END
        // total count of ressource_type
        TYPE_LENGTH
    };

    typedef time_t timestamp_t;
    struct ressource_t {
        // name of the file
        std::string name;
        // location of the file
        std::string file_location;
        // type of the file
        ressource_type type = ressource_type::UNKNOW;
        // last time stamp it was modified (stored in the file system data i think)
        timestamp_t last_write_time;
        // does the ressource is a dir
        bool is_dir = false;
        

        // just data in bytes
        uint8_t* plain_data;
    };
    namespace ressource {
        MXRSXDEF ressource_t load(const std::string& path);
        MXRSXDEF ressource_t load(const std::string& file, const std::string& location);

        // MXRSXDEF bool read_data(ressource_t& rsx);
    }
    
    ////////////////////////////////////////

    struct directory_t {
        const std::string path;
        const std::string name;

        std::vector<ressource_t> files{};
        std::vector<directory_t> subdirs{};
        directory_t *parent = nullptr;
    };
    namespace directory {
        MXRSXDEF std::vector<ressource_t> list(const std::string& path);
        MXRSXDEF directory_t load(const std::string& path);
    }

    ////////////////////////////////////////

    struct rsxmanager_t {
        const std::string origin;
        std::map<ressource_type, std::map<std::string,ressource_t>> ressources;
    };

    namespace rsxmanager {
        MXRSXDEF rsxmanager_t create(const std::string& path);
        MXRSXDEF void           list(const rsxmanager_t& rsxm);
    }

    //////////////////////////////////////////

    #define MXRSX_MAX_PATH 1024

    MXRSXDEF std::string    cwd();
    MXRSXDEF std::string    filename(const std::string& path);
    MXRSXDEF std::string    file_ext(const std::string& path);
    MXRSXDEF ressource_type ext_to_type(const std::string& ext);
    MXRSXDEF std::string    absolute_path(const std::string& path);
    MXRSXDEF bool           is_directory(const std::string& path);
    MXRSXDEF timestamp_t    get_write_time(const std::string& path);
    MXRSXDEF int filesize(const std::string& path);
    
    //////////////////////////////////////////
} // namespace mx

///////////////////////////////////////////////////////::

#ifdef MX_IMPLEMENTATION_RSX


#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>

#if defined(__unix__) || defined(__linux__)
    #define MX_RSX_UNIX
    #include <unistd.h>
#elif defined(_WIN32) || defined(WIN32) || defined(_WIN64) || defined(WIN64)
    #define MX_RSX_WIN
    #include <windows.h>
    // to use stat with visual studio
    #define stat _stat
#endif

////////////////////////////////////
// File path manipulation

MXRSXDEF std::string mx::cwd()
{
    char buffer[MXRSX_MAX_PATH];

    // getters
    #ifdef MX_RSX_UNIX
    if(getcwd(buffer, sizeof(buffer)) != NULL)
    {
    #elif defined(MX_RSX_WIN)
    GetModuleFileName( NULL, buffer, MXRSX_MAX_PATH );
    #endif
    
    // process string
    std::string abspath(buffer);
    std::string::size_type dpos = abspath.find_last_of(".");
    std::string::size_type pos = abspath.find_last_of("\\/");
    if(dpos == abspath.npos || dpos < pos) return abspath;
    return abspath.substr(0, pos);
    
    #ifdef MX_RSX_UNIX
    }
    #endif
    return std::string("");
}

MXRSXDEF std::string mx::absolute_path(const std::string& p)
{
    std::string cwd = mx::cwd();
    return cwd + "/" + p;
}

MXRSXDEF std::string mx::filename(const std::string& path) 
{
    std::string::size_type ldir = path.find_last_of( "\\/" );
    if(ldir == path.npos)  ldir = -1; // no folder found

    std::string::size_type     ext = path.find_last_of( "." );
    if(ext == path.npos)       ext = path.length(); // no extension found
    if(mx::is_directory(path)) ext = path.length();

    return path.substr(ldir+1, ext - ldir - 1);
}

MXRSXDEF std::string mx::file_ext(const std::string& path) 
{
    std::string::size_type ext  = path.find_last_of( "." );
    if(ext == path.npos) return std::string("");
    return path.substr(ext+1);
}

MXRSXDEF mx::ressource_type mx::ext_to_type(const std::string& ext)
{
    std::map<std::string, mx::ressource_type> str2types{
        // SYSTEM
        {"..",  mx::ressource_type::FOLDER}, // parent
        {".",   mx::ressource_type::FOLDER}, // current
        {"rar", mx::ressource_type::COMPRESSED_FOLDER},
        {"zip", mx::ressource_type::COMPRESSED_FOLDER},
        {"exe", mx::ressource_type::APPLICATION},

        // IMAGES
        {"png",  mx::ressource_type::IMAGE_PNG},
        {"bmp",  mx::ressource_type::IMAGE_PPM},
        {"ppm",  mx::ressource_type::IMAGE_PPM},
        {"jpeg", mx::ressource_type::IMAGE_JPEG},
        {"jpg",  mx::ressource_type::IMAGE_JPEG},

        // SHADER
        {"vs",     mx::ressource_type::SHADER_VERTEX},
        {"fs",     mx::ressource_type::SHADER_FRAGMENT},
        {"gs",     mx::ressource_type::SHADER_COMPUTE},
        {"shader", mx::ressource_type::SHADER_PLAIN},

        // TEXT
        {"txt",  mx::ressource_type::TEXT_PLAIN},
        {"yml",  mx::ressource_type::TEXT_YAML},
        {"md",   mx::ressource_type::TEXT_MARKDOWN},
        {"json", mx::ressource_type::TEXT_JSON},

        // FONT
        {"ttf", mx::ressource_type::FONT_TTF}
    };
    if(str2types.find(ext) == str2types.end())
        return mx::ressource_type::UNKNOW;
    return str2types[ext];
}


////////////////////////////////////

// source(unix): https://stackoverflow.com/a/19534482
// source(win) : https://docs.microsoft.com/en-us/windows/win32/fileio/listing-the-files-in-a-directory
MXRSXDEF std::vector<mx::ressource_t> mx::directory::list(const std::string& path)
{
    std::vector<mx::ressource_t> content{};

    #ifdef MX_RSX_UNIX
    struct dirent **entries;
    int n = scandir(path.c_str(), &entries, NULL, alphasort);
    if(n < 0) return content;

    while(n--) {
        content.push_back(entries[n]->d_name);
        free(entries[n]);
    }
    free(entries);
    #elif defined(MX_RSX_WIN)
    WIN32_FIND_DATA ffd;
    HANDLE hfind = FindFirstFile((path + "\\*").c_str(), &ffd);
    DWORD derror;

    // error
    if(hfind == INVALID_HANDLE_VALUE)
    {
        printf("Error while loading folder.\n");
        return content;
    }

    // list files
    while(FindNextFile(hfind, &ffd) != 0)
    {
        // convert FILETIME TO TIME
        ULARGE_INTEGER ull;
        ull.LowPart = ffd.ftLastWriteTime.dwLowDateTime;
        ull.HighPart = ffd.ftLastWriteTime.dwHighDateTime;
        mx::timestamp_t t = ull.QuadPart / 10000000ULL - 11644473600ULL;

        // generate rsx struct
        content.push_back(mx::ressource_t{
            ffd.cFileName,
            path,
            mx::ext_to_type(mx::file_ext(ffd.cFileName)),
            t,
            (bool)(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
        });
    }
    #endif // MX_RSX_UNIX

    return content;
}

MXRSXDEF mx::directory_t mx::directory::load(const std::string& path)
{
    mx::directory_t dir{
        path, mx::filename(path),
        {}, {}, nullptr
    };
    printf("Loading folder '%s' (loc: %s)...", dir.name.c_str(), dir.path.c_str());
    
    // loading content
    std::vector<mx::ressource_t> dir_contents = mx::directory::list(path);

    printf("(Found %d Files):\n", dir_contents.size());

    // load into ressources
    for(const auto& c : dir_contents)
    {
        if(c.name == ".." || c.name == ".") continue; // ignore parent and current folder
        if(c.is_dir) {
            mx::directory_t subdir = mx::directory::load(path+'/'+c.name);
            subdir.parent = &dir;
            dir.subdirs.push_back(subdir);
        }
        dir.files.push_back(c);
        printf("File: %s (extID: %d)\n", c.name.c_str(), (int)c.type);
    }

    dir_contents.empty();
    return dir;
}

MXRSXDEF bool mx::is_directory(const std::string& path)
{
    struct stat s;
    if(stat(path.c_str(), &s) == 0
    && s.st_mode & S_IFDIR) return true;
    return false;
}

MXRSXDEF mx::timestamp_t mx::get_write_time(const std::string& path)
{
    struct stat s;
    if(stat(path.c_str(), &s) == 0) {
        return s.st_mtime;
    }
    return time_t(0);
}

////////////////////////////////////

MXRSXDEF mx::ressource_t mx::ressource::load(const std::string& path)
{
    mx::ressource_t r{
        mx::filename(path),
        path,
        mx::ext_to_type(mx::file_ext(path)),
        0
    };
    return r;
}

MXRSXDEF mx::ressource_t mx::ressource::load(const std::string& file, const std::string& location)
{
    mx::ressource_t r{
        mx::filename(file),
        location,
        mx::ext_to_type(mx::file_ext(file)),
        0
    };
    return r;
}

////////////////////////////////////

// #include <fstream>
// MXRSXDEF std::string mx::get_file_data(const std::string& path)
// {
//     std::string abs_path = mx::absolute_path(path);
//     std::ifstream f;
//     std::string data = "";
//     f.exceptions(std::ifstream::badbit | std::ifstream::failbit);
//     try {
//         f.open(abs_path, std::ios::in);
//         if(!f.is_open()) return data;

//         std::stringstream ss;
//         ss << f.rdbuf();
//         data = ss.str();
//     } catch(std::ifstream::failure e)
//     {
//         MXRERROR("FileReadingError[at %s]: (%d) %s\n", abs_path.c_str(), e.code().value(), e.what());
//     }
//     return data;
// }

//////////////////////////////////////////////

inline std::vector<mx::ressource_t> __mxrsx_unpack(const std::string& path)
{
    std::vector<mx::ressource_t> content = mx::directory::list(path);
    for(auto& c : content)
    {
        if(c.name == ".." || c.name == ".") continue; // ignore parent and current folder
        // unpack subdirs
        if(c.is_dir) {
            std::vector<mx::ressource_t> subcontent = __mxrsx_unpack(c.file_location+"/"+c.name+"/");
            content.insert(content.end(), subcontent.begin(), subcontent.end());
            continue;
        }
    }
    return content;
}

MXRSXDEF mx::rsxmanager_t mx::rsxmanager::create(const std::string& path)
{
    mx::rsxmanager_t rsxm{path, {}};
    // init map
    for(int i = 0; i < (int)mx::ressource_type::TYPE_LENGTH; ++i)
        rsxm.ressources[(mx::ressource_type)i] = {};

    // get rsx dir
    printf("Loading folder '%s' (loc: %s)...\n", mx::filename(path).c_str(), path.c_str());
    
    // loading content
    std::vector<mx::ressource_t> contents = __mxrsx_unpack(path);

    // load into ressources
    for(auto& c : contents) {
        if(c.name == ".." || c.name == ".") continue;
        std::string filekey = c.name;
        int i = 0;
        while(rsxm.ressources[c.type].find(filekey) != rsxm.ressources[c.type].end())
        {
            filekey += std::to_string(i);
            ++i;
        }
        rsxm.ressources[c.type].insert({filekey, c});
    }

    contents.empty();
    return rsxm;
}

MXRSXDEF void mx::rsxmanager::list(const mx::rsxmanager_t& rsxm)
{
    printf("Listing rsxm (o: %s):\n", rsxm.origin.c_str());
    for(int i = 0; i < (int)mx::ressource_type::TYPE_LENGTH; ++i)
    {
        printf("=====[TYPE: %d - %d elements]=====\n", i, rsxm.ressources.at((mx::ressource_type)i).size());
        for(auto& r : rsxm.ressources.at((mx::ressource_type)i)) {
            printf(" - %s (t: %ld)\n", r.first.c_str(), r.second.last_write_time);
        }
    }
}

////////////////////////////////////////////////////////////////////////

// using sys/stat
// src: https://stackoverflow.com/a/6039648/13972967

MXRSXDEF int mx::filesize(const std::string& path)
{
    struct stat stbuf;
    int rc = stat(path.c_str(), &stbuf);
    return rc == 0 ? stbuf.st_size : -1;
}

#endif //MX_IMPLEMENTATION_RSX

#endif //_MX_RSX_H_