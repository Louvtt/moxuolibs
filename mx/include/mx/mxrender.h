/*
MXLIBS. mx render is part of MXLIBS.
They are small(header-only or .h+.cpp files) libraries to make some graphical
experiments or small projetcs quickly and easily.
It let's you access anything 
Copyright (C) 2021 Lucyel Zanardo (@Louvtt)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _MX_MXRENDER_H_
#define _MX_MXRENDER_H_

#define MXRENDER_VERSION 1

// RENDER APIS
#if defined(MXRENDER_USE_VULKAN)
    #error "mxrender doesn't support Vulkan yet."
#elif defined(MXRENDER_USE_METAL)
    #error "mxrender doesn't support Metal yet."
#elif defined(MXRENDER_USE_DX)
    #error "mxrender doesn't support DirectX yet."
#else // Default to GL
    #define MXRENDER_GL 1
    #if !defined(MXRENDER_USE_GL) && !defined(MXRENDER_USE_GLES)
        #define MXRENDER_USE_GL 1
    #endif
    #ifndef __glad_h_
        #include <glad/glad.h>
    #endif
#endif


#ifndef _glfw3_h_
    #include <GLFW/glfw3.h>
    #ifndef _glfw3_h_
        #error "This library require glfw3"
    #endif
#endif

#ifndef STBI_VERSION
    #include <stb/stb_image.h>
    #ifndef STBI_VERSION
        #error "This library require stbimage"
    #endif
#endif

#ifndef __glm_h__
    #define MXM_INCLUDE_IMPLEMENTATION
    #include "mxmaths.h"
    #ifndef __mxmaths_h__
        #error "Require glm or mxmaths"
    #endif
#endif

#ifndef __cplusplus
    #error "This is a c++ lib"
#endif


//////////////////////////////////////////////////
// HEADER FILE
#define MXRDEF extern
// #if !defined(MXR_STATIC)
//     #define MXRDEF static
// #else
//    #define MXRDEF extern
// #endif

#include <vector>
#include <map>
#include <string>
#ifndef mat4
    #ifdef __mxmaths_h__
        using mat4 = mx_mat4;
    #elif defined(__glm_h__)
        using mat4 = glm::mat4;
    #endif
#endif

#ifndef vec2
    #ifdef __mxmaths_h__
        using vec2 = mx_vec2;
    #elif defined(__glm_h__)
        using vec2 = glm::vec2;
    #endif
#endif

#ifndef vec3
    #ifdef __mxmaths_h__
        using vec3 = mx_vec3;
    #elif defined(__glm_h__)
        using vec3 = glm::vec3;
    #endif
#endif

#ifndef vec4
    #ifdef __mxmaths_h__
        using vec4 = mx_vec4;
    #elif defined(__glm_h__)
        using vec4 = glm::vec4;
    #endif
#endif

namespace mx {

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    // time infos of the current app
    struct time_infos_t
    {
        double delta_time;
        double time;
        uint64_t frames;
    };
    static time_infos_t time_infos;

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    MXRDEF std::string __mxr_cwd();
    MXRDEF std::string __mxr_absolute_path(std::string p);
    MXRDEF std::string ___read_file(std::string path);
    MXRDEF std::string __get_filename(std::string path);

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    enum image_channels : int {
        GREY = 1,
        RGB  = 3,
        RGBA = 4
    };
    // image data
    // represented by a rgb(a) buffer of uint8_t [platform compatibility]
    typedef struct
    {
        uint8_t* pixels;
        unsigned int width, height;
        image_channels channels;
    } image_data_t;
    namespace image_data {
        // Create image_data
        MXRDEF image_data_t create(unsigned int width, unsigned int height, image_channels channels=image_channels::RGB);

        // Add pixels
        MXRDEF void write_pixel(image_data_t* image, unsigned int x, unsigned int y, float g);
        // Add pixel
        MXRDEF void write_pixel(image_data_t* image, unsigned int x, unsigned int y, float r, float g, float b);
        MXRDEF void write_pixel(image_data_t* image, unsigned int x, unsigned int y, vec3 color);
        // Add pixel
        MXRDEF void write_pixel(image_data_t* image, unsigned int x, unsigned int y, float r, float g, float b, float a);
        MXRDEF void write_pixel(image_data_t* image, unsigned int x, unsigned int y, vec4 color);

        // Get pixel data
        MXRDEF vec4 read_pixel(image_data_t image, unsigned int x, unsigned int y);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    // texture
    // represented by a render api id
    // render api binding idx
    // texture size
    typedef struct
    {
        uint32_t rendererID = 0;
        uint32_t binding_index = 0;
        int width = 1, height = 1;
        int channels = 0;
    } texture_t;

    // sub texture from a texture
    // represented by uv pos in a texture
    typedef struct
    {
        texture_t texture;
        vec2 pos;
        vec2 size;
    } subtexture_t;
    namespace texture {
        // create texture from pixel data
        MXRDEF texture_t create(unsigned char* data, int width, int height, int channels);
        // create texture from image_data
        MXRDEF texture_t create(image_data_t image_data);

        // create texture from a image file using stbi
        MXRDEF texture_t load_from_file(const char* path);

        // create sub texture from pixel pos and pixel size
        MXRDEF subtexture_t create_subtexture(texture_t tex, vec2 pos, vec2 size);

        MXRDEF void set_data(texture_t texture, unsigned char* data);
        MXRDEF void set_data(texture_t texture, image_data_t img);

        // bind texture to render api
        MXRDEF void bind(texture_t t, int index = 0);
        // unbind texture to render api
        MXRDEF void unbind(texture_t);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    // shader
    typedef struct
    {
        uint32_t rendererID;
        std::string name;
    } shader_t;
    namespace shader {
        // create shader program from vertex code and fragment code
        MXRDEF shader_t create(const char* vcode, const char* fcode);
        // create shader program from vertex code, fragment code and geometry code(compute shader)
        MXRDEF shader_t create(const char* vcode, const char* fcode, const char* gcode);
        // create shader from vertex glsl file and fragment glsl file
        MXRDEF shader_t load_from_files(std::string vpath, std::string fpath);
        // load a render api shader from code and shader type
        MXRDEF uint32_t load_shader(std::string code, int type);

        // begin draw using shader
        MXRDEF void begin(shader_t s);
        // end draw using shader
        MXRDEF void end(shader_t s);
	
	#define __R_SHADER_F(n, type) template<> void set_uniform##n(shader_t shader, std::string name, type value)
        // set an uniform (or varying) in a shader
        // [use mx::shader::begin before use]
        template<typename T>
        MXRDEF void set_uniform(shader_t shader, std::string name, T value);

        // set an uniform (or varying) array in a shader
        // [use mx::shader::begin before use]
        template <typename T>
        MXRDEF void set_uniform_array(shader_t shader, std::string name, T* values, uint32_t count);

        // Set a square matrix uniform (or varying) to a shader program
        // [use mx::shader::begin before use]
        MXRDEF void set_uniform_mat(shader_t shader, std::string name, float* valueptr, int size);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Simple events
    namespace events
    {
        // #define REGISTER_EVENT(name, ...) typedef void (*name)(...);
        #define nullevent nullptr;

        // ENVIRONNEMENT EVENTS
        
        // event occuring when the window (or context) is resized
        typedef void (*resize)        (void* obj, int w, int h);


        // INPUT EVENTS

        // event occuring the first frame when a key is pressed
        typedef void (*key_pressed)    (void* obj, int key, int scancode, int mods);
        // event occuring when a key is hold down (and pressed)
        typedef void (*key_down)       (void* obj, int key, int scancode, int mods);
        // event occuring when a key is released
        typedef void (*key_released)   (void* obj, int key, int scancode, int mods);

        // event occuring the first frame a mouse button is pressed
        typedef void (*mouse_pressed)  (void* obj, int button, int mods);
        // event occuring when a mouse button is released
        typedef void (*mouse_released) (void* obj, int button, int mods);
        // event occuring when the mouse move
        typedef void (*mouse_move)     (void* obj, float x, float y);
        // event occuring when the mouse wheel is used
        typedef void (*scroll)         (void* obj, float x_off, float y_off);
    }
    // Simple event manager
    typedef struct
    {
        // What describe a event in mx
        struct __event_descriptor_t
        {
            void* event_fnc;
            void* obj;
            // void* params;
        };

        // events registered in the manager
        std::map<std::string, std::vector<__event_descriptor_t>> registered_events { };
    } event_manager_t;
    namespace event_manager 
    {
        // return if the event manager has binding to this event
        MXRDEF bool has_event(event_manager_t* manager, std::string event_name);

        // register an event into a manager by event name and event function (see mx::events)
        MXRDEF void register_on(event_manager_t* manager, std::string event_name, void* obj, void* eventfnc);

        #ifndef REGISTER_EVT_FNC
        #define REGISTER_EVT_FNC(name, ...)\
            MXRDEF void register_on_##name(event_manager_t* manager, void* obj, events::name fnc);\
            MXRDEF void invoke_##name     (event_manager_t* manager, __VA_ARGS__)
        #endif

        REGISTER_EVT_FNC(resize, int width, int height);

        REGISTER_EVT_FNC(key_pressed , int key, int scancode, int mods);
        REGISTER_EVT_FNC(key_down    , int key, int scancode, int mods);
        REGISTER_EVT_FNC(key_released, int key, int scancode, int mods);

        REGISTER_EVT_FNC(scroll,         float xoff, float yoff);
        REGISTER_EVT_FNC(mouse_pressed,  int button, int mods);
        REGISTER_EVT_FNC(mouse_released, int button, int mods);
        REGISTER_EVT_FNC(mouse_move,     float xpos, float ypos);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    #define MX_KEY_END GLFW_KEY_END
    typedef struct {
        enum class state : int {
            UP = 0, PRESSED, DOWN, RELEASED
        };
        state keys[MX_KEY_END]{};
    } input_manager_t;
    namespace input_manager {
        MXRDEF bool     is_key_down(input_manager_t in, uint16_t key);
        MXRDEF bool  is_key_pressed(input_manager_t in, uint16_t key);
        MXRDEF bool is_key_released(input_manager_t in, uint16_t key);
        MXRDEF bool       is_key_up(input_manager_t in, uint16_t key);
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Default controller for camera
    typedef struct
    {
        float min_zoom   = 1.f;
        float max_zoom   = 1000.f;
        float zoom_speed = 250.f;
        float move_speed = 300.f;

        events::resize        resize_callback      = nullevent;
        events::key_pressed   keypress_callback    = nullevent;
        events::mouse_move    mousemove_callback   = nullevent;
        events::mouse_pressed mousepress_callback  = nullevent;
        events::scroll        mousescroll_callback = nullevent;
    } camera_controller_t;

    // camera
    typedef struct
    {
        // projection matrix (to make points fit into camera plane)
        mat4  proj;
        // view matrix (camera pos, rot, scale)
        mat4  view;
        vec3  pos;

        float zoom;
        vec2  size;

        vec3 up, right;

        camera_controller_t controller;
    } camera_t;

    namespace camera
    {
        // create a camera
        MXRDEF camera_t create(vec3 pos = {.0f, 0.0f, 0.f}, vec2 size = { 800.f, 600.f }, float zoom = 1.f);

        // Set camera to orthographic projection
        MXRDEF void set_projection_ortho(camera_t* cam, vec2 size = { 800.f, 600.f }, float zoom = 1.f);
        // Set camera to perspective projection
        MXRDEF void set_projection_persp(camera_t* cam, float fov=80.f, float near=0.1f, float far=1000.f);

        // move the camera by a certain amount
        MXRDEF void move(camera_t* cam, vec3 pos);
        // void lookat(vec3 target);
        MXRDEF void set_uniforms(camera_t cam, shader_t shader);
    };

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    // graphical context
    typedef struct
    {
        GLFWwindow* window_handle = nullptr;
        int width = 1, height = 1;
        const char* name = "mxctx";

        camera_t camera = {};
        bool can_camera_move = false;

        // time
        double last_frame_time = .0;

        // events
        event_manager_t event_manager;
        input_manager_t input_manager;
    } context_t;
    namespace context {
        // create graphical context (window + gl/vulkan/...)
        // width / height / window name
        MXRDEF context_t create(const std::string& name = "[MX APP]", int width = 800, int height = 640, bool can_camera_move = false);
        
        // end context with all the process link into it
        MXRDEF void end();
        
        // call before rendering anything
        MXRDEF void pre_render (context_t* ctx);
        // Set default uniform for a context (u_proj, u_view and u_time)
        MXRDEF void set_uniforms(context_t* ctx, mx::shader_t shader);
        // call after rendering everything (swap buffers & poll events)
        MXRDEF void post_render(context_t* ctx);

        // set size of window + viewport
        MXRDEF void set_size(context_t* ctx, int w, int h);

        // return if window should close
        MXRDEF bool should_close(context_t ctx);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    enum draw_mode : int {
        TRIANGLES,
        TRIANGLE_FAN,
        LINES,
        LINE_STRIP,
        POINTS
    };
    #define MX__DRAW_MODE_COUNT 4
    MXRDEF uint32_t draw_mode_to_GL(draw_mode mode);

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    typedef struct 
    {
            uint32_t renderer_id;
            uint32_t count;
            uint32_t vertex_size;
            bool is_dynamic;
    } vertex_buffer_t;
    namespace vertex_buffer
    {
            // create static vertex buffer
            MXRDEF vertex_buffer_t create(void* data, uint32_t count, uint32_t vertex_size);
            
            // create dynamic vertex buffer
            MXRDEF vertex_buffer_t create(uint32_t count, uint32_t vertex_size); 
            
            // Bind buffer
            MXRDEF void bind(vertex_buffer_t buffer);
            // Unbind buffer
            MXRDEF void unbind(vertex_buffer_t buffer);

            // Set data for dynamic buffers
            MXRDEF void set_data(vertex_buffer_t* buffer, void* data);
            // Set a chunk of data for dynamic buffers 
            MXRDEF void set_data(vertex_buffer_t* buffer, void* data, uint32_t offset, uint32_t count);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    typedef struct 
    {
        uint32_t renderer_id;
        uint32_t count;
        bool isDynamic = false;
    } index_buffer_t;
    namespace index_buffer
    {
        // MXRDEF index_buffer_t create(uint32_t size);
        
        // Create (int) indices buffer
        MXRDEF index_buffer_t create(int* indices_data, uint32_t count);
        // Create (uint32_t) indices buffer [preferred]
        MXRDEF index_buffer_t create(uint32_t* indices_data, uint32_t count);
        // Create (uint64_t) indices buffer
        MXRDEF index_buffer_t create(uint64_t* indices_data, uint32_t count);

        // Bind buffer
        MXRDEF void bind(index_buffer_t buffer);
        // Unbind buffer
        MXRDEF void unbind(index_buffer_t buffer);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    // vao layout (array of attr)
    struct vertex_layout_t
    {
        // vertex attr type
        enum class attr_type : int {
            INT = 0,   INT2,   INT3,
            FLOAT,   FLOAT2, FLOAT3, FLOAT4,
            BOOL
        };

        // vertex attr infos
        typedef struct 
        {
            std::string name;
            attr_type type;
            bool normalize = false;
            uint32_t offset = 0;
        } attr;

        std::vector<attr> attributes;
    };
    // layout of the vertex in buffer
    namespace vertex_layout
    {
        // create a layout
        MXRDEF vertex_layout_t create(std::initializer_list<vertex_layout_t::attr> attr = {});

        // add an attribute with type as string (check the definition to know)
        MXRDEF void add(vertex_layout_t* layout, const std::string& name, const std::string& type, bool normalize = false);
        // add an attribute
        MXRDEF void add(vertex_layout_t* layout, const std::string& name, vertex_layout_t::attr_type type, bool normalize = false);

        // bind attributes to a vao
        MXRDEF void bind(uint32_t vaoID, vertex_layout_t layout);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    // typedef struct {
    //     vec3 pos, scale;
    // } quad;

    typedef struct {
        vec3 pos, scale;
        vec3 rotation;
        
        draw_mode mode;

        uint32_t vao_id = 0;
        mx::vertex_buffer_t vbo;
        mx::index_buffer_t  ibo;
    } mesh_t;
    namespace mesh {
        MXRDEF mesh_t quad(vec3 pos = {.0f, .0f, .0f}, float size = 1.f);

        MXRDEF void draw(mesh_t mesh, shader_t shader);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    // simple vertex
    typedef struct 
    {
        // vertex position
        vec3  pos       = {.0f, .0f, .0f};      // origin to 0   (default)
    } vertex_t;
    // vertex with color
    typedef struct
    {
        // vertex position
        vec3  pos       = {.0f, .0f, .0f};      // origin to 0   (default)
        // vertex color (rgba)
        vec4  color     = {1.f, 1.f, 1.f, 1.f}; // white color   (default)
    } vertex_colored_t;
    // vertex with color and texCoords and texID (for multiple texture)
    typedef struct
    {
        // vertex position
        vec3  pos       = {.0f, .0f, .0f};      // origin to 0   (default)
        // vertex color (rgba)
        vec4  color     = {1.f, 1.f, 1.f, 1.f}; // white color   (default)
        // vertex UV coordinates (0 to 1)
        vec2  texCoords = {.0f, .0f };          // bottom left   (default)
        // vertex texture ID (renderer lib id)
        float texID     = 0.f;                  // white texture (default)
    } vertex_textured_t;

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // NEW BATCHING SYSTEM

    #define MAX_GEOMETRY_SIZE 4294967295 // uint32_t limit (2^32).


    typedef struct {
        uint32_t vertex_per_shape  = 0;
        uint32_t indices_per_shape = 0;
        uint32_t max_shapes        = 0;
        uint32_t max_vertices      = 0;

        vertex_textured_t* shape_vertices = nullptr;
        uint32_t*          shape_indices  = nullptr;

        draw_mode mode = draw_mode::TRIANGLES;
    } geometry_params_t;

    typedef struct {
        vertex_textured_t* vertices   = nullptr;
        vertex_textured_t* vertex_ptr = nullptr;

        uint32_t vertex_count = 0;
        uint32_t indices_count = 0;
        geometry_params_t params{};

        // DRAWING STUFF
        uint32_t vaoID       = 0;
        index_buffer_t ibo    {};
        bool is_indexed = false;
        vertex_buffer_t vbo   {};
        vertex_layout_t layout{};

        bool need_update     = false;
        // Do not touch.
        bool _static_geometry = true;
    } geometry_t;

    namespace geometry {
        MXRDEF geometry_t create_static(geometry_params_t& params);
        MXRDEF geometry_t create_dynamic(geometry_params_t& params);
    
        MXRDEF void add_shape(geometry_t* g, vec3 pos);
        MXRDEF void add_shape(geometry_t* g, vec3 pos, vec2 uv);
        MXRDEF void add_shape(geometry_t* g, vec3 pos, vec3 size);
        MXRDEF void add_shape(geometry_t* g, vec3 pos, vec3 size, vec2 uv0, vec2 uv1);
        MXRDEF void add_shape(geometry_t* g, vec3 pos, vec4 color);
        MXRDEF void add_shape(geometry_t* g, vec3 pos, vec4 color, vec3 size);
        MXRDEF void add_shape(geometry_t* g, vec3 pos, vec4 color, vec2 uv);
        MXRDEF void add_shape(geometry_t* g, vec3 pos, vec4 color, vec2 uv, vec3 size);
        MXRDEF void add_shape(geometry_t* g, vec3 pos, vec4 color, vec2 uv, float texID);

        // add a shape based on a vertices list with length must be equal to the param
        // vertex_per_shape passed in the geometry_t
        MXRDEF void add_shape(geometry_t* g, vertex_textured_t* vertices);

        // set data of the geometry
        MXRDEF void set_data(geometry_t* g, vertex_textured_t* vertices, uint32_t vertex_count, uint32_t offset = 0);

        // send data to the gpu
        MXRDEF void flush(geometry_t* g);
        // reset the vertices of a dynamic geometry
        MXRDEF void reset(geometry_t* g);
        // render the geometry data
        MXRDEF void render(geometry_t* g, shader_t shader);
    }

} //mx
#endif //_MX_MXRENDER_H_


