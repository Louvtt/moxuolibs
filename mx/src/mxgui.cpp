#include "../include/mx/mxgui.h"

mx::progress_bar_t mx::progress_bar::create( mx_vec3 pos, float min, float max, mx_vec2 size, bool alignCenter) {
    progress_bar_t res;

    res.pos = pos;
    res.alignCenter = alignCenter;

    res.background = mx::mesh::quad();
    res.backgroundColor = { .0f, .0f, .0f, 1.f };
    res.bar        = mx::mesh::quad();
    res.barColor   = { .1f, .8f, .2f, 1.f };

    res.min   = min;
    res.max   = max;
    res.size  = size;
    res.value = res.min;
    return res;
}

void mx::progress_bar::draw(mx::progress_bar_t bar, mx::shader_t shader) {
    if(bar.value >= bar.max) bar.value = bar.max;

    float bar_scale = ((bar.max - bar.min) / bar.max);
    bar.background.scale.x = bar.size.x * bar_scale;
    bar.background.scale.y = bar.size.y;
    
    bar.bar.scale.x = bar.size.x * bar_scale * (bar.value / bar.max);
    bar.bar.scale.y = bar.size.y;

    bar.background.pos = bar.pos;
    bar.bar.pos        = bar.pos;
    if(!bar.alignCenter) { // align to left
        bar.bar.pos.x -= (bar.size.x * bar_scale) * .5f;
        bar.bar.pos.x += bar.bar.scale.x * .5f;
    }

    // Draw progress bars
    mx::shader::begin(shader);
    mx::shader::set_uniform(shader, "u_color", bar.backgroundColor);
    mx::mesh::draw(bar.background, shader);

    mx::shader::begin(shader);
    mx::shader::set_uniform(shader, "u_color", bar.barColor);
    mx::mesh::draw(bar.bar,   shader);
}