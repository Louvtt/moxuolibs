#include <mx/mxrender.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>


// DEBUG ASSERT
#ifndef MXR_ASSERT
    #include <assert.h>
    #define MXR_ASSERT(x) assert(x)
#endif //MXM_ASSERT

// LOGGING COMPATIBILITY
#if defined(__WIN64) || defined(__WIN32)
    #define mxrlog(...)                printf_s(__VA_ARGS__)
    #define mxrstrbuf(s, max_len, ...) _snprintf_s(s, max_len, __VA_ARGS__)
#elif defined(__unix__) || defined(__linux__)
    #define mxrlog(...)                printf(__VA_ARGS__)
    #define mxrstrbuf(s, max_len, ...) snprintf(s, max_len, __VA_ARGS__)
#endif //__unix__ || __linux__

// LOG
#ifndef MXRLOG
    #include <stdio.h>
    #define MXRLOG_(x)      { mxrlog("%s:%s> %s", __FILE__, __LINE__, x); }
    #define MXRLOG(x, ...)  { mxrlog("%s:%s> ", __FILE__, __LINE__); mxrlog(x, __VA_ARGS__); }
#endif

// ERRORS
#ifndef MXRERROR
    #include <stdio.h>
    #define MXRERROR_(f)     { mxrlog("ERROR:"); mxrlog(f); }
    #define MXRERROR(f, ...) { mxrlog("ERROR:"); mxrlog(f, __VA_ARGS__); }
#endif

// FILE HANDLING
#include <sstream>
#include <fstream>
#include <cstdarg>


typedef struct 
{
    std::string* dirs;
    std::string  end;
} __mxr_path;
#if defined(__unix__)
    #define MXR_MAX_PATH 1024
    #include <unistd.h>
    MXRDEF std::string mx::__mxr_cwd()
    {
        char buffer[MXR_MAX_PATH];
        if(getcwd(buffer, sizeof(buffer)) != NULL)
        {
            std::string abspath(buffer);
            std::string::size_type dpos = abspath.find_last_of(".");
            std::string::size_type pos = abspath.find_last_of("\\/");
            if(dpos == abspath.npos || dpos < pos) return abspath;
            return abspath.substr(0, pos);
        }
        return std::string("");
    }

    MXRDEF std::string mx::__mxr_absolute_path(std::string p)
    {
        std::string cwd = mx::__mxr_cwd();
        return cwd + "/" + p;
    }
#elif defined(_WIN32) || defined(_WIN64)
    #define MXR_MAX_PATH 1024
    #include <windows.h>
    MXRDEF std::string mx::__mxr_cwd()
    {
        char buffer[MXR_MAX_PATH];
        GetModuleFileName( NULL, buffer, MXR_MAX_PATH );
        std::string::size_type position = std::string( buffer ).find_last_of( "\\/" );
        return std::string( buffer ).substr( 0, position);
    }

    MXRDEF std::string mx::__mxr_absolute_path(std::string p)
    {
        std::string cwd = mx::__mxr_cwd();
        return cwd + "/" + p;
    }
#endif

MXRDEF std::string mx::___read_file(std::string path)
{
    std::string abs_path = mx::__mxr_absolute_path(path);
    std::ifstream f;
    std::string data = "";
    f.exceptions(std::ifstream::badbit | std::ifstream::failbit);
    try {
        f.open(abs_path, std::ios::in);
        if(!f.is_open()) return data;

        std::stringstream ss;
        ss << f.rdbuf();
        data = ss.str();
    } catch(std::ifstream::failure e)
    {
        MXRERROR("FileReadingError[at %s]: (%d) %s\n", abs_path.c_str(), e.code().value(), e.what());
    }
    return data;
}

MXRDEF std::string mx::__get_filename(std::string path) 
{
    std::string::size_type ldir = path.find_last_of( "\\/" );
    if(ldir == path.npos) ldir = 0; // no folder found
    std::string::size_type ext  = path.find_last_of( "." );
    if(ext == path.npos) ext == path.length(); // no extension found
    return path.substr(ldir+1, ext - ldir - 1);
}

////////////////////////////////////////////
//               IMAGE DATA               //
////////////////////////////////////////////

// Create image_data
MXRDEF mx::image_data_t mx::image_data::create(unsigned int width, unsigned int height, mx::image_channels channels) {
    mx::image_data_t img;
    img.width  = width;
    img.height = height;
    img.pixels = new uint8_t[width * height * (int)channels];
    img.channels = channels;
    return img;
}

// Add pixels
MXRDEF void mx::image_data::write_pixel(image_data_t* img, unsigned int x, unsigned int y, float g) {
    unsigned int i = x + y * img->width;
    img->pixels[i] = static_cast<uint8_t>(g * 255.99f); // one color
}
// Add pixel
MXRDEF void mx::image_data::write_pixel(image_data_t* img, unsigned int x, unsigned int y, float r, float g, float b) {
    unsigned int i = x + y * img->width;
    i *= (int)img->channels;
    img->pixels[i + 0] = static_cast<uint8_t>(r * 255.99f); // red
    img->pixels[i + 1] = static_cast<uint8_t>(g * 255.99f); // blue
    img->pixels[i + 2] = static_cast<uint8_t>(b * 255.99f); // green
    if(img->channels == image_channels::RGBA)
        img->pixels[i + 3] = static_cast<uint8_t>(255.99f);
}
MXRDEF void mx::image_data::write_pixel(image_data_t* img, unsigned int x, unsigned int y, vec3 c) {
    unsigned int i = x + y * img->width;
    i *= (int)img->channels;
    img->pixels[i + 0] = static_cast<uint8_t>(c.x * 255.99f); // red
    img->pixels[i + 1] = static_cast<uint8_t>(c.y * 255.99f); // blue
    img->pixels[i + 2] = static_cast<uint8_t>(c.z * 255.99f); // green
}

// Add pixel
MXRDEF void mx::image_data::write_pixel(image_data_t* img, unsigned int x, unsigned int y, float r, float g, float b, float a) {
    unsigned int i = x + y * img->width;
    i *= (int)img->channels;
    img->pixels[i + 0] = static_cast<uint8_t>(r * 255.99f); // red
    img->pixels[i + 1] = static_cast<uint8_t>(g * 255.99f); // blue
    img->pixels[i + 2] = static_cast<uint8_t>(b * 255.99f); // green
    img->pixels[i + 3] = static_cast<uint8_t>(a * 255.99f); // green
}
MXRDEF void mx::image_data::write_pixel(image_data_t* img,unsigned int x,unsigned int y, vec4 c) {
    unsigned int i = x + y * img->width;
    i *= (int)img->channels;
    img->pixels[i + 0] = static_cast<uint8_t>(c.x * 255.99f); // red
    img->pixels[i + 1] = static_cast<uint8_t>(c.y * 255.99f); // blue
    img->pixels[i + 2] = static_cast<uint8_t>(c.z * 255.99f); // green
    img->pixels[i + 3] = static_cast<uint8_t>(c.w * 255.99f); // green
}

// Get pixel data
MXRDEF vec4 mx::image_data::read_pixel(image_data_t img,unsigned int x,unsigned int y) {
    unsigned int i = x + y * img.width;
    i *= (int)img.channels;

    float r = 1.f, g = 1.f, b = 1.f, a = 1.f;
    switch(img.channels) {
        case mx::image_channels::GREY:
            g = static_cast<float>(img.pixels[i] / 255.99f);
            return {g,g,g,1.f};
        case mx::image_channels::RGB:
            r = static_cast<float>(img.pixels[i + 0] / 255.99f);
            g = static_cast<float>(img.pixels[i + 1] / 255.99f);
            b = static_cast<float>(img.pixels[i + 2] / 255.99f);
            return {r,g,b,1.f};
        case mx::image_channels::RGBA:
            r = static_cast<float>(img.pixels[i + 0] / 255.99f);
            g = static_cast<float>(img.pixels[i + 1] / 255.99f);
            b = static_cast<float>(img.pixels[i + 2] / 255.99f);
            a = static_cast<float>(img.pixels[i + 3] / 255.99f);
            return {r,g,b,a};
    }
    return {.0f,.0f,.0f,.0f};
}

////////////////////////////////////////////
//                 TEXTURE                //
////////////////////////////////////////////

MXRDEF mx::texture_t mx::texture::create(unsigned char* data, int w, int h, int channels)
{
    mx::texture_t t;
    t.width = w;
    t.height = h;
    t.channels = channels;
    #ifdef MXRENDER_GL
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glGenTextures(1, &t.rendererID);
    glBindTexture(GL_TEXTURE_2D, t.rendererID);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    GLuint channels_gl[]{GL_NONE, GL_RED, GL_RG, GL_RGB, GL_RGBA};
    glTexImage2D(GL_TEXTURE_2D, 0, channels_gl[channels], w, h, 0, channels_gl[channels], GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0); // unbind
    #endif
    // printf("%d/%d (c: %d) => %d\n", w, h, channels, t.rendererID);
    return t;
}

MXRDEF mx::texture_t mx::texture::create(mx::image_data_t image)
{
    return mx::texture::create(image.pixels, image.width, image.height, (int)image.channels);
}

MXRDEF void mx::texture::bind(mx::texture_t t, int texture_i)
{
    glActiveTexture(GL_TEXTURE0 + texture_i);
    t.binding_index = texture_i;
    glBindTexture(GL_TEXTURE_2D, t.rendererID);
    // glActiveTexture(GL_TEXTURE0);
}

MXRDEF void mx::texture::unbind(mx::texture_t t)
{
    glActiveTexture(GL_TEXTURE0 + t.binding_index);
    glBindTexture(GL_TEXTURE_2D, 0);
    // glActiveTexture(GL_TEXTURE0);
}

MXRDEF mx::texture_t mx::texture::load_from_file(const char* path) 
{
    int width, height, channels;
    std::string abspath = __mxr_absolute_path(path);
    unsigned char* data = stbi_load(abspath.c_str(), &width, &height, &channels, 0);
    if(!data)
    {
        MXRERROR("STBI_ImageReadingError: Failed to load the image data at [%s].\n", abspath.c_str());
    }
    mx::texture_t t = mx::texture::create(data, width, height, channels);
    return t;
}


MXRDEF void mx::texture::set_data(mx::texture_t tex, unsigned char* data) {
    #if MXRENDER_GL
    glBindTexture(GL_TEXTURE_2D, tex.rendererID);
    glTexImage2D(GL_TEXTURE_2D, 0, (tex.channels == 3)? GL_RGB : GL_RGBA, tex.width, tex.height, 0, (tex.channels == 3)? GL_RGB : GL_RGBA, GL_UNSIGNED_BYTE, data);
    glBindTexture(GL_TEXTURE_2D, 0);
    #endif
}
MXRDEF void mx::texture::set_data(mx::texture_t tex, mx::image_data_t img) {
    MXR_ASSERT(tex.width == img.width && tex.height == img.height && tex.channels == (int)img.channels);
    #if MXRENDER_GL
    glBindTexture(GL_TEXTURE_2D, tex.rendererID);
    glTexImage2D(GL_TEXTURE_2D, 0, (tex.channels == 3)? GL_RGB : GL_RGBA, tex.width, tex.height, 0, (tex.channels == 3)? GL_RGB : GL_RGBA, GL_UNSIGNED_BYTE, img.pixels);
    glBindTexture(GL_TEXTURE_2D, 0);
    #endif
}

////////////////////////////////////////////
//                 SHADER                 //
////////////////////////////////////////////

MXRDEF mx::shader_t mx::shader::create(const char* vcode, const char* fcode)
{
    shader_t shader;
    #ifdef MXRENDER_GL
    shader.rendererID = glCreateProgram();
    uint32_t vid = load_shader(vcode, GL_VERTEX_SHADER);
    uint32_t fid = load_shader(fcode, GL_FRAGMENT_SHADER);
    glAttachShader(shader.rendererID, vid);
    glAttachShader(shader.rendererID, fid);
    glLinkProgram(shader.rendererID);
    int success; char log[512];
    glGetProgramiv(shader.rendererID, GL_LINK_STATUS, &success);
    if(!success)
    {
        glGetProgramInfoLog(shader.rendererID, 512, NULL, log);
        MXRERROR("ShaderLinkError: %s", log);
    }
    #endif
    return shader;
}

MXRDEF mx::shader_t mx::shader::create(const char* vcode, const char* fcode, const char* gcode)
{
    shader_t shader;
    #ifdef MXRENDER_GL
    shader.rendererID = glCreateProgram();
    uint32_t vid = load_shader(vcode, GL_VERTEX_SHADER);
    uint32_t fid = load_shader(fcode, GL_FRAGMENT_SHADER);
    uint32_t gid = load_shader(gcode, GL_COMPUTE_SHADER);

    glAttachShader(shader.rendererID, vid);
    glAttachShader(shader.rendererID, fid);
    glAttachShader(shader.rendererID, gid);
    glLinkProgram(shader.rendererID);
    
    int success; char log[512];
    glGetProgramiv(shader.rendererID, GL_LINK_STATUS, &success);
    if(!success)
    {
        glGetProgramInfoLog(shader.rendererID, 512, NULL, log);
        MXRERROR("ShaderLinkError: %s", log);
    }
    #endif
    return shader;
}


MXRDEF void mx::shader::begin(mx::shader_t shader)
{
    #ifdef MXRENDER_GL
    glUseProgram(shader.rendererID);
    #endif
}

MXRDEF void mx::shader::end(mx::shader_t shader)
{
    #ifdef MXRENDER_GL
    glUseProgram(0);
    #endif
}

// UNIFORMS
template<>
void mx::shader::set_uniform(mx::shader_t s, std::string name, float value) {
    #ifdef MXRENDER_GL
    uint32_t loc = glGetUniformLocation(s.rendererID, name.c_str());
    glUniform1f(loc, value);
    #endif
}

template<>
void mx::shader::set_uniform(shader_t s, std::string name, double value) {
    #ifdef MXRENDER_GL
    uint32_t loc = glGetUniformLocation(s.rendererID, name.c_str());
    glUniform1f(loc, (float)value);
    #endif
}

template<>
void mx::shader::set_uniform(shader_t s, std::string name, int value) {
    #ifdef MXRENDER_GL
    uint32_t loc = glGetUniformLocation(s.rendererID, name.c_str());
    glUniform1i(loc, value);
    #endif
}

template<>
void mx::shader::set_uniform(shader_t s, std::string name, bool value) {
    #ifdef MXRENDER_GL
    uint32_t loc = glGetUniformLocation(s.rendererID, name.c_str());
    glUniform1i(loc, (int)value);
    #endif
}

template<>
void mx::shader::set_uniform(mx::shader_t s, std::string name, vec3 value) {
    #ifdef MXRENDER_GL
    uint32_t loc = glGetUniformLocation(s.rendererID, name.c_str());
    glUniform3f(loc, value.x, value.y, value.z);
    #endif
}

template<>
void mx::shader::set_uniform(mx::shader_t s, std::string name, vec4 value) {
    #ifdef MXRENDER_GL
    uint32_t loc = glGetUniformLocation(s.rendererID, name.c_str());
    glUniform4f(loc, value.x, value.y, value.z, value.w);
    #endif
}

MXRDEF void mx::shader::set_uniform_mat(mx::shader_t s, std::string name, float* valueptr, int size) {
    #ifdef MXRENDER_GL
    uint32_t loc = glGetUniformLocation(s.rendererID, name.c_str());
    if(loc == -1) MXRERROR("ERROR: Shader '%s' doesn't have a uniform named '%s'\n.", s.name.c_str(), name.c_str());
    switch(size)
    {
        case 3:
            glUniformMatrix3fv(loc, 1, GL_FALSE, valueptr);
            break;
        case 4:
            glUniformMatrix4fv(loc, 1, GL_FALSE, valueptr);
            break;
    }
    #endif
}


template<>
void mx::shader::set_uniform_array(mx::shader_t s, std::string name, int* arr, uint32_t count)
{
    #ifdef MXRENDER_GL
    uint32_t loc = glGetUniformLocation(s.rendererID, name.c_str());
    glUniform1iv(loc, count, arr);
    #endif
}

template<>
void mx::shader::set_uniform_array(mx::shader_t s, std::string name, float* arr, uint32_t count)
{
    #ifdef MXRENDER_GL
    uint32_t loc = glGetUniformLocation(s.rendererID, name.c_str());
    glUniform1fv(loc, count, arr);
    #endif
}

// LOADERS
MXRDEF mx::shader_t mx::shader::load_from_files(std::string vpath, std::string fpath) {
    // read each files
    ::std::string vcode = ___read_file(vpath);
    ::std::string fcode = ___read_file(fpath);

    mx::shader_t res = mx::shader::create(vcode.c_str(), fcode.c_str());
    res.name = __get_filename(vpath);
    return res;
}


MXRDEF uint32_t mx::shader::load_shader(std::string code, int type)
{
    const char* str = code.c_str();
    #ifdef MXRENDER_GL
    char infoLog[512];

    uint32_t s_id = glCreateShader(type);
    glShaderSource(s_id, 1, &str, NULL);
    glCompileShader(s_id);
    int compiled = 0;
    glGetShaderiv(s_id, GL_COMPILE_STATUS, &compiled);
    if(!compiled)
    {
        glGetShaderInfoLog(s_id, 512, NULL, infoLog);
        MXRERROR("ShaderCompileError: %s\n", infoLog);
    }
    #endif
    return s_id;
}

////////////////////////////////////////////
//                 EVENTS                 //
////////////////////////////////////////////

MXRDEF bool mx::event_manager::has_event(mx::event_manager_t* manager, std::string event_name) {
    return (manager->registered_events.find(event_name) != manager->registered_events.end());
}

MXRDEF void mx::event_manager::register_on(mx::event_manager_t* manager, std::string event_name, void* obj, void* eventfnc)
{
    manager->registered_events[event_name].push_back(
        {
            eventfnc,
            obj
        }
    );
}

// easy macro for registering events functions
#ifndef REGEVNT_FNC
    #define REGEVNT_FNC(n) \
    MXRDEF void mx::event_manager::register_on_##n (mx::event_manager_t* manager, void* obj, mx::events::n fnc) {\
        manager->registered_events[#n].push_back({(void*)fnc, obj});\
    }
#endif

// window resized
REGEVNT_FNC(resize)
MXRDEF void mx::event_manager::invoke_resize(mx::event_manager_t* manager, int w, int h)
{
    if(!mx::event_manager::has_event(manager, "resize")) return;
    for(const auto& fnc : manager->registered_events["resize"])
    {
        mx::events::resize f = (mx::events::resize)fnc.event_fnc;
        f(fnc.obj, w, h);
    }
}

// key pressed
REGEVNT_FNC(key_pressed)
MXRDEF void mx::event_manager::invoke_key_pressed(mx::event_manager_t* manager, int key, int scancode, int mods)
{
    if(!mx::event_manager::has_event(manager, "key_pressed")) return;
    for(const auto& fnc : manager->registered_events.at("key_pressed"))
    {
        mx::events::key_pressed f = (mx::events::key_pressed)fnc.event_fnc;
        f(fnc.obj, key, scancode, mods);
    }
}

// key down
REGEVNT_FNC(key_down)
MXRDEF void mx::event_manager::invoke_key_down(mx::event_manager_t* manager, int key, int scancode, int mods)
{
    if(!mx::event_manager::has_event(manager, "key_down")) return;
    for(const auto& fnc : manager->registered_events.at("key_down"))
    {
        mx::events::key_down f = (mx::events::key_down)fnc.event_fnc;
        f(fnc.obj, key, scancode, mods);
    }
}

// key released
REGEVNT_FNC(key_released)
MXRDEF void mx::event_manager::invoke_key_released(mx::event_manager_t* manager, int key, int scancode, int mods)
{
    if(!mx::event_manager::has_event(manager, "key_released")) return;
    for(const auto& fnc : manager->registered_events.at("key_released"))
    {
        mx::events::key_released f = (mx::events::key_released)fnc.event_fnc;
        f(fnc.obj, key, scancode, mods);
    }
}

// mouse scrolled
REGEVNT_FNC(scroll)
MXRDEF void mx::event_manager::invoke_scroll(mx::event_manager_t* manager, float xoff, float yoff)
{
    if(!mx::event_manager::has_event(manager, "scroll")) return;
    for(const auto& fnc : manager->registered_events.at("scroll"))
    {
        mx::events::scroll f = (mx::events::scroll)fnc.event_fnc;
        f(fnc.obj, xoff, yoff);
    }
}

// mouse button pressed
REGEVNT_FNC(mouse_pressed)
MXRDEF void mx::event_manager::invoke_mouse_pressed(mx::event_manager_t* manager, int button, int mods)
{
    if(!mx::event_manager::has_event(manager, "mouse_pressed")) return;
    for(const auto& fnc : manager->registered_events.at("mouse_pressed"))
    {
        mx::events::mouse_pressed f = (mx::events::mouse_pressed)fnc.event_fnc;
        f(fnc.obj, button, mods);
    }
}

// mouse button released
REGEVNT_FNC(mouse_released)
MXRDEF void mx::event_manager::invoke_mouse_released(mx::event_manager_t* manager, int button, int mods)
{
    if(!mx::event_manager::has_event(manager, "mouse_released")) return;
    for(const auto& fnc : manager->registered_events.at("mouse_released"))
    {
        mx::events::mouse_released f = (mx::events::mouse_released)fnc.event_fnc;
        f(fnc.obj, button, mods);
    }
}

// MOUSE MOVE
REGEVNT_FNC(mouse_move)
MXRDEF void mx::event_manager::invoke_mouse_move(mx::event_manager_t* manager, float xpos, float ypos)
{
    if(!mx::event_manager::has_event(manager, "mouse_move")) return;
    for(const auto& fnc : manager->registered_events.at("mouse_move"))
    {
        mx::events::mouse_move f = (mx::events::mouse_move)fnc.event_fnc;
        f(fnc.obj, xpos, ypos);
    }
}

// prevent usage
#ifndef REGEVNT_FNC
    #undef REGEVNT_FNC
#endif

////////////////////////////////////////////
//                 INPUTS                 //
////////////////////////////////////////////

MXRDEF bool     mx::input_manager::is_key_down(mx::input_manager_t in, uint16_t key) {
    return in.keys[key] == mx::input_manager_t::state::DOWN;
}
MXRDEF bool  mx::input_manager::is_key_pressed(mx::input_manager_t in, uint16_t key) {
    return in.keys[key] == mx::input_manager_t::state::PRESSED;
}
MXRDEF bool mx::input_manager::is_key_released(mx::input_manager_t in, uint16_t key) {
    return in.keys[key] == mx::input_manager_t::state::RELEASED;
}
MXRDEF bool       mx::input_manager::is_key_up(mx::input_manager_t in, uint16_t key) {
    return in.keys[key] == mx::input_manager_t::state::UP;
}

////////////////////////////////////////////
//                 CAMERA                //
////////////////////////////////////////////

static void camdefault_resizecallback(void* obj, int w, int h)
{
    mx::camera_t* cam = (mx::camera_t*)obj;
    // Recalculate proj
    float aspect_ratio = float(w)/float(h);
    cam->size = { (float)w, (float)h };
    float z2_inv = 1.f / cam->zoom;
    cam->proj = mx_ortho_proj(
        -cam->size.x * z2_inv, 
         cam->size.x * z2_inv, 
        -cam->size.y * z2_inv, 
         cam->size.y * z2_inv
    );
} 

static void camdefault_keycallback(void* obj, int key, int scancode, int mods)
{
    mx::camera_t* cam = (mx::camera_t*)obj;
    float s = cam->controller.move_speed * (float)mx::time_infos.delta_time * (1.f / cam->zoom);
    switch(key)
    {
        case GLFW_KEY_W: mx::camera::move(cam, { 0.f,    s, 0.f}); break; // down
        case GLFW_KEY_S: mx::camera::move(cam, { 0.f,   -s, 0.f}); break; // up
        case GLFW_KEY_D: mx::camera::move(cam, {   s,  0.f, 0.f}); break; // left
        case GLFW_KEY_A: mx::camera::move(cam, {  -s,  0.f, 0.f}); break; // right
    }
} 

static void camdefault_mousescrollcallback(void* obj, float xoff, float yoff)
{
    mx::camera_t* cam = (mx::camera_t*)obj;
    float zoom_change = yoff * cam->controller.zoom_speed * (float)mx::time_infos.delta_time;
    cam->zoom += (float)zoom_change;
    if(cam->zoom < .0f) cam->zoom = .0f;
    // if(cam->zoom < (1.f / cam->controller.max_zoom)) cam->zoom = 1.f / cam->controller.max_zoom;

    float z2_inv = .5f / cam->zoom;
    cam->proj = mx_ortho_proj(
        -cam->size.x * z2_inv, 
         cam->size.x * z2_inv, 
        -cam->size.y * z2_inv, 
         cam->size.y * z2_inv
    );
 } 

MXRDEF mx::camera_t mx::camera::create(vec3 pos, vec2 size, float zoom)
{
    mx::camera_t cam;
    cam.zoom = zoom;
    float z2_inv = 1.f / zoom;
    cam.proj = mx_ortho_proj(-size.x, size.x, -size.y, size.y, -1.f, 100.f);
    cam.pos  = pos;
    cam.view = mx_mat4_translate(mx_mat4_identity(), pos);
    cam.size = { size.x, size.y };

    cam.controller.resize_callback      = camdefault_resizecallback;
    cam.controller.keypress_callback    = camdefault_keycallback;
    cam.controller.mousescroll_callback = camdefault_mousescrollcallback;
    return cam;
}

MXRDEF void mx::camera::set_projection_ortho(mx::camera_t* cam, vec2 size, float zoom)
{
    float z2_inv = 1.f / zoom;
    cam->proj = mx_ortho_proj(-size.x * z2_inv, size.x * z2_inv, size.y * z2_inv, -size.y * z2_inv, -1, 1);
}

MXRDEF void mx::camera::set_projection_persp(mx::camera_t* cam, float fov, float near_, float far_)
{
    cam->proj = mx_persp_proj(near_, far_, fov);
}

MXRDEF void mx::camera::move(mx::camera_t* cam, vec3 pos)
{
    #if defined(__mxmaths_h__)
    cam->pos = mx_vec3_add(cam->pos, pos);
    cam->view = mx_mat4_translate(mx_mat4_identity(), cam->pos);
    #endif
}

MXRDEF void mx::camera::set_uniforms(mx::camera_t cam, mx::shader_t s)
{
    mx::shader::begin(s);
    #if defined(__mxmaths_h__)
    mx_mat4_valueptr projptr = mx_mat4_raw(cam.proj);
    mx::shader::set_uniform_mat(s, "u_proj", projptr.ptr, 4);

    mx_mat4_valueptr viewptr = mx_mat4_raw(cam.view);
    mx::shader::set_uniform_mat(s, "u_view", viewptr.ptr, 4);
    #endif
    mx::shader::end(s);
}



////////////////////////////////////////////
//                 CONTEXT                //
////////////////////////////////////////////

MXRDEF mx::context_t mx::context::create(const std::string& name, int w, int h, bool can_camera_move)
{
    mx::context_t ctx {
        nullptr,
        w, h,
        name.c_str()
    };
    ctx.can_camera_move = can_camera_move;

    // init glfw
    int success = glfwInit();
    if(!success)
    {
        MXRERROR_("GLFW FAILED TO INITIALIZE\n");
        exit(1);
    }

    glfwWindowHint(GLFW_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_VERSION_MAJOR, 3);
    ctx.window_handle = glfwCreateWindow(w, h, name.c_str(), NULL, NULL);
    glfwMakeContextCurrent(ctx.window_handle);

    // Vsync 60 FPS
    glfwSwapInterval(1);

    // init api (GL/Vulkan/Metal/DX...)
    #if defined(MXRENDER_USE_GL)
    success = gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
    #elif defined(MXRENDER_USE_GLES)
    success = gladLoadGLES2Loader((GLADloadproc)glfwGetProcAddress)
    #endif
    if(!success)
    {
        MXRERROR_("Render api couldn't be loaded\n");
        exit(2);
    }

    #ifdef MXRENDER_GL
        glViewport(0, 0, w, h);
    #endif

    ctx.camera = mx::camera::create({}, {(float)w, (float)h});
    
    mx::time_infos.delta_time = 1. / 60.; // 60 FPS
    mx::time_infos.frames     = 0U;
    mx::time_infos.time       = 0.0;

    // CALLBACKS
    glfwSetWindowUserPointer(ctx.window_handle, (void*)&ctx);
    glfwSetInputMode(ctx.window_handle, GLFW_STICKY_KEYS, GLFW_TRUE);
    glfwSetKeyCallback(ctx.window_handle, [](GLFWwindow* win, int key, int scancode, int action, int mods)
    {
        mx::context_t* ctx = (mx::context_t*)glfwGetWindowUserPointer(win);
        if(key == GLFW_KEY_ESCAPE)
            glfwSetWindowShouldClose(win, true);

        if(ctx->camera.controller.keypress_callback
        && action == GLFW_REPEAT)
            ctx->camera.controller.keypress_callback(&ctx->camera, key, scancode, mods);

        // event manager
        if(action == GLFW_PRESS) {
            ctx->input_manager.keys[key] = mx::input_manager_t::state::PRESSED;
            mx::event_manager::invoke_key_pressed(&ctx->event_manager, key, scancode, mods);
        }
        if(action == GLFW_RELEASE) {
            ctx->input_manager.keys[key] = mx::input_manager_t::state::RELEASED;
            mx::event_manager::invoke_key_released(&ctx->event_manager, key, scancode, mods);
        }
        if(action == GLFW_PRESS || action == GLFW_REPEAT)
            mx::event_manager::invoke_key_down(&ctx->event_manager, key, scancode, mods);
    });
    glfwSetScrollCallback(ctx.window_handle, [](GLFWwindow* win, double xscroll, double yscroll)
    {
        mx::context_t* ctx = (mx::context_t*)glfwGetWindowUserPointer(win);

        if(ctx->camera.controller.keypress_callback)
            ctx->camera.controller.mousescroll_callback(&ctx->camera, (float)xscroll, (float)yscroll);

        mx::event_manager::invoke_scroll(&ctx->event_manager, (float)xscroll, (float)yscroll);
    });
    glfwSetFramebufferSizeCallback(ctx.window_handle, [](GLFWwindow* win, int w, int h)
    {
        mx::context_t* ctx = (mx::context_t*)glfwGetWindowUserPointer(win);
        mx::context::set_size(ctx, w, h);
        #if MXRENDER_GL
            glViewport(0, 0, w, h);
        #endif //GL

        if(ctx->camera.controller.resize_callback)
            ctx->camera.controller.resize_callback(&ctx->camera, w, h);

        mx::event_manager::invoke_resize(&ctx->event_manager, w, h);

    });
    glfwSetMouseButtonCallback(ctx.window_handle, [](GLFWwindow* win, int button, int action, int mods)
    {
        mx::context_t* ctx = (mx::context_t*)glfwGetWindowUserPointer(win);

        if(action == GLFW_PRESS)
            mx::event_manager::invoke_mouse_pressed(&ctx->event_manager, button, mods);
        if(action == GLFW_RELEASE)
            mx::event_manager::invoke_mouse_released(&ctx->event_manager,button, mods);
    });

    glfwSetCursorPosCallback(ctx.window_handle, [](GLFWwindow* win, double x, double y)
    {
        mx::context_t* ctx = (mx::context_t*)glfwGetWindowUserPointer(win);

        mx::event_manager::invoke_mouse_move(&ctx->event_manager, (float)x, (float)y);
    });
    return ctx;
}

MXRDEF void mx::context::pre_render(mx::context_t* ctx)
{
    #ifdef MXRENDER_GL
        glClearColor(.1f, .1f, .1f, 1.f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    #endif

    // update time
    mx::time_infos.time = glfwGetTime();
    mx::time_infos.delta_time = glfwGetTime() - ctx->last_frame_time;
    mx::time_infos.frames++;
    // printf("[delta: %f (%fs)][%d frames]: %fs since begin.\n", mx::time_infos.delta_time, ctx->last_frame_time, mx::time_infos.frames, mx::time_infos.time);
    ctx->last_frame_time = glfwGetTime();
}

MXRDEF void mx::context::set_uniforms(mx::context_t* ctx, mx::shader_t shader)
{
    mx::shader::begin(shader);
    mx::camera::set_uniforms(ctx->camera, shader);
    mx::shader::set_uniform(shader, "u_time", (float)glfwGetTime());
    mx::shader::end(shader);
}

MXRDEF void mx::context::post_render(mx::context_t* ctx)
{
    glfwSwapBuffers(ctx->window_handle);

    // save a copy of the last inputs
    mx::input_manager_t::state inputcpy[MX_KEY_END]; 
    memcpy(&inputcpy[0], &ctx->input_manager.keys[0], MX_KEY_END * sizeof(mx::input_manager_t::state));

    // update inputs
    glfwPollEvents();
    
    // update key states based on previous ones
    for(int i = 0; i < MX_KEY_END; ++i) {
        mx::input_manager_t::state s = ctx->input_manager.keys[i];
        if(s == mx::input_manager_t::state::PRESSED && inputcpy[i] == s) // still pressed
            ctx->input_manager.keys[i] = mx::input_manager_t::state::DOWN;
        else if(s == mx::input_manager_t::state::RELEASED && inputcpy[i] == s) // still released
            ctx->input_manager.keys[i] = mx::input_manager_t::state::UP;
    }
}

MXRDEF void mx::context::end()
{
    glfwTerminate();
}

MXRDEF bool mx::context::should_close(mx::context_t ctx)
{
    return glfwWindowShouldClose(ctx.window_handle);
}

MXRDEF void mx::context::set_size(mx::context_t* ctx, int w, int h)
{
    ctx->width = w;
    ctx->height = h;
    glfwSetWindowSize(ctx->window_handle, w, h);
}

////////////////////////////////////////////
//             DRAW MODE                  //
////////////////////////////////////////////

MXRDEF uint32_t mx::draw_mode_to_GL(mx::draw_mode mode) {
    #if MXRENDER_GL
    switch(mode) {
        case mx::draw_mode::TRIANGLES:
            return GL_TRIANGLES;
        case mx::draw_mode::TRIANGLE_FAN:
            return GL_TRIANGLE_FAN;
        case mx::draw_mode::LINES:
            return GL_LINES;
        case mx::draw_mode::LINE_STRIP:
            return GL_LINE_STRIP;
        case mx::draw_mode::POINTS:
            return GL_POINTS;
    }
    #endif
    return 0;
}

////////////////////////////////////////////
//           VERTEX BUFFER                //
////////////////////////////////////////////

MXRDEF mx::vertex_buffer_t mx::vertex_buffer::create(void* data, uint32_t count, uint32_t vertex_size)
{
    mx::vertex_buffer_t res;

    res.vertex_size = vertex_size;
    res.count = count;

    res.is_dynamic = false;

    #if MXRENDER_GL
    glGenBuffers(1, &res.renderer_id);
    glBindBuffer(GL_ARRAY_BUFFER, res.renderer_id);
    glBufferData(GL_ARRAY_BUFFER, count * vertex_size, data, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind
    #endif
    return res;
}

MXRDEF mx::vertex_buffer_t mx::vertex_buffer::create(uint32_t count, uint32_t vertex_size)
{
    mx::vertex_buffer_t res;

    res.count = count;
    res.vertex_size = vertex_size;
    
    res.is_dynamic = true;

    #if MXRENDER_GL
    glGenBuffers(1, &res.renderer_id);
    glBindBuffer(GL_ARRAY_BUFFER, res.renderer_id);
    glBufferData(GL_ARRAY_BUFFER, count * vertex_size, nullptr, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind
    #endif
    return res;
}

MXRDEF void mx::vertex_buffer::bind(mx::vertex_buffer_t buffer)
{
    #if MXRENDER_GL
    glBindBuffer(GL_ARRAY_BUFFER, buffer.renderer_id);
    #endif
}
MXRDEF void mx::vertex_buffer::unbind(mx::vertex_buffer_t buffer)
{
    #if MXRENDER_GL
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    #endif
}

MXRDEF void mx::vertex_buffer::set_data(mx::vertex_buffer_t* buf, void* data)
{
    #if MXRENDER_GL
    glBindBuffer(GL_ARRAY_BUFFER, buf->renderer_id);
    glBufferSubData(GL_ARRAY_BUFFER, 0, buf->count * buf->vertex_size, data);
    glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind
    #endif
}
MXRDEF void mx::vertex_buffer::set_data(mx::vertex_buffer_t* buf, void* data, uint32_t offs, uint32_t count)
{
    #if MXRENDER_GL
    glBindBuffer(GL_ARRAY_BUFFER, buf->renderer_id);
    glBufferSubData(GL_ARRAY_BUFFER, offs * buf->vertex_size, count * buf->vertex_size, data);
    glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind
    #endif
}


////////////////////////////////////////////
//           INDICES BUFFER               //
////////////////////////////////////////////

MXRDEF mx::index_buffer_t mx::index_buffer::create(int* indices_data, uint32_t count) {
    mx::index_buffer_t res;
    res.count = count;
    #if MXRENDER_GL
    glGenBuffers(1, &res.renderer_id);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, res.renderer_id);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(int), indices_data, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); // unbind
    #endif
    return res;
}
// Create (uint32_t) indices buffer [preferred]
MXRDEF mx::index_buffer_t mx::index_buffer::create(uint32_t* indices_data, uint32_t count) {
    mx::index_buffer_t res;
    res.count = count;
    #if MXRENDER_GL
    glGenBuffers(1, &res.renderer_id);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, res.renderer_id);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(uint32_t), indices_data, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); // unbind
    #endif
    return res;
}
// Create (uint64_t) indices buffer
MXRDEF mx::index_buffer_t mx::index_buffer::create(uint64_t* indices_data, uint32_t count) {
    mx::index_buffer_t res;
    res.count = count;
    #if MXRENDER_GL
    glGenBuffers(1, &res.renderer_id);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, res.renderer_id);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(uint64_t), indices_data, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); // unbind
    #endif
    return res;
}

// Bind buffer
MXRDEF void mx::index_buffer::bind(mx::index_buffer_t buffer) {
    #if MXRENDER_GL
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer.renderer_id);
    #endif
}
// Unbind buffer
MXRDEF void mx::index_buffer::unbind(mx::index_buffer_t buffer) {
    #if MXRENDER_GL
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    #endif
}


////////////////////////////////////////////
//           VERTEX LAYOUT                //
////////////////////////////////////////////

MXRDEF mx::vertex_layout_t mx::vertex_layout::create(std::initializer_list<mx::vertex_layout_t::attr> attr)
{
    mx::vertex_layout_t layout;
    layout.attributes = attr;
    return layout;
}

#ifndef __str2vattrt
#define __str2vattrt(s, t) if(type == #s) return mx::vertex_layout_t::attr_type::t
static mx::vertex_layout_t::attr_type str_to_vertex_attr_type(const std::string& type)
{
    // TODO: order them by most used first
    __str2vattrt(int,    INT);
    __str2vattrt(float,  FLOAT);
    __str2vattrt(bool,   BOOL);
    __str2vattrt(float2, FLOAT2);
    __str2vattrt(uv,     FLOAT2); // spec type uv => float2
    __str2vattrt(int2,   INT2);
    __str2vattrt(float3, FLOAT3);
    __str2vattrt(rgb,    FLOAT3); // spec type rgb => float3
    __str2vattrt(int3,   INT3);
    __str2vattrt(float4, FLOAT4);
    __str2vattrt(rgba,   FLOAT3); // spec type rgba => float4
    return mx::vertex_layout_t::attr_type::FLOAT;
}
#undef __str2vattrt
#endif

MXRDEF void mx::vertex_layout::add(mx::vertex_layout_t* layout, const std::string& name, const std::string& type, bool normalize)
{
    mx::vertex_layout_t::attr_type attr_type = str_to_vertex_attr_type(type);
    layout->attributes.push_back({ name, attr_type, normalize });
}
MXRDEF void mx::vertex_layout::add(mx::vertex_layout_t* layout, const std::string& name, mx::vertex_layout_t::attr_type type, bool normalize)
{
    layout->attributes.push_back({ name, type, normalize });
}

#ifndef sattrt
    // shorten name for easy coding
    #define sattrt(t) (int)mx::vertex_layout_t::attr_type::t
#endif
// return nb of components in type
static uint32_t vattrcomp(mx::vertex_layout_t::attr attr)
{
    switch((int)attr.type)
    {
        case sattrt(BOOL):   return 1;

        case sattrt(INT):    return 1;
        case sattrt(INT2):   return 2;

        case sattrt(FLOAT):  return 1;
        case sattrt(FLOAT2): return 2;
        case sattrt(FLOAT3): return 3;
        case sattrt(FLOAT4): return 4;
        default:
            return 0;
    }
    return 0; // no corresponding return 0
}
// return bit size of type
static uint32_t vattrsize(mx::vertex_layout_t::attr attr)
{
    switch((int)attr.type)
    {
        case sattrt(BOOL): return 1;

        case sattrt(INT):  return 4;
        case sattrt(INT2): return 4 * 2;
        case sattrt(INT3): return 4 * 3;

        case sattrt(FLOAT):  return 4;
        case sattrt(FLOAT2): return 4 * 2;
        case sattrt(FLOAT3): return 4 * 3;
        case sattrt(FLOAT4): return 4 * 4;

        default:
            return 0;
    }
    return 0; // no corresponding return 0
}

MXRDEF void mx::vertex_layout::bind(uint32_t vaoID, mx::vertex_layout_t layout)
{
    #if MXRENDER_GL
    glBindVertexArray(vaoID);

    // printf("Calculating stride of %d attribute(s)\n", layout.attributes.size());
    // Calculate stride and offsets
    uint32_t stride = 0;
    for(int i = 0; i < layout.attributes.size(); i++)
    {
        layout.attributes[i].offset = stride;
        stride += vattrsize(layout.attributes[i]);
    }

    uint32_t attrib_idx = 0; // different than layout_attribute_idx because if we use matrix
    for(auto attr : layout.attributes) // loop throught the attributes
    {
        switch((int)attr.type)
        {
            case sattrt(INT):
            case sattrt(INT2):
            case sattrt(INT3):
            case sattrt(BOOL):
                // printf("glEnableVertexAttribArray(%d)\nglVertexAttribPointer(%d, %d, GL_INT, GL_FALSE, %d, %d)\n", attrib_idx, attrib_idx, vattrcomp(attr), stride, attr.offset);
                glEnableVertexAttribArray(attrib_idx);
                glVertexAttribPointer(
                    attrib_idx,
                    vattrcomp(attr),
                    GL_INT,
                    (attr.normalize) ? GL_TRUE : GL_FALSE,
                    stride,
                    reinterpret_cast<const void*>(attr.offset)
                );
                attrib_idx++;
                break;
            case sattrt(FLOAT):
            case sattrt(FLOAT2):
            case sattrt(FLOAT3):
            case sattrt(FLOAT4):
                // printf("glEnableVertexAttribArray(%d)\nglVertexAttribPointer(%d, %d, GL_FLOAT, GL_FALSE, %d, %d)\n", attrib_idx, attrib_idx, vattrcomp(attr), stride, attr.offset);
                glEnableVertexAttribArray(attrib_idx);
                glVertexAttribPointer(
                    attrib_idx,
                    vattrcomp(attr),
                    GL_FLOAT,
                    (attr.normalize) ? GL_TRUE : GL_FALSE,
                    stride,
                    reinterpret_cast<const void*>(attr.offset)
                );
                attrib_idx++;
                break;
        }
    }
    #endif
    // printf("-- Done --\n");
}
#ifdef sattrt
    #undef sattrt
#endif

////////////////////////////////////////////
//                 MESH                   //
////////////////////////////////////////////


MXRDEF mx::mesh_t mx::mesh::quad(vec3 pos, float size) {
    mx::mesh_t mesh;
    mesh.pos = pos;
    mesh.scale = {size, size, size};
    
    mx::vertex_textured_t vts[4] = {
        // pos                  // color                 // uv              // texID
        {{ -0.5f, -0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, { 0.f, 0.f }, .0f}, // top left
        {{  0.5f, -0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, { 1.f, 0.f }, .0f}, // top right
        {{  0.5f,  0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, { 1.f, 1.f }, .0f}, // bottom right
        {{ -0.5f,  0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, { .0f, 1.f }, .0f}, // bottom left
    };
    uint32_t idx[6] = { 0,1,2, 2,3,0 };

    #if MXRENDER_GL
        glGenVertexArrays(1, &mesh.vao_id);
        glBindVertexArray(mesh.vao_id);
    #endif

    mx::vertex_layout_t layout = mx::vertex_layout::create({
        { "pos",       mx::vertex_layout_t::attr_type::FLOAT3 },
        { "color",     mx::vertex_layout_t::attr_type::FLOAT4 },
        { "texCoords", mx::vertex_layout_t::attr_type::FLOAT2 },
        { "texID",     mx::vertex_layout_t::attr_type::FLOAT  }
    });

    mesh.vbo = mx::vertex_buffer::create(vts, 4, sizeof(mx::vertex_textured_t));
    mx::vertex_buffer::bind(mesh.vbo);
    mesh.ibo = mx::index_buffer::create(idx, 6);
    mx::index_buffer::bind(mesh.ibo);

    mx::vertex_layout::bind(mesh.vao_id, layout);

    mesh.mode = mx::draw_mode::TRIANGLES;

    #if MXRENDER_GL
        glBindVertexArray(0);
    #endif
    return mesh;
}

MXRDEF void mx::mesh::draw(mx::mesh_t mesh, mx::shader_t shader)
{
    mx::shader::begin(shader);

    // model mat
    mat4 model;
    #ifdef __mxmaths_h__
        model = mx_mat4_identity();
        model = mx_mat4_scale(model, mesh.scale);
        // model = mx_mat4_rotate(model, {0.f, 0.f, 1.f}, mesh.rotation.x);
        model = mx_mat4_translate(model, mesh.pos);
        shader::set_uniform_mat(shader, "u_model", mx_mat4_raw(model).ptr, 4);
    #endif

    #if MXRENDER_GL
        const GLint mxmodes2gl[MX__DRAW_MODE_COUNT] = {GL_TRIANGLES, GL_TRIANGLE_FAN, GL_LINES, GL_POINTS};
        GLint mode = mxmodes2gl[mesh.mode];
        if(!mode) mode = GL_TRIANGLES;

        glBindVertexArray(mesh.vao_id);
        glBindBuffer(GL_ARRAY_BUFFER, mesh.vbo.renderer_id);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh.ibo.renderer_id);
        glDrawElements(mode, mesh.ibo.count, GL_UNSIGNED_INT, nullptr);
        glBindVertexArray(0);
    #endif
    mx::shader::end(shader);
}

////////////////////////////////////////////////////////////
//                   GEOMETRY TYPE                        //
////////////////////////////////////////////////////////////

MXRDEF mx::geometry_t mx::geometry::create_static(mx::geometry_params_t& params)
{
    mx::geometry_t g;
    g._static_geometry = true;
    // setup params
    g.params.shape_vertices    = new mx::vertex_textured_t[params.vertex_per_shape];
    memcpy(&g.params.shape_vertices[0], &params.shape_vertices[0], params.vertex_per_shape * sizeof(mx::vertex_textured_t));
    g.params.vertex_per_shape  = params.vertex_per_shape;
    g.params.max_shapes        = params.max_shapes;
    g.params.max_vertices      = g.params.max_shapes * g.params.vertex_per_shape;
    g.params.shape_indices     = new uint32_t[params.indices_per_shape];
    memcpy(&g.params.shape_indices[0], &params.shape_indices[0], params.indices_per_shape * sizeof(uint32_t));
    g.params.indices_per_shape = params.indices_per_shape;
    g.is_indexed               = g.params.indices_per_shape != 0U;
    g.params.mode              = params.mode;

    #if MXRENDER_GL
        glGenVertexArrays(1, &g.vaoID);
        glBindVertexArray(g.vaoID);
    #endif

    g.vertices      = g.params.shape_vertices;
    g.is_indexed    = g.params.indices_per_shape != 0U;
    g.vertex_ptr    = &g.vertices[0];
    g.vertex_count  = 0;
    g.indices_count = 0;
    g.need_update   = false;

    // generate buffer
    g.vbo = mx::vertex_buffer::create((void*)g.params.shape_vertices, g.params.vertex_per_shape, sizeof(mx::vertex_textured_t));
    if(g.is_indexed) 
        g.ibo = mx::index_buffer::create(g.params.shape_indices, g.params.indices_per_shape);

    g.layout = mx::vertex_layout::create();
    mx::vertex_layout::add(&g.layout, "pos",   "float3");
    mx::vertex_layout::add(&g.layout, "color", "float3");
    mx::vertex_layout::add(&g.layout, "uv",    "float2");
    mx::vertex_layout::add(&g.layout, "texID", "float" );
    mx::vertex_layout::bind(g.vaoID, g.layout);

    #if MXRENDER_GL
        glBindVertexArray(0);
    #endif

    return g;
}

MXRDEF mx::geometry_t mx::geometry::create_dynamic(mx::geometry_params_t& params) 
{
    mx::geometry_t g;
    g._static_geometry = false;
    
    // setup params
    g.params.shape_vertices    = new mx::vertex_textured_t[params.vertex_per_shape];
    memcpy(&g.params.shape_vertices[0], &params.shape_vertices[0], params.vertex_per_shape * sizeof(mx::vertex_textured_t));
    g.params.vertex_per_shape  = params.vertex_per_shape;
    g.params.max_shapes        = params.max_shapes;
    g.params.max_vertices      = g.params.max_shapes * g.params.vertex_per_shape;
    g.params.shape_indices     = new uint32_t[params.indices_per_shape];
    memcpy(&g.params.shape_indices[0], &params.shape_indices[0], params.indices_per_shape * sizeof(uint32_t));
    g.params.indices_per_shape = params.indices_per_shape;
    g.is_indexed               = g.params.indices_per_shape != 0U;
    g.params.mode              = params.mode;

    #if MXRENDER_GL
        glGenVertexArrays(1, &g.vaoID);
        glBindVertexArray(g.vaoID);
    #endif
    
    g.vertex_count = 0;
    g.vertices   = new mx::vertex_textured_t[g.params.max_vertices];
    g.vertex_ptr = &g.vertices[0];

    // generate buffer
    g.vbo = mx::vertex_buffer::create(g.params.max_vertices, sizeof(mx::vertex_textured_t));
    mx::vertex_buffer::bind(g.vbo);

    if(g.is_indexed) {
        uint32_t* indices = new uint32_t[g.params.max_shapes * g.params.indices_per_shape];
        uint32_t offset = 0;
        for(uint32_t i = 0; i < g.params.max_vertices; i += g.params.vertex_per_shape)
        {
            for(uint32_t p_i = 0; p_i < g.params.indices_per_shape; ++p_i)
            {
                indices[offset + p_i] = i + g.params.shape_indices[p_i];
            }
            offset += g.params.indices_per_shape;
        }

        g.ibo = mx::index_buffer::create(indices, g.params.max_shapes * g.params.indices_per_shape);
        mx::index_buffer::bind(g.ibo);
    }
    g.indices_count = 0;

    g.layout = mx::vertex_layout::create({
        { "pos",       mx::vertex_layout_t::attr_type::FLOAT3 },
        { "color",     mx::vertex_layout_t::attr_type::FLOAT4 },
        { "texCoords", mx::vertex_layout_t::attr_type::FLOAT2 },
        { "texID",     mx::vertex_layout_t::attr_type::FLOAT  }
    });
    mx::vertex_layout::bind(g.vaoID, g.layout);

    #if MXRENDER_GL
        glBindVertexArray(0);
    #endif

    g.need_update = true;
    return g;
}

MXRDEF void  mx::geometry::add_shape(mx::geometry_t* g, vec3 pos) {
    if(g->_static_geometry) return;
    if(g->vertex_count + g->params.vertex_per_shape >= g->params.max_vertices) return; // prevent overflow
    for(int i = 0; i < g->params.vertex_per_shape; ++i) {
        g->vertex_ptr->pos       = mx_vec3_add(pos, g->params.shape_vertices[i].pos);
        g->vertex_ptr->color     = g->params.shape_vertices[i].color;
        g->vertex_ptr->texCoords = g->params.shape_vertices[i].texCoords;
        g->vertex_ptr->texID     = g->params.shape_vertices[i].texID;
        g->vertex_ptr++;
    }
    g->vertex_count+=g->params.vertex_per_shape;
    g->indices_count+=g->params.indices_per_shape;
    g->need_update = true;
}

MXRDEF void mx::geometry::add_shape(mx::geometry_t* g, vec3 pos, vec3 size, vec2 uv0, vec2 uv1) {
    if(g->_static_geometry) return;
    if(g->vertex_count + g->params.vertex_per_shape >= g->params.max_vertices) return; // prevent overflow
    for(int i = 0; i < g->params.vertex_per_shape; ++i) {
        g->vertex_ptr->pos       = mx_vec3_add(pos, mx_vec3_mul(g->params.shape_vertices[i].pos, size));
        g->vertex_ptr->color     = g->params.shape_vertices[i].color;

        vec2 computed_uv = {uv0}; // default to bottom left
        float tx = g->params.shape_vertices[i].texCoords.x;
        float ty = g->params.shape_vertices[i].texCoords.y;
        computed_uv.x = (1.f - tx) * uv0.x + tx * uv1.x;
        computed_uv.y = (1.f - ty) * uv0.y + ty * uv1.y;
        g->vertex_ptr->texCoords = computed_uv;
        g->vertex_ptr->texID     = g->params.shape_vertices[i].texID;
        g->vertex_ptr++;
    }
    g->vertex_count+=g->params.vertex_per_shape;
    g->indices_count+=g->params.indices_per_shape;
    g->need_update = true;
}

MXRDEF void  mx::geometry::add_shape(mx::geometry_t* g, vec3 pos, vec3 size) {
    if(g->_static_geometry) return;
    if(g->vertex_count + g->params.vertex_per_shape >= g->params.max_vertices) return; // prevent overflow
    for(int i = 0; i < g->params.vertex_per_shape; ++i) {
        g->vertex_ptr->pos       = mx_vec3_add(pos, mx_vec3_mul(g->params.shape_vertices[i].pos, size));
        g->vertex_ptr->color     = g->params.shape_vertices[i].color;
        g->vertex_ptr->texCoords = g->params.shape_vertices[i].texCoords;
        g->vertex_ptr->texID     = g->params.shape_vertices[i].texID;
        g->vertex_ptr++;
    }
    g->vertex_count+=g->params.vertex_per_shape;
    g->indices_count+=g->params.indices_per_shape;
    g->need_update = true;
}

MXRDEF void mx::geometry::add_shape(mx::geometry_t* g, vec3 pos, vec2 uv) {
    if(g->_static_geometry) return;
    if(g->vertex_count + g->params.vertex_per_shape >= g->params.max_vertices) return; // prevent overflow
    for(int i = 0; i < g->params.vertex_per_shape; ++i) {
        g->vertex_ptr->pos       = mx_vec3_add(pos, g->params.shape_vertices[i].pos);
        g->vertex_ptr->color     = g->params.shape_vertices[i].color;
        g->vertex_ptr->texCoords = mx_vec2_add(uv, g->params.shape_vertices[i].texCoords);
        g->vertex_ptr->texID     = g->params.shape_vertices[i].texID;
        g->vertex_ptr++;
    }
    g->vertex_count+=g->params.vertex_per_shape;
    g->indices_count+=g->params.indices_per_shape;
    g->need_update = true;
}

MXRDEF void  mx::geometry::add_shape(mx::geometry_t* g, vec3 pos, vec4 color) {
    if(g->_static_geometry) return;
    if(g->vertex_count + g->params.vertex_per_shape >= g->params.max_vertices) return; // prevent overflow
    for(int i = 0; i < g->params.vertex_per_shape; ++i) {
        g->vertex_ptr->pos       = mx_vec3_add(pos, g->params.shape_vertices[i].pos);
        g->vertex_ptr->color     = color;
        g->vertex_ptr->texCoords = g->params.shape_vertices[i].texCoords;
        g->vertex_ptr->texID     = g->params.shape_vertices[i].texID;
        g->vertex_ptr++;
    }
    g->vertex_count+=g->params.vertex_per_shape;
    g->indices_count+=g->params.indices_per_shape;
    g->need_update = true;
}

MXRDEF void  mx::geometry::add_shape(mx::geometry_t* g, vec3 pos, vec4 color, vec3 size) {
    if(g->_static_geometry) return;
    if(g->vertex_count + g->params.vertex_per_shape >= g->params.max_vertices) return; // prevent overflow
    for(int i = 0; i < g->params.vertex_per_shape; ++i) {
        g->vertex_ptr->pos       = mx_vec3_add(pos, mx_vec3_mul(g->params.shape_vertices[i].pos, size));
        g->vertex_ptr->color     = color;
        g->vertex_ptr->texCoords = g->params.shape_vertices[i].texCoords;
        g->vertex_ptr->texID     = g->params.shape_vertices[i].texID;
        g->vertex_ptr++;
    }
    g->vertex_count+=g->params.vertex_per_shape;
    g->indices_count+=g->params.indices_per_shape;
    g->need_update = true;
}

MXRDEF void  mx::geometry::add_shape(mx::geometry_t* g, vec3 pos, vec4 color, vec2 uv) {
    if(g->_static_geometry) return;
    if(g->vertex_count + g->params.vertex_per_shape >= g->params.max_vertices) return; // prevent overflow
    for(int i = 0; i < g->params.vertex_per_shape; ++i) {
        g->vertex_ptr->pos       = mx_vec3_add(pos,   g->params.shape_vertices[i].pos);
        g->vertex_ptr->color     = color;
        g->vertex_ptr->texCoords = mx_vec2_add(uv, g->params.shape_vertices[i].texCoords);
        g->vertex_ptr->texID     = g->params.shape_vertices[i].texID;
        g->vertex_ptr++;
    }
    g->vertex_count+=g->params.vertex_per_shape;
    g->indices_count+=g->params.indices_per_shape;
    g->need_update = true;
}

MXRDEF void  mx::geometry::add_shape(mx::geometry_t* g, vec3 pos, vec4 color, vec2 uv, vec3 size) {
    if(g->_static_geometry) return;
    if(g->vertex_count + g->params.vertex_per_shape >= g->params.max_vertices) return; // prevent overflow
    for(int i = 0; i < g->params.vertex_per_shape; ++i) {
        g->vertex_ptr->pos       = mx_vec3_add(pos, mx_vec3_mul(g->params.shape_vertices[i].pos, size));
        g->vertex_ptr->color     = color;
        g->vertex_ptr->texCoords = mx_vec2_add(uv, g->params.shape_vertices[i].texCoords);
        g->vertex_ptr->texID     = g->params.shape_vertices[i].texID;
        g->vertex_ptr++;
    }
    g->vertex_count+=g->params.vertex_per_shape;
    g->indices_count+=g->params.indices_per_shape;
    g->need_update = true;
}

MXRDEF void  mx::geometry::add_shape(mx::geometry_t* g, vec3 pos, vec4 color, vec2 uv, float texID) {
    if(g->_static_geometry) return;
    if(g->vertex_count + g->params.vertex_per_shape >= g->params.max_vertices) return; // prevent overflow
    for(int i = 0; i < g->params.vertex_per_shape; ++i) {
        g->vertex_ptr->pos       = mx_vec3_add(pos,   g->params.shape_vertices[i].pos);
        g->vertex_ptr->color     = color;
        g->vertex_ptr->texCoords = mx_vec2_add(uv, g->params.shape_vertices[i].texCoords);
        g->vertex_ptr->texID     = texID;
        g->vertex_ptr++;
    }
    g->vertex_count+=g->params.vertex_per_shape;
    g->indices_count+=g->params.indices_per_shape;
    g->need_update = true;
}

MXRDEF void mx::geometry::add_shape(mx::geometry_t* g, mx::vertex_textured_t* vertices) {
    if(g->_static_geometry) return;
    if(g->vertex_count + g->params.vertex_per_shape >= g->params.max_vertices) return; // prevent overflow
    for(int i = 0; i < g->params.vertex_per_shape; ++i) {
        g->vertex_ptr->pos       = vertices[i].pos;
        g->vertex_ptr->color     = vertices[i].color;
        g->vertex_ptr->texCoords = vertices[i].texCoords;
        g->vertex_ptr->texID     = vertices[i].texID;
        g->vertex_ptr++;
    }
    g->vertex_count+=g->params.vertex_per_shape;
    g->indices_count+=g->params.indices_per_shape;
    g->need_update = true;
}

MXRDEF void mx::geometry::set_data(mx::geometry_t* g, mx::vertex_textured_t* vertices, uint32_t vertex_count, uint32_t offset)
{
    if(offset > g->params.max_vertices - vertex_count) return; // overflow
    if(vertex_count >= g->params.max_vertices)
        vertex_count = g->params.max_vertices - 1;
    memcpy(&g->vertices[offset], vertices, vertex_count * sizeof(mx::vertex_textured_t));
    g->vertex_count = offset + vertex_count;
    g->need_update = true;
}



MXRDEF void mx::geometry::flush(mx::geometry_t* g) 
{
    if(g->_static_geometry) return;
    #if MXRENDER_GL
    if(g->need_update)
    {
        glBindVertexArray(g->vaoID);
        mx::vertex_buffer::set_data(&g->vbo, &g->vertices[0], 0, g->vertex_count);
        mx::vertex_buffer::bind(g->vbo);
        glBindVertexArray(0);
        g->need_update = false;
    }
    #endif //GL
}

MXRDEF void mx::geometry::reset(mx::geometry_t* g)
{
    if(g->_static_geometry) return;
    g->indices_count = 0;               // reset indices counter
    g->vertex_count  = 0;               // reset vertices counter
    g->vertex_ptr  = &g->vertices[0];   // reset pointer to begin of data
    // g->need_update = true;
}

MXRDEF void mx::geometry::render(geometry_t* g, mx::shader_t shader) 
{
    mx::shader::begin(shader);

    #if MXRENDER_GL
    glBindVertexArray(g->vaoID);
    if(g->is_indexed) {
        glDrawElements(mx::draw_mode_to_GL(g->params.mode), g->indices_count, GL_UNSIGNED_INT, nullptr);
    } else {
        glDrawArrays(mx::draw_mode_to_GL(g->params.mode), 0, g->vertex_count);
    }
    glBindVertexArray(0);
    #endif

    mx::shader::end(shader);
}

