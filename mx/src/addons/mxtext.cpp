#include <mx/addons/mxtext.h>

#include <stb/stb_truetype.h>
#include <fstream>

#include <sys/stat.h>
static int __mxtext_filesize(const std::string& path)
{
    struct stat filebuf;
    int rc = stat(path.c_str(), &filebuf);
    return rc == 0 ? filebuf.st_size : -1;
}

static std::string __mxtext_filename(const std::string& path) {
    std::string::size_type ldir = path.find_last_of( "\\/" );
    if(ldir == path.npos)  ldir = -1; // no folder found

    std::string::size_type     ext = path.find_last_of( "." );
    if(ext == path.npos)       ext = path.length(); // no extension found

    return path.substr(ldir+1, ext - ldir - 1);
}

MXRDEF mx::font_t mx::font::load(const std::string& path, int size) 
{
    // load font
    stbtt_fontinfo font;
    int filesize = __mxtext_filesize(path.c_str());
    unsigned char *ttf_buffer = new unsigned char[filesize];
    fread(ttf_buffer, 1, filesize, fopen(path.c_str(), "rb"));
    if(!stbtt_InitFont(&font, ttf_buffer, stbtt_GetFontOffsetForIndex(ttf_buffer, 0)))
        printf("ERROR: Loading font failed !\n");

    /* prepare font */
    mx::font_t res{
        __mxtext_filename(path), {}
    };
    res.size = size;
    res.scale_factor = stbtt_ScaleForPixelHeight(&font, res.size);

    stbtt_GetFontVMetrics(&font, &res.ascent, &res.descent, &res.linegap);
    res.ascent  = roundf(res.ascent  * res.scale_factor);
    res.descent = roundf(res.descent * res.scale_factor);
    res.linegap = roundf(res.linegap * res.scale_factor);

    // font atlas
    int minx,miny,maxx,maxy;
    stbtt_GetFontBoundingBox(&font, &minx, &miny, &maxx, &maxy);
    int charw = (maxx - minx) * res.scale_factor,
        charh = (maxy - miny) * res.scale_factor;
    res.baselineH = res.scale_factor*-miny;
    int padding = 5;
    printf("Max char is %d by %d\n", charw, charh);

    const char* alphabet   = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz./,' ";
    const int char_count   = strlen(alphabet);
    const int atlas_cols   = 10;
    const int colsize      = (charw + padding);
    const int rowsize      = (charh + res.linegap);
    const int atlas_rows   = ceilf(char_count / (float)atlas_cols);
    const int atlas_width  = colsize * atlas_cols;
    const int atlas_height = rowsize * atlas_rows;
    unsigned char* atlas   = new unsigned char[atlas_width * atlas_height];
    // printf("Loading chars into atlas (%dx%d => %dx%d)\n", atlas_cols, atlas_rows, atlas_width, atlas_height);

    // load chars
    for(int i = 0; i < strlen(alphabet); ++i) {
        int x = i % atlas_cols;
        int y = roundf(i / atlas_cols);

        mx::fontchar_t charinfo;

        // how wide is the char
        int ax, lsb;
        stbtt_GetCodepointHMetrics(&font, alphabet[i], &ax, &lsb);
       
        // Get bounding box
        int x0,y0,x1,y1;
        stbtt_GetCodepointBitmapBox(&font, alphabet[i], res.scale_factor, res.scale_factor, &x0, &y0, &x1, &y1);
        charinfo.sizex = x1 - x0;
        charinfo.sizey = y1 - y0;
        charinfo.bearingx = x0;
        charinfo.bearingy =res.ascent - (charinfo.sizey + y0);
        charinfo.advance  = roundf(ax  * res.scale_factor);
        
        // TEXTURE
        int stride = (y * rowsize * atlas_width) + (x * colsize);
        stbtt_MakeCodepointBitmap(&font, atlas + stride, x1 - x0, y1 - y0, atlas_width, res.scale_factor, res.scale_factor, alphabet[i]);

        // add char to map
        charinfo.uv0 = mx_vec2{
            (float)(x * colsize) / (float)atlas_width,
            (float)(y * rowsize) / (float)atlas_height
        };
        charinfo.uv1 = mx_vec2{
            (float)((x + 1) * colsize) / (float)atlas_width,
            (float)((y + 1) * rowsize) / (float)atlas_height
        };
        // printf("Writing %c (%dx%d) at uv0:(%f,%f) - uv1:(%f,%f)\n", alphabet[i], x, y, charinfo.uv0.x, charinfo.uv0.y, charinfo.uv1.x, charinfo.uv1.y);

        res.characters.insert({(char)alphabet[i], charinfo});
    }
    res.tex = mx::texture::create(atlas, atlas_width, atlas_height, 1);

    return res;
}

MXRDEF mx::geometry_t mx::font::bake_text(mx::font_t font, const std::string& text)
{
    mx::vertex_textured_t quad_verts[]{
        // pos                  // color                 // uv              // texID
        {{ -0.5f, -0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, { 0.f, 1.f }, .0f}, // top left
        {{  0.5f, -0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, { 1.f, 1.f }, .0f}, // top right
        {{  0.5f,  0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, { 1.f, 0.f }, .0f}, // bottom right
        {{  0.5f,  0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, { 1.f, 0.f }, .0f}, // bottom right
        {{ -0.5f,  0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, { 0.f, 0.f }, .0f}, // bottom left
        {{ -0.5f, -0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, { 0.f, 1.f }, .0f}, // top left
    };
    mx::geometry_params_t alphabet_gp;
    alphabet_gp.max_shapes       = 1000;
    alphabet_gp.mode             = mx::draw_mode::TRIANGLES;
    alphabet_gp.vertex_per_shape = 6;
    alphabet_gp.shape_vertices   = &quad_verts[0];
    mx::geometry_t text_geometry = mx::geometry::create_dynamic(alphabet_gp);
    printf("Set up geometry\n");

    // baked quads
    float originy = -100.f, originx = -100.f;
    int c = 0;
    mx_vec3 char_pos{originx, originy};
    for(int i = 0; i < text.size(); i++)
    {
        if(text[i] == '\n')
        {
            originy -= font.descent + font.linegap + font.size;
            char_pos.x = originx;
            continue;
        }
        mx::fontchar_t fchar = font.characters[text[i]];
        
        // position of the quad
        char_pos.x += fchar.bearingx;
        char_pos.y  = originy + fchar.bearingy;

        // add baked quad
        mx::geometry::add_shape(
            &text_geometry,
            char_pos, 
            mx_vec3{(float)fchar.sizex, (float)fchar.sizey},
            fchar.uv0,
            fchar.uv1 
        );

        // advance for the next char
        char_pos.x += fchar.advance + font.kerning;
        ++c;
    }
    mx::geometry::add_shape(&text_geometry, mx_vec3{-128.f,-256.f}, mx_vec3{256.f, 256.f});
    mx::geometry::flush(&text_geometry);

    return text_geometry;
}
