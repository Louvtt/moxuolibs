#include <mx/addons/mxtilemap.h>
#include <bitset>

////////////////////////////////////////////
//                 TILES                  //
////////////////////////////////////////////

bool mx::tile_t::operator== (const mx::tile_t& o) 
{
    return o.id == this->id;
}

////////////////////////////////////////////
//                TILESET                 //
////////////////////////////////////////////

MXRDEF mx::tileset_t mx::tileset::load_tileset(const ::std::string& atlas)
{
    mx::tileset_t t;
    t.atlas = mx::texture::load_from_file(atlas.c_str());
    t.count = 0;
    t.tiles = { };
    return t;
}

MXRDEF void mx::tileset::auto_tiles(mx::tileset_t* tileset, vec2 tile_count)
{
    tileset->count = (int)tile_count.x * (int)tile_count.y;
    tileset->tiles = { };
    vec2 uv_per_tile = { 1.f / tile_count.x, 1.f / tile_count.y };
    for(int y = 0; y < tile_count.y; ++y)
    {
        for(int x = 0; x < tile_count.x; ++x)
        {
            int i = x + y * (int)tile_count.x;
            mx::tile_t tile;
            tile.id = i;
            tile.texpos  = { x * uv_per_tile.x, y * uv_per_tile.y };
            tile.texsize = { uv_per_tile };
            tileset->tiles.insert({ (mx::tiletype)i, tile });
        }
    }
}

MXRDEF void mx::tileset::add_tile(mx::tileset_t* tileset, vec2 pixelorigin, vec2 pixelsize, mx::tiletype type)
{
    mx::tile_t tile;
    tile.id = tileset->count;
    tile.texpos  = { pixelorigin.x / (float)tileset->atlas.width, pixelorigin.y / (float)tileset->atlas.height };
    tile.texsize = { pixelsize.x   / (float)tileset->atlas.width, pixelsize.y   / (float)tileset->atlas.height };
    tileset->tiles.insert({ type, tile });
    tileset->count += 1;
}

////////////////////////////////////////////
//                TILEMAP                 //
////////////////////////////////////////////

MXRDEF mx::tiletype mx::autotiling::test_4(mx::tilemap_t tilemap, vec2 tilepos, int tile_value)
{
    // not a valid cell, return 0
    if(!mx::tilemap::is_valid_cell(tilemap, (uint32_t)tilepos.x, (uint32_t)tilepos.y)) return mx::tiletype::NONE;
    if(tilemap.data[(int)tilepos.x][(int)tilepos.y] != tile_value) return mx::tiletype::NONE;

    std::bitset<4> n(0);
    vec2 dirs[] = {
        { 0, -1}, // bottom
        { 1,  0}, // right
        {-1,  0}, // left
        { 0,  1}, // top
    };
    for(int n_i = 0; n_i < 4; ++n_i) {
        vec2 dir = dirs[n_i];
        int xx = tilepos.x + (int)dir.x;
        int yy = tilepos.y + (int)dir.y;
        if(!mx::tilemap::is_valid_cell(tilemap, xx, yy))
        {
            n.set(n_i, true);
            continue;
        }

        n.set(n_i, (tilemap.data[xx][yy] != tile_value));
    }
    int bitsetval =
          1 * n.test(0) 
        + 2 * n.test(1) 
        + 4 * n.test(2) 
        + 8 * n.test(3);
    
    return (mx::tiletype)bitsetval;
}


MXRDEF mx::tilemap_t mx::tilemap::create(int width, int height, mx::tileset_t tileset, vec2 tile_pixelsize, int default_value)
{
    printf("Generating tilemap...\n");
    // PARAMS
    mx::tilemap_t tilemap;
    tilemap.height = height;
    tilemap.width  = width;
    tilemap.tileset = { tileset };
    int tile_count = tileset.count ? tileset.count : 1;
    tilemap.tile_pixelsize = { tile_pixelsize };
    tilemap.pos = { (float)width * -.5f, (float)height * -.5f, .0f};

    // BATCH TILES
    vec2 uv_per_tile = { 
        tilemap.tile_pixelsize.x / (float)tileset.atlas.width, 
        tilemap.tile_pixelsize.y / (float)tileset.atlas.height 
    };
    mx::vertex_textured_t quad_verts[]{
        // pos                  // color                 // uv                       // texID
        {{ -0.5f, -0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, {           0.f, uv_per_tile.y }, .0f}, // top left
        {{  0.5f, -0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, { uv_per_tile.x, uv_per_tile.y }, .0f}, // top right
        {{  0.5f,  0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, { uv_per_tile.x,           0.f }, .0f}, // bottom right
        {{ -0.5f,  0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, {           0.f,           0.f }, .0f}, // bottom left
    };
    uint32_t quad_indices[] = { 0, 1, 2, 2, 3, 0 };
    mx::geometry_params_t gp;
    gp.max_shapes        = width * height;
    gp.vertex_per_shape  = 4;
    gp.indices_per_shape = 6;
    gp.shape_indices     = quad_indices;
    gp.shape_vertices    = quad_verts;
    tilemap.tilebatch = mx::geometry::create_dynamic(gp);
    
    // GENERATING MAP AND FIRST QUAD
    mx_vec4 color = {1.f, 1.f, 1.f, 1.f};
    tilemap.data = new int*[width];
    for(uint32_t x = 0; x < width; ++x)
    {
        tilemap.data[x] = new int[height];
        for(uint32_t y = 0; y < height; ++y)
        {
            tilemap.data[x][y] =  tilemap.default_value;
            mx::geometry::add_shape(&tilemap.tilebatch, {(float)x, (float)y, .0f}, color, mx_vec2{0.f, 0.f}, (float)tilemap.data[x][y]);
        }
    }
    printf("-- DONE\n");

    return tilemap;
}

MXRDEF void mx::tilemap::generate_geometry(mx::tilemap_t tilemap)
{
    if(!tilemap.has_changed) return;
    mx::geometry::reset(&tilemap.tilebatch);
    for(uint32_t x = 0; x < tilemap.width; ++x)
    {
        for(uint32_t y = 0; y < tilemap.height; ++y)
        {
            vec2 computedUV;
            int tile_idx    = tilemap.data[x][y];
            mx::tile_t tile = tilemap.tileset.tiles[(mx::tiletype)tile_idx];
            computedUV      = { tile.texpos };
            mx::geometry::add_shape(&tilemap.tilebatch, {(float)x, (float)y, .0f}, computedUV);
        }
    }
    mx::geometry::flush(&tilemap.tilebatch);
}

MXRDEF void mx::tilemap::generate_geometry_with_autotiling(mx::tilemap_t tilemap)
{
    if(!tilemap.has_changed) return;
    mx::geometry::reset(&tilemap.tilebatch);
    for(int x = 0; x < tilemap.width; ++x) {
        for(int y = 0; y < tilemap.height; ++y)
        {

            mx::tiletype type = mx::autotiling::test_4(tilemap, { (float)x, (float)y }, tilemap.tileset.map_value);
            if(tilemap.tileset.tiles.find(type) == tilemap.tileset.tiles.end())
                type = mx::tiletype::NONE;
            mx::tile_t tile = tilemap.tileset.tiles[type];

            // apply tile found
            mx::geometry::add_shape(&tilemap.tilebatch, {float(x), float(y), .0f}, tile.texpos);
        }
    }
    mx::geometry::flush(&tilemap.tilebatch);
}


MXRDEF bool mx::tilemap::is_valid_cell(mx::tilemap_t tilemap, uint32_t x, uint32_t y)
{
    return (0 <= x && x < (int)tilemap.width)
        && (0 <= y && y < (int)tilemap.height);
}

MXRDEF void mx::tilemap::set_tile(mx::tilemap_t* tilemap, uint32_t x, uint32_t y, int value)
{
    if(!is_valid_cell(*tilemap, x, y)) return;
    tilemap->data[x][y]  = value;
    tilemap->has_changed = true;
}

MXRDEF void mx::tilemap::draw(mx::tilemap_t tilemap, mx::shader_t shader)
{
    // Model matrix
    mx_mat4 model = mx_mat4_translate(mx_mat4_identity(), tilemap.pos);
    mx::shader::begin(shader);

    mx::texture::bind(tilemap.tileset.atlas, 1);
    mx::shader::set_uniform(shader, "tile_atlas", 1);
    mx::shader::set_uniform(shader, "u_tilingFactor", 1.f); // set to 1

    mx::shader::set_uniform_mat(shader, "u_model", mx_mat4_raw(model).ptr, 4);
    
    mx::geometry::render(&tilemap.tilebatch, shader);

    mx::shader::end(shader);
}
