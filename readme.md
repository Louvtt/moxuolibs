# Moxuo Libs

Libs for making game in c++
or graphical app

- mxmaths: small c/c++ maths header lib.
- mxrender: small c++ graphical static lib. (.h and .cpp to include)
- ?

The libs require glfw for now.
(+ glad and stb_image that are included by default)

## MX TODOS

- [x] Remake the folder architecture (need to rework cmake files, im bad at writting them)
- [x] Basic Context
- [x] Shaders
- [x] 2D Textures
- [x] Buffer layout
- [x] Vertex buffers wrappers (static and dynamic)
- [x] batching vertex (simple, colored, textured)
- [x] Camera (orthographic + perspective)
- [x] Camera controller
- [x] Simple events (events type and manager)
- [x] text rendering (uses stb_true_type)
- [x] ressource manager
- [ ] simple gui
- [ ] more...

Addons:

- [x] tilemap renderer
- [x] tileset
- [x] tile rule (needs work)

## BUILDING

To build the lib you need to install the dependencies (GLFW):

- You have the git cli installed :
   Go to the script subfolder and run `install_dependencies.bat`
- You don't : install the lib via [the glfw git page](https://github.com/glfw/glfw) and put the files in the `external` folder.

The build use [cmake](https://cmake.org/download/) and [cmake-addons](https://github.com/rpavlik/cmake-modules).
(Put the cmake-addons file in the `external` folder).

To create a project with the libs, just add the files in your projet in a sub folder (e.g "external", "libs", "vendors") and copy paste the `CMakeFiles.txt` in `example/template/`.
