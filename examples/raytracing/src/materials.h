#ifndef MATERIALS_H
#define MATERIALS_H

#include <unordered_map>

#include "utils.h"
#include "hit_record.h"

struct material_t {
    typedef bool (*scatter_fnc)(const ray_t& ray_in, const hit_record_t& rec, mx_vec3& attenuation, ray_t& scattered, material_t* mat);
    struct matdata {
        mx_vec3 v3_0{};
        float   f_0 = 1.f;
    };

    std::string id;
    scatter_fnc scatter;
    matdata params;
};
static std::unordered_map<std::string, material_t> materials{
    {"default", {
        "default",
        [](const ray_t& ray_in, const hit_record_t& rec, mx_vec3& attenuation, ray_t& scattered, material_t* mat) {
            attenuation = { rec.normal };
            scattered = { rec.point, rec.normal };
            return true;
        },
        {}
    }}
};

namespace materiallib {
    inline void log_material_table() {
        __mxm_log("Material Table: \n\n");
        for(const auto& pair : materials) {
            __mxm_log("Material: %s\n", pair.first.c_str());
        }
        __mxm_log("\n==========================\n\n");
    }


    inline material_t get(const std::string& mat_id) {
        if(materials.find(mat_id) == materials.end()) return materials["default"];
        return materials.at(mat_id);
    }

    #define CREATE_MAT(name, ...)                                                                     \
    int count_##name = 0;                                                                             \
    inline material_t* add_##name(__VA_ARGS__) {                                                      \
        std::string mat_id = std::string(#name) + "_" + std::to_string(count_##name);                 \
        count_##name+=1; /*printf_s("Creating %s material with id : %s.\n", #name, mat_id.c_str());*/ \
        materials.insert({ mat_id, { mat_id,                                                          \
        [](const ray_t& ray_in, const hit_record_t& rec, mx_vec3& attenuation, ray_t& scattered, material_t* mat) {

    #define END_MAT(...) },{ __VA_ARGS__ } }}); return &materials.at(mat_id); }

    // LAMBERTIAN
    CREATE_MAT(lambertian, mx_vec3 albedo)
        mx_vec3 scatter_dir = mx_vec3_add(rec.normal, mx_vec3_random_unit_sphere());

        // Catch degenerate scatter direction
        if (mx_near_zero(scatter_dir))
            scatter_dir = rec.normal;

        scattered.origin = rec.point;
        scattered.dir    = scatter_dir;
        attenuation = mat->params.v3_0;
        return true;
    END_MAT(albedo)

    // MIROR (METAL)
    CREATE_MAT(metal, mx_vec3 albedo)
        mx_vec3 reflected = mx_vec3_refl(ray_in.dir, rec.normal);
        scattered = { rec.point, reflected };
        attenuation = mat->params.v3_0;
        return (mx_vec3_dot(scattered.dir, rec.normal) > 0);
    END_MAT(albedo)

    // FUZZY REFLECTION
    CREATE_MAT(fuz_metal, mx_vec3 albedo, float fuzz)
        mx_vec3 reflected = mx_vec3_refl(ray_in.dir, rec.normal);
        mx_vec3 fuzz = mx_vec3_mulf(mx_vec3_random_unit_sphere(), mat->params.f_0);
        scattered = { rec.point, mx_vec3_add(reflected, fuzz) };
        attenuation = mat->params.v3_0;
        return (mx_vec3_dot(scattered.dir, rec.normal) > 0);
    END_MAT(albedo, fuzz)

    // DIOPTRE (dielectric)
    CREATE_MAT(dielectric, float ir, mx_vec3 albedo = {1.f, 1.f,1.f})
        attenuation = mat->params.v3_0;
        float refraction_ratio = rec.front_face ? (1.f/mat->params.f_0) : mat->params.f_0;

        mx_vec3 unit_dir = mx_vec3_normalize(ray_in.dir);
        float cos_theta = std::fmin(mx_vec3_dot(mx_vec3_mulf(unit_dir, -1.f), rec.normal), 1.f);
        float sin_theta = sqrtf(1.f - cos_theta*cos_theta);

        bool cannot_refract = refraction_ratio * sin_theta > 1.f;
        bool steepness = reflectance(cos_theta, refraction_ratio) > mx_random();
        mx_vec3 dir = (cannot_refract || steepness) ? mx_vec3_refl(unit_dir, rec.normal) : refract(unit_dir, rec.normal, refraction_ratio);

        scattered = { rec.point, dir };
        return true;
    END_MAT(albedo, ir)

    // DEFINE NEW MATERIALS HERE
}

#endif //MATERIALS_H