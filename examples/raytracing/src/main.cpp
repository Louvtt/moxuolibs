#define MXM_INCLUDE_IMPLEMENTATION
#include <mx/mxmaths.h>
#include <mx/mxrender.h>
#include <mx/mxgui.h>

#include "ray.h"
#include "camera.h"
#include "params.h"

#include "denoising.h"

//////////////////////////////////////////////

hittable_list<sphere> random_scene() {
    hittable_list<sphere> world;

    material_t* gmat = materiallib::add_lambertian({.5f, .5f, .5f});
    world.add(sphere::create({0.f,-1000.f,0.f}, 1000.f, gmat));

    material_t* glass_mat = materiallib::add_dielectric(1.5f);

    for (int a = -RANDOM_SCENE_SIZE; a < RANDOM_SCENE_SIZE; a++) {
        for (int b = -RANDOM_SCENE_SIZE; b < RANDOM_SCENE_SIZE; b++) {
            auto choose_mat = mx_random();
            mx_vec3 center{a + .9f*mx_random(), .2f, b + .9f*mx_random()};

            if (mx_vec3_mag(mx_vec3_sub(center, {4.f, .2f, 0.f})) > .9f) {
                material_t* smat;

                if (choose_mat < .8f) {
                    // diffuse
                    auto albedo = mx_vec3_mul(mx_vec3_random(), mx_vec3_random());
                    smat = materiallib::add_lambertian(albedo);
                    world.add(sphere::create(center, .2f, smat));
                } else if (choose_mat < .95f) {
                    // metal
                    auto albedo = mx_vec3_random_range(.5f, 1.f);
                    auto fuzz = mx_random_range(0.f, .5f);
                    smat = materiallib::add_fuz_metal(albedo, fuzz);
                    world.add(sphere::create(center, .2f, smat));
                } else {
                    // glass
                    world.add(sphere::create(center, .2f, glass_mat));
                }
            }
        }
    }

    world.add(sphere::create({0.f, 1.f, 0.f}, 1.f, glass_mat));

    world.add(sphere::create({-4.f, 1.f, 0.f}, 1.f, materiallib::add_lambertian({.4f,.2f,.1f})));

    world.add(sphere::create({4.f, 1.f, 0.f}, 1.f, materiallib::add_metal({.7f,.6f,.5f})));

    return world;
}

//////////////////////////////////////////////

static mx_vec3 get_raw_color(mx::image_data_t img, mx_vec3* raw, unsigned x, unsigned y) {
    return raw[x + y * img.width];
}

static void write_color(mx::image_data_t* img, mx_vec3* raw, int x, int y, mx_vec3 col, float samples = SAMPLES_PER_PIXEL) {
    float r = col.x;
    float g = col.y;
    float b = col.z;
    
    // antialiasing
    // + gamma correction
    r = powf(r / samples, 1.f / GAMMA_CORRECTION);
    g = powf(g / samples, 1.f / GAMMA_CORRECTION);
    b = powf(b / samples, 1.f / GAMMA_CORRECTION);

    // write to texture
    mx_vec3 result = {r,g,b};
    mx::image_data::write_pixel(img, x, y, result);
    raw[x + y * img->width] = col;
}

#include <fstream>
// doesn't work properly, need to be reworked.
static void write_to_ppm(const std::string& out_file, mx::image_data_t img) {
    std::ofstream f;
    f.open(out_file.c_str());
    if(!f.is_open()) {
        std::cerr << "ERROR: file cannot be opened (" << out_file << ").\n";
        return;
    }
    __mxm_log("Writing output to %s...\n", out_file.c_str());
    // f.seekp(0); // begin to the top

    // // TARGA HEADER: http://www.paulbourke.net/dataformats/tga/
    // f.put(0);
    // f.put(0);
    // f.put(2);           //TYPE 2 (uncompressed bgr)
    // f.put(0); f.put(0); // Color Map Specification 
    // f.put(0); f.put(0);
    // f.put(0);
    // // Image Specification.
    // f.put(0); f.put(0); // X origin
    // f.put(0); f.put(0); // Y origin
    // // Image width
    // f.put( 0x00FF & img.width);          // LOW
    // f.put((0xFF00 & img.width) / 256);  // HIGH
    // // image height
    // f.put( 0x00FF & img.height);        // LOW
    // f.put((0xFF00 & img.height) / 256); // HIGH
    // f.put(img.channels * 8U); // channel with 8 bit - bitmap
    // f.put(0);  // Image identification field

    // // Write data backwards
    // uint32_t imgsize = img.height*img.width*img.channels;
    // uint8_t* rpixels = new uint8_t[imgsize];
    // for(int y = 0; y < img.height; ++y) {
    //     for(int x = 0; x < img.width; ++x) {
    //         int idx = (x + y * img.width) * img.channels;
    //         // swap rgb to bgr
    //         rpixels[idx + 2] = img.pixels[idx + 0]; // r
    //         rpixels[idx + 1] = img.pixels[idx + 1]; // g
    //         rpixels[idx + 0] = img.pixels[idx + 2]; // b
    //     }
    // }
    // f.write((char*)rpixels, imgsize);

    f << "P3\n" << img.width << ' ' << img.height << "\n255\n";
    for(int i = 0; i < img.height*img.width*img.channels; i+=img.channels) {
        f << static_cast<int>(img.pixels[i + 0]) << " " << static_cast<int>(img.pixels[i + 1]) << " " << static_cast<int>(img.pixels[i + 2]) << "\n";
    }
    f.close();
}

//////////////////////////////////////////////

int main(int argc, char* argv[])
{
    printf("Setup context\n");
    mx::context_t ctx = mx::context::create("mxRayCast Test", 1000, 1000, true);
    
    printf("Load shaders\n");
    mx::shader_t quad_shader = mx::shader::load_from_files("../../assets/shaders/quad.vs", "../../assets/shaders/quad.fs");
    mx::shader_t img_shader = mx::shader::load_from_files("../../assets/shaders/image.vs", "../../assets/shaders/image.fs");

    // setup image
    printf("Setup shader uniforms\n");
    double ctx_ratio = ctx.width / ctx.height;
    mx::shader::begin(quad_shader);
    mx::shader::set_uniform(quad_shader, "u_aspect", ctx_ratio);
    mx::shader::begin(img_shader);
    mx::shader::set_uniform(img_shader, "u_aspect", ctx_ratio);

    // IMG
    printf("Setup image buffer\n");
    float aspect_ratio      = ASPECT_RATIO;
    constexpr int img_width = IMG_WIDTH;
    mx_ivec2 image_size     = { img_width, (int)(img_width / aspect_ratio) };
    mx::image_data_t img    = mx::image_data::create(image_size.x, image_size.y);
    mx_vec3* raw_img = new mx_vec3[image_size.x*image_size.y];

    // Camera
    printf("Setup camera\n");
    mx_vec3 o = { 5.f, 10.f, 2.f};
    mx_vec3 t = { 0.f,  .5f, 0.f};
    raycast_camera_t cam = raycast_camera::create(
        o,
        t,
        { 0.f, 1.f, 0.f},
        13.f,//mx_vec3_mag(mx_vec3_sub(o, t)),
        40.f,
        ASPECT_RATIO, 
        .1f
    );

    // UI
    printf("Setup gui\n");
    mx::progress_bar_t progress = mx::progress_bar::create({.0f, .89f, .0f}, .0f, image_size.y*image_size.x*SAMPLES_PER_PIXEL, { .5f, .1f }, false);
    mx::mesh_t renderQuad = mx::mesh::quad({.0f, .0f, .0f}, 1.f);
    renderQuad.scale.x = aspect_ratio;
    renderQuad.scale.y = 1.f;

    mx::texture_t tex = mx::texture::create(img);

    // setup scene
    printf("Setup scene\n");
    hittable_list<sphere> world;

    world = random_scene();
    // materiallib::log_material_table();

    // ScanLines
    printf("Begin rendering\n");
    double start_time = glfwGetTime();
    double st = start_time;
    for(int csamples = 1; csamples < SAMPLES_PER_PIXEL; csamples++) {
        for(int y = 0; y < image_size.y; ++y)
        {
            // render
            mx::context::pre_render(&ctx);
            mx::context::set_uniforms(&ctx, quad_shader);

            for(int x = 0; x < image_size.x; ++x) {
                
                mx_vec3 col{};
                if(csamples > 1) col = get_raw_color(img, raw_img, x, y);

                // SAMPLE
                float u = (float)(x + mx_random()) / (image_size.x-1);
                float v = (float)(y + mx_random()) / (image_size.y-1);
                ray_t ray = raycast_camera::get_ray(cam, u, v);
                col = mx_vec3_add(col, ray::color(ray, world));

                write_color(&img, raw_img, x, y, col, csamples);

                progress.value += 1.f;
            }


            // Update scene
            mx::texture::set_data(tex, img);
            mx::texture::bind(tex);
            mx::shader::set_uniform(img_shader, "u_tex", 0);
            mx::mesh::draw(renderQuad, img_shader);
            mx::texture::unbind(tex);
            mx::progress_bar::draw(progress, quad_shader);
            mx::context::post_render(&ctx);
            if(mx::context::should_close(ctx)) break;
        }
        printf("Sample %d took %f seconds !\n", csamples, glfwGetTime() - st);
        st = glfwGetTime();
        // denoising::gaussian_filter(&img);
        // printf("Denoising took %f seconds !\n", glfwGetTime() - st);
        // st = glfwGetTime();
        write_to_ppm("./out.ppm", img);
        printf("Writing ppm took %f seconds !\n", glfwGetTime() - st);
        st = glfwGetTime();

        // Update scene
        mx::texture::set_data(tex, img);
        mx::texture::bind(tex);
        mx::shader::set_uniform(img_shader, "u_tex", 0);
        mx::mesh::draw(renderQuad, img_shader);
        mx::texture::unbind(tex);
        mx::progress_bar::draw(progress, quad_shader);
        mx::context::post_render(&ctx);
        if(mx::context::should_close(ctx)) break;
    }
    printf("Done in %f seconds !\n", glfwGetTime() - start_time);

    // END LOOP JUST WAITING
    while(!mx::context::should_close(ctx)) {
        // render
        mx::context::pre_render(&ctx);
        mx::context::set_uniforms(&ctx, quad_shader);

        mx::texture::bind(tex);
        mx::shader::set_uniform(img_shader, "u_tex", 0);
        mx::mesh::draw(renderQuad, img_shader);
        mx::texture::unbind(tex);
        
        mx::context::post_render(&ctx);
    }
    mx::context::end();

    return 0;
}