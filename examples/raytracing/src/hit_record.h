#ifndef HIT_RECORD_H
#define HIT_RECORD_H

#include <mx/mxmaths.h>

#include "ray.h"

struct material_t;
struct hit_record_t {
    mx_vec3 point;
    float t;
    mx_vec3 normal;
    material_t* mat_ptr;
    bool front_face;

    inline void set_face_normal(const ray_t& ray, const mx_vec3& outward_n)
    {
        front_face = mx_vec3_dot(ray.dir, outward_n) < .0f;
        normal     = front_face ? outward_n : mx_vec3_mulf(outward_n, -1.f);
    }
};


typedef bool (*hit_event)(void* obj, const ray_t& ray, float tmin, float tmax, hit_record_t& rec);

struct hittable {
    hit_event hit_callback = [](void* obj, const ray_t& ray, float tmin, float tmax, hit_record_t& rec) { 
        return false;
    };
    material_t* mat_ptr;
};


#endif //HIT_RECORD_H