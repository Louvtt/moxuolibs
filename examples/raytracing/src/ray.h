#ifndef RAY_H
#define RAY_H

#include <mx/mxmaths.h>
#include <vector>

//////////////////////

#include "params.h"
#include "utils.h"

//////////////////////////////////
// RAY

struct ray_t {
    mx_vec3 origin;
    mx_vec3 dir;
};

namespace ray {
    ray_t cast(mx_vec3 origin, mx_vec3 dir) {
        ray_t r = {
            origin, dir
        };
        return r;
    }
    mx_vec3 at(const ray_t& ray, float t) {
        return mx_vec3_add(ray.origin, mx_vec3_mulf(ray.dir, t));
    }
}

///////////////////////////////

#include "materials.h"
#include "hittable.h"

///////////////////////////////
// RAY FNC

namespace ray {
    template <typename T>
    mx_vec3 color(const ray_t& ray, hittable_list<T> world, int depth = BOUNCE_LIMIT+1) {
        // draw scene
        hit_record_t rec;

        // exceed bounce limit fallback
        if(depth <= 0) return {.0f, .0f, .0f};

        if(world.hit(ray, 0.001F, infinity, rec)) {
            ray_t scattered;
            mx_vec3 attenuation;
            if(rec.mat_ptr->scatter(ray, rec, attenuation, scattered, rec.mat_ptr)) {
                return mx_vec3_mul(attenuation, color(scattered, world, depth - 1));
            }
            return {.0f, .0f, .0f};
        }

        // sky
        mx_vec3 unit_direction = mx_vec3_normalize(ray.dir);
        float t = 0.5f * (unit_direction.y + 1.0f);
        return mx_interpolate_vec3({1.0, 1.0, 1.0}, {0.5, 0.7, 1.0}, t);
    }
}

#endif //RAY_H
