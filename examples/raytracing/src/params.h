#ifndef PARAMS_H
#define PARAMS_H

// PARAMS
#define SAMPLES_PER_PIXEL 100
#define ASPECT_RATIO      6.f / 13.f
#define IMG_WIDTH         540

#define GAMMA_CORRECTION  2.f

#define BOUNCE_LIMIT      10

#define CAM_FOV           20

////////////////////
#define RANDOM_SCENE_SIZE 3

// convert some params
constexpr float inv_sample_per_pixels = 1.f / SAMPLES_PER_PIXEL;

#endif //PARAMS_H