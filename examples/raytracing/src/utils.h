#ifndef UTILS_H
#define UTILS_H

#include <cmath>
#include <limits>
#include <memory>

#include <cstdlib>
#include <random>

#include <iostream>

/////////////////////
/// UTILS

constexpr float infinity = std::numeric_limits<float>::infinity();
constexpr float pi = 3.1415926535897932385f;

/////////////////////////////


template <typename T>
using sptr = std::shared_ptr<T>;
template <typename T, typename ... Args>
constexpr inline sptr<T> create_sptr(Args&& ... args) {
    return std::make_shared<T>(std::forward<Args>(args)...);
}

template <typename T>
using uptr = std::unique_ptr<T>;
template <typename T, typename ...Args>
constexpr inline uptr<T> create_uptr(Args&& ...args) {
    return std::make_unique<T>(std::forward<Args>(args)...);
}

////////////////////////////

inline mx_vec3 refract(const mx_vec3& uv, const mx_vec3& n, float etai_over_etat) {
    float cos_theta = std::fmin(mx_vec3_dot(mx_vec3_mulf(uv, -1.f), n), 1.f);

    mx_vec3 r_out_perp = mx_vec3_mulf(mx_vec3_add(uv, mx_vec3_mulf(n, cos_theta)), etai_over_etat);
    mx_vec3 r_out_parallel = mx_vec3_mulf(n, -sqrtf(fabs(1.0 - mx_vec3_dot(r_out_perp,r_out_perp))));
    return mx_vec3_add(r_out_perp, r_out_parallel);
}

inline float reflectance(float cosine, float ref_idx) {
    // Use Schlick's approximation for reflectance.
    float r0 = (1.f-ref_idx) / (1.f+ref_idx);
    r0 = r0*r0;
    return r0 + (1.f-r0)*powf((1.f - cosine),5.f);
}

#endif //UTILS_H