#ifndef CAMERA_H
#define CAMERA_H

#include "ray.h"

struct raycast_camera_t {
    mx_vec3 origin;
    mx_vec3 bottom_left_corner;
    mx_vec3 horizontal;
    mx_vec3 vertical;

    mx_vec3 u, v, w;
    float lens_radius;
};
namespace raycast_camera {
    raycast_camera_t create(mx_vec3 origin, mx_vec3 target, mx_vec3 vup, float focus_dist, float fov = CAM_FOV, float aspect_ratio = ASPECT_RATIO, float aperture = 2.f) {
        raycast_camera_t cam;

        float theta        = MX_DEG2RAD(fov);
        float h            = tanf(theta/2.f);
        float viewporth    = 2.f * h;
        float viewportw    = aspect_ratio * viewporth;

        cam.w = mx_vec3_normalize(mx_vec3_sub(origin, target));
        cam.u = mx_vec3_normalize(mx_vec3_cross(vup, cam.w));
        cam.v = mx_vec3_cross(cam.w, cam.u);

        cam.origin     = origin;
        cam.horizontal = mx_vec3_mulf(cam.u, viewportw * focus_dist);
        cam.vertical   = mx_vec3_mulf(cam.v, viewporth * focus_dist);
        cam.bottom_left_corner = {
            cam.origin.x - cam.horizontal.x * .5f - cam.vertical.x * .5f - focus_dist*cam.w.x,
            cam.origin.y - cam.horizontal.y * .5f - cam.vertical.y * .5f - focus_dist*cam.w.y,
            cam.origin.z - cam.horizontal.z * .5f - cam.vertical.z * .5f - focus_dist*cam.w.z,
        };

        cam.lens_radius = aperture / 2.f;

        return cam;
    }

    ray_t get_ray(raycast_camera_t cam, float u, float v) {
        mx_vec3 rd  = mx_vec3_mulf(mx_vec3_random_unit_disk(), cam.lens_radius);
        mx_vec3 off = mx_vec3_add(mx_vec3_mulf(cam.u, rd.x), mx_vec3_mulf(cam.v, rd.y));

        mx_vec3 dir = {
            cam.bottom_left_corner.x + u*cam.horizontal.x + v*cam.vertical.x - cam.origin.x - off.x,
            cam.bottom_left_corner.y + u*cam.horizontal.y + v*cam.vertical.y - cam.origin.y - off.y,
            cam.bottom_left_corner.z + u*cam.horizontal.z + v*cam.vertical.z - cam.origin.z - off.z
        };
        return ray::cast(mx_vec3_add(cam.origin, off), dir);
    }
}


#endif //CAMERA_H