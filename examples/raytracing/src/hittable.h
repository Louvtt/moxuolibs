#ifndef HITTABLE_H
#define HITTABLE_H

/////////////////////////////////
// HITTABLE

#include "hit_record.h"
#include "materials.h"

struct sphere : public hittable {
    mx_vec3 pos;
    float radius;

    static sphere create(mx_vec3 pos, float radius, material_t* mat_ptr)
    {
        sphere s;
        s.pos    = pos;
        s.radius = radius;
        s.mat_ptr = mat_ptr;

        s.hit_callback = [](void* obj, const ray_t& ray, float tmin, float tmax, hit_record_t& rec) {
            sphere* o = (sphere*)obj;

            mx_vec3 oc = mx_vec3_sub(ray.origin, o->pos);
            float   a  = mx_vec3_dot(ray.dir, ray.dir);
            float  hb  = mx_vec3_dot(oc, ray.dir);
            float   c  = mx_vec3_dot(oc, oc) - o->radius*o->radius;

            // discriminant
            float   d  = hb*hb - a*c;
            if (d < .0f) return false;
            float sqrtd = sqrtf(d);

            // root
            float root = (-hb - sqrtd) / a;
            if(root < tmin || root > tmax) {
                root = (-hb + sqrtd) / a;
                if(root < tmin || root > tmax)
                    return false;
            }

            rec.t      = root;
            rec.point  = ray::at(ray, rec.t);
            rec.mat_ptr = o->mat_ptr;
            mx_vec3 onormal = mx_vec3_mulf(mx_vec3_sub(rec.point, o->pos), 1.f / o->radius);
            rec.set_face_normal(ray, onormal);

            return true;
        };
        return s;
    }
};

template <typename T>
struct hittable_list {

    std::vector<T> list{};

    inline void add(T h)
    {
        list.push_back(h);
    }

    inline bool hit(const ray_t& ray, float tmin, float tmax, hit_record_t& rec)
    {
        hit_record_t tmp_rec;
        bool has_hit = false;
        float closest = tmax;

        for(int i = 0; i < list.size(); ++i) 
        {
            T obj = list[i];
            if(obj.hit_callback(&obj, ray, tmin, closest, tmp_rec)) {
                has_hit = true;
                closest = tmp_rec.t;
                rec = tmp_rec;
            }
        }

        return has_hit;
    }
};
#endif //HITTABLE_H
