#ifndef DENOISING_H
#define DENOISING_H

#include <mx/mxrender.h>
#include <mx/mxmaths.h>

#include <queue>
#include <array>

namespace denoising {
    static float intensity(const mx_vec3& rgb) {
        return mx_rgb2hsl(rgb).y;
    }
    
    static void median_filter(mx::image_data_t* img, const int& size = 3) {
        // number of pixel values in image (pixel_count * channels)
        uint32_t pixelsize = img->width*img->height*img->channels;
        // original data
        uint8_t* pixels = new uint8_t[pixelsize];
        memcpy(&pixels[0], &img->pixels[0], pixelsize*sizeof(uint8_t));
        
        for(int y = size; y < img->height - size; ++y) {
            for(int x = size; x < img->width - size; ++x) {
                std::vector<mx_vec3> neighbours{};

                // Window check
                int idx = 0;
                for(int yy = -size; yy < size; ++yy) {
                    int _y = y + yy;
                    for(int xx = -size; xx < size; ++xx) {
                        int _x = x + xx;
                        // Get pixel data
                        uint32_t i = (_x + _y * img->width) * img->channels; 
                        mx_vec3 p = {
                            static_cast<float>(pixels[i + 0] / 255.99f), // R
                            static_cast<float>(pixels[i + 1] / 255.99f), // G
                            static_cast<float>(pixels[i + 2] / 255.99f), // B
                        };
                        neighbours.emplace_back(p);
                        ++idx;
                    }
                }

                // sort values (simple insertion sort)
                for(int i = 1; i < neighbours.size(); ++i) {
                    float x = intensity(neighbours[i]); // intensity
                    int j = i - 1;
                    while(j >= 0 && intensity(neighbours[j]) < x) {
                        neighbours[j+1] = neighbours[j];
                        --j;
                    }
                    neighbours[j+1] = neighbours[i];
                }
                // get the median value
                mx_vec3 m = neighbours[floorf((float)neighbours.size() / 2.f)];

                // write result to image
                mx::image_data::write_pixel(img, x, y, m);
            }
        }
    }

    static void gaussian_filter(mx::image_data_t* img) {
        // number of pixel values in image (pixel_count * channels)
        uint32_t pixelsize = img->width*img->height*img->channels;
        // original data
        uint8_t* pixels = new uint8_t[pixelsize];
        memcpy(&pixels[0], &img->pixels[0], pixelsize*sizeof(uint8_t));
        
        for(int y = 1; y < img->height - 1; ++y) {
            for(int x = 1; x < img->width - 1; ++x) {
                uint32_t idx = (x + y * img->width) * img->channels; 
                mx_vec3 m = {
                    static_cast<float>(pixels[idx + 0] / 255.99f), // R
                    static_cast<float>(pixels[idx + 1] / 255.99f), // G
                    static_cast<float>(pixels[idx + 2] / 255.99f), // B
                };

                mx::image_data::write_pixel(img, x, y, m);
            }
        }
    }
}


#endif //DENOISING_H