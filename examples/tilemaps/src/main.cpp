#include <stdio.h>
#include <iostream>

#include <mx/mxrender.h>
#include <mx/addons/mxtilemap.h>

#include <random>
#include <chrono>

#include <PerlinNoise.hpp>

static void generate_map(mx::tilemap_t map)
{
    uint32_t seed = std::chrono::steady_clock().now().time_since_epoch().count();
    siv::PerlinNoise perlin(seed);

    double frequency = 4.0;
    double fx = (double)map.width  / frequency;
    double fy = (double)map.height / frequency;

    uint32_t octaves = 8;

    float w = map.width;
    float h = map.height;

    // RANDOMIZE MAP
    for(uint32_t x = 0; x < map.width; ++x)
    {
        for(uint32_t y = 0; y < map.height; ++y)
        {
            float value = perlin.accumulatedOctaveNoise2D_0_1((double)x / fx, (double)y / fy, octaves) * 4.f;
            float d = 1.f - (mx_vec2_mag({ (float)x / w - .5f, (float)y / h  - .5f }) * 1.2f);
            value *= d;
            value = (value >= 1.f)? 1.f : 0.f;
            mx::tilemap::set_tile(&map, x, y, (int)value);
        }
    }

    
    mx::tilemap::generate_geometry_with_autotiling(map);
}

mx::tilemap_t map;
mx::tileset_t tilesets[3];
int active_tileset_idx = 0;
static void tilesetcallback(void* obj, int key, int scancode, int mods)
{
    if(key == GLFW_KEY_L)
    {
        active_tileset_idx++;
        if(active_tileset_idx == 3)
        {
            active_tileset_idx = 0;
        }

        map.tileset = tilesets[active_tileset_idx];
        mx::tilemap::generate_geometry_with_autotiling(map);
    }
}

int main(int argc, char* argv[])
{
    printf("[Running app at %s]\n", mx::__mxr_cwd().c_str());

    printf("Creating context...\n");
    mx::context_t ctx = mx::context::create("mxTestBed", 700, 700, true);

    printf("Creating shader...\n");
    mx::shader_t s = mx::shader::load_from_files("../../assets/shaders/tilemap.vs", "../../assets/shaders/tilemap.fs");

    printf("Creating texture...\n");
    mx::texture_t atlas = mx::texture::load_from_file("../../assets/autotiles.png");

    uint32_t size = 32;
    if(argc > 1)
    {
        size = std::stoi(argv[1], nullptr, 10);
    }
    ctx.camera = mx::camera::create({ .0f, .0f, .0f }, { 700.f, 700.f }, size);
    ctx.camera.controller.move_speed = 800.f;
    
    printf("Loading tileset...\n");
    tilesets[0] = mx::tileset::load_tileset("../../assets/autotiles_grass.png");
    tilesets[1] = mx::tileset::load_tileset("../../assets/autotiles_.png");
    tilesets[2] = mx::tileset::load_tileset("../../assets/grass_water_tileset.png");
    mx::tileset::auto_tiles(&tilesets[1], { 4, 4 });
    mx::tileset::auto_tiles(&tilesets[2], { 4, 4 });
    
    mx::tileset::add_tile(&tilesets[0], {  0,  0 }, { 16, 16 }, mx::tiletype::TL_CORNER);
    mx::tileset::add_tile(&tilesets[0], { 16,  0 }, { 16, 16 }, mx::tiletype::TOP);
    mx::tileset::add_tile(&tilesets[0], { 32,  0 }, { 16, 16 }, mx::tiletype::TR_CORNER);
    mx::tileset::add_tile(&tilesets[0], {  0, 16 }, { 16, 16 }, mx::tiletype::LEFT);
    mx::tileset::add_tile(&tilesets[0], { 16, 16 }, { 16, 16 }, mx::tiletype::CENTER);
    mx::tileset::add_tile(&tilesets[0], { 32, 16 }, { 16, 16 }, mx::tiletype::RIGHT);
    mx::tileset::add_tile(&tilesets[0], {  0, 32 }, { 16, 16 }, mx::tiletype::BL_CORNER);
    mx::tileset::add_tile(&tilesets[0], { 16, 32 }, { 16, 16 }, mx::tiletype::BOTTOM);
    mx::tileset::add_tile(&tilesets[0], { 32, 32 }, { 16, 16 }, mx::tiletype::BR_CORNER);
    mx::tileset::add_tile(&tilesets[0], { 48,  0 }, { 16, 16},  mx::tiletype::NONE);

    printf("Creating tilemap...\n");
    map = mx::tilemap::create(size, size, tilesets[0], {16.f, 16.f }, 0);

    // switch sand and grass
    generate_map(map);

    mx::event_manager::register_on_key_pressed(&ctx.event_manager, &ctx, (mx::events::key_pressed)tilesetcallback);
    // glfwSetKeyCallback(ctx.window_handle, tilesetcallback);

    while(!mx::context::should_close(ctx))
    {
        mx::context::pre_render(&ctx);
        mx::context::set_uniforms(&ctx, s);

        mx::tilemap::draw(map, s);

        mx::context::post_render(&ctx);
    }
    mx::context::end();
    return 0;
};