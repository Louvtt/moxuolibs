#ifndef _MX_TEXTRENDERER_H_
#define _MX_TEXTRENDERER_H_


#include <mx/mxrender.h>
#include <map>
#include <string>
namespace mx {

    struct fontchar_t {
        int sizex, sizey;
        int bearingx, bearingy;
        int advance;

        mx_vec2 uv0;
        mx_vec2 uv1;
    };

    struct font_t {
        std::string name;
        std::map<char, fontchar_t> characters;
        
        mx::texture_t tex;

        float scale_factor = 1.f;
        int baselineH = 0.f;
        int descent, ascent;
        int linegap = 1;
        int kerning = 2;
        int size = 32;
    };

    namespace font {
        MXRDEF font_t load(const std::string& name, int size);
        
        
        MXRDEF mx::geometry_t bake_text(mx::font_t font, const std::string& text);
    }
}

#endif //_MX_TEXTRENDERER_H_



/*


 o(O)o 
o() ()o
 o(O)o 
   \  _
    \//
    /'
   |



*/