#include <mx/mxrender.h>

#define MX_IMPLEMENTATION_RSX
#include <mx/mxrsx.h>
#include <mx/addons/mxtext.h>

#define STB_TRUETYPE_IMPLEMENTATION
#include <stb/stb_truetype.h>

int main(int argc, char* argv[])
{

    // SETUP
    mx::context_t ctx = mx::context::create("mxui test app");
    // ctx.camera.controller.zoom_speed = 0.f;
    printf("[Running on %s]\n", mx::cwd().data());
    
    // Enablind blending
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // mx::directory::load(mx::cwd());
    mx::rsxmanager_t cwd_rsxm = mx::rsxmanager::create("F:/PROJECT/mx/mxlibs/assets");
    // mx::rsxmanager::list(cwd_rsxm);

    //////////////////////////////////////////////////////////////////::

    mx::font_t novamono = mx::font::load("F:/PROJECT/mx/mxlibs/assets/fonts/NovaMono-Regular.ttf", 64);
    mx::geometry_t textg = mx::font::bake_text(novamono, "It seems to work\nI think...");

    //////////////////////////////////////////////////////////////////::

    printf("Setting up the shader\n");
    mx::shader_t quadshader = mx::shader::load_from_files("../../assets/shaders/text.vs", "../../assets/shaders/text.fs");

    mx_mat4 model = mx_mat4_identity();
    mx::shader::begin(quadshader);
    mx::shader::set_uniform_mat(quadshader, "u_model", mx_mat4_raw(model).ptr, 4);
    mx::shader::end(quadshader);

    printf("Setting up the texture uniform \n");
    mx::texture::bind(novamono.tex);
    mx::shader::set_uniform<int>(quadshader, "tex", 2);

    printf("Main loop...\n");
    // MAIN LOOP
    while(!mx::context::should_close(ctx))
    {
        // begin render
        mx::context::pre_render(&ctx);
        mx::context::set_uniforms(&ctx, quadshader);

        mx::geometry::render(&textg, quadshader);

        // end render
        mx::context::post_render(&ctx);
    }
    mx::context::end();

    return 0;
}