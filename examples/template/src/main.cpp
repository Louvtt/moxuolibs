#include <mx/mxrender.h>

int main(int argc, char* argv[])
{
    // SETUP
    mx::context_t ctx = mx::context::create("mxTemplate app");

    // MAIN LOOP
    while(!mx::context::should_close(ctx))
    {
        // begin render
        mx::context::pre_render(&ctx);

        // GRAPHIC CODE

        // end render
        mx::context::post_render(&ctx);
    }
    mx::context::end();

    return 0;
}