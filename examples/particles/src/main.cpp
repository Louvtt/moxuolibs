#include <mx/mxrender.h>

#define MAX_PARTICLES 10000

struct particle_system_2D_t {
    struct particle {
        mx_vec3 pos   {};
        mx_vec3 vel   {};
        mx_vec3 color {1.f,1.f,1.f};
        float scale   {20.f};
        float rotation{0.f};
        float lifetime{10.f};
    };
    std::vector<particle> particles{ };
    mx::geometry_t rendered_particles;
    mx::shader_t shader;
    uint32_t max_particles  = 0;
    uint32_t particle_count = 0;

    inline void create(uint32_t reserved_count = MAX_PARTICLES) {
        shader = mx::shader::load_from_files("../../assets/shaders/quad.vs", "../../assets/shaders/quad.fs");

        mx::geometry_params_t gp;
        gp.max_shapes = reserved_count;
        max_particles = reserved_count;
        particle_count= 0;
        gp.mode = mx::draw_mode::TRIANGLES;
        mx::vertex_textured_t verts[] = {
            // pos                  // color                 // uv              // texID
            {{ -0.5f, -0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, { 0.f, 0.f }, .0f}, // top left
            {{  0.5f, -0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, { 1.f, 0.f }, .0f}, // top right
            {{  0.5f,  0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, { 1.f, 1.f }, .0f}, // bottom right
            {{  0.5f,  0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, { 1.f, 1.f }, .0f}, // bottom right
            {{ -0.5f,  0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, { .0f, 1.f }, .0f}, // bottom left
            {{ -0.5f, -0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, { 0.f, 0.f }, .0f}, // top left
        };
        gp.vertex_per_shape = sizeof(verts) / sizeof(mx::vertex_textured_t);
        gp.shape_vertices = &verts[0];
        rendered_particles = mx::geometry::create_dynamic(gp);
        particles.reserve(reserved_count);

        mx_mat4 model = mx_mat4_identity();
        mx::shader::begin(shader);
        mx::shader::set_uniform_mat(shader, "u_model", mx_mat4_raw(model).ptr, 4);
        mx::shader::end(shader);

    }

    inline void emit(const particle& p) {
        if(particle_count >= max_particles) return;
        // printf_s("Emitting new particle (%d) at (%f, %f, %f)\n", particles.size(), MX_VEC3_UNPACK(p.pos));
        mx::geometry::add_shape(&rendered_particles, 
                p.pos,
                mx_vec4{MX_VEC3_UNPACK(p.color), 1.f},
                mx_vec3{p.scale, p.scale, p.scale}
            );
        particles.push_back(p);
        ++particle_count;
    }
    
    inline void update(float ts) {
        std::vector<particle> tmp {};
        tmp.reserve(particles.size());
        for(int i = 0; i < particles.size(); ++i) {
            if(particles[i].lifetime < 0) { --particle_count; continue; }
            particles[i].lifetime -= ts;
            particles[i].pos = mx_vec3_add(particles[i].pos, mx_vec3_mulf(particles[i].vel, ts));

            // bound check
            if(particles[i].pos.x < -800.f + particles[i].scale
            || particles[i].pos.x >  800.f - particles[i].scale) particles[i].vel.x *= -1.f;
            if(particles[i].pos.y < -640.f + particles[i].scale
            || particles[i].pos.y >  640.f - particles[i].scale) particles[i].vel.y *= -1.f;

            mx::geometry::add_shape(&rendered_particles, 
                particles[i].pos,
                mx_vec4{MX_VEC3_UNPACK(particles[i].color), 1.f},
                mx_vec3{particles[i].scale, particles[i].scale, particles[i].scale}
            );
            tmp.push_back(particles[i]);
        }
        particles.clear();
        particles.insert(particles.begin(), tmp.begin(), tmp.end());
    }
    
    inline void render() 
    {
        mx::geometry::flush(&rendered_particles);
        mx::geometry::render(&rendered_particles, shader);
        mx::geometry::reset(&rendered_particles);
    }
};

static void particle_add(particle_system_2D_t* p, mx_vec3 pos, mx_vec3 col) {
    
    const mx_vec3 rvel = mx_vec3_mulf(mx_vec3_random_unit(), 599.f);
    p->emit({
        pos,
        mx_vec3{rvel.x, rvel.y, .0f},
        col,
        20.f,
        0.f,
        15.f
    }); 
}

static void emitter(mx::context_t ctx, particle_system_2D_t* p){
    mx_vec3 pos{};
    mx_vec3 col{1.f,1.f,1.f};
    bool create = true;

    if(mx::input_manager::is_key_down(ctx.input_manager, GLFW_KEY_F)) {
        pos = mx_vec3{50.f, -50.f,};
        particle_add(p, pos, col);
    }
    if(mx::input_manager::is_key_down(ctx.input_manager, GLFW_KEY_G)) {
        // col = mx_vec3{0.f, 7.f, .0f};
        pos = mx_vec3{-50.f, .7f,};
        particle_add(p, pos, col);
    }
    if(mx::input_manager::is_key_down(ctx.input_manager, GLFW_KEY_H)) {
        pos = {50.f, 25.f };
        // col = mx_vec3{.6f,.0f,.9f};
        particle_add(p, pos, col);
    }
    if(mx::input_manager::is_key_pressed(ctx.input_manager,GLFW_KEY_R)){
        p->particles.clear();
        mx::geometry::reset(&p->rendered_particles);
    }
}

static void emitter2(mx::context_t ctx, particle_system_2D_t* p){
    mx_vec3 pos{};
    mx_vec3 col{1.f,1.f,1.f};
    bool create = true;

    if(mx::input_manager::is_key_down(ctx.input_manager, GLFW_KEY_S)) {
        pos = mx_vec3{0.f, 0.f,};
        particle_add(p, pos, col);
    }
    if(mx::input_manager::is_key_down(ctx.input_manager, GLFW_KEY_S)) {
        pos = mx_vec3{-250.f, 250.f,};
        particle_add(p, pos, col);
    }
    if(mx::input_manager::is_key_down(ctx.input_manager, GLFW_KEY_S)) {
        col = mx_vec3{0.f, 7.f, .0f};
        pos = mx_vec3{250.f, 250.f,};
        particle_add(p, pos, col);
    }
    if(mx::input_manager::is_key_down(ctx.input_manager, GLFW_KEY_S)) {
        pos = {250.f, -250.f };
        col = mx_vec3{.6f,.0f,.9f};
        particle_add(p, pos, col);
    }
    if(mx::input_manager::is_key_down(ctx.input_manager, GLFW_KEY_S)) {
        pos = {-250.f, -250.f };
        col = mx_vec3{.9f,.9f,.1f};
        particle_add(p, pos, col);
    }

    if(mx::input_manager::is_key_down(ctx.input_manager,GLFW_KEY_R)){
        p->particles.clear();
        mx::geometry::reset(&p->rendered_particles);
    }
}

int main(int argc, char* argv[])
{
    printf("[Running app at %s]\n", mx::__mxr_cwd().c_str());

    // SETUP
    mx::context_t ctx = mx::context::create("mxTemplate app", 1000, 1000);
    // ctx.camera = mx::camera::create({}, {1600.f, 1600.f}, 1.f);
    
    // Particles
    particle_system_2D_t p;
    p.create();
    mx::context::set_uniforms(&ctx, p.shader);

    // MAIN LOOP
    printf_s("LOOOOP\n");
    while(!mx::context::should_close(ctx))
    {
        // update
        emitter(ctx, &p);
        emitter2(ctx, &p);
        p.update(.016f);

        // render
        mx::context::pre_render(&ctx);
        p.render();
        mx::context::post_render(&ctx);
    }
    mx::context::end();

    return 0;
}